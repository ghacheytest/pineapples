﻿namespace Sw.Component.PagedList {
  let trackingCols = [
    {
      fields: "EditUser", name: "EditUser", displayName: "Edited by...",
      editable: false,
      cellClass: "tracking",
      width: 120,
    },
    {
      fields: "EditDateTime", name: "EditDateTime", displayName: "edited on...",
      editable: false, cellFilter: 'date:"dd-MMM-yyyy HH:mm"',
      cellClass: "tracking",
      width: 120
    }
  ];
  // modelled on the ui-grid row header, but that cannot be turned off once it is on,
  // so fake it with a regular column
  let rowHeaderCol = {
    name: 'rowHeader', displayName: '',
    cellTemplate: '<row-edit-manager row="row"></row-edit-manager>',
    headerCellTemplate: `<div class=ui-grid-header-cell" ng-click="grid.appScope.vm.newRow()">
                            <span ng-show="grid.appScope.vm.canInsert()"><ng-md-icon icon="add" size=18 /></span></div`,
    editable: false, cellClass: "ui-grid-row-header-cell",
    width: 40, pinnedLeft: true
  };
  let deleteCol = {
    name: "Delete", displayName: '',
    cellTemplate: `<i class="material-icons md-18 md-dark" 
                    ng-click="grid.appScope.vm.delete(row)"
                    ng-show="!row.entity.isNew",
                    ng-enabled="grid.appScope.vm.canDelete(row.entity)">delete_forever</i>`,
    editable: false,
    width: 30
  };
  // calculate the width of a set of columns
  // assumes each columndef has a width attribute, in px

  var colwidths = function (columndefs, start, end) {
    var tot = 0;
    for (var i = start; i <= end; i++) {
      tot += parseFloat(columndefs[i].width);
    }
    return tot;

  };
  /**
   * @ngdoc controller
   * @name
   * Controller
   * @description
   * manages the display of the grid
   *
   **/
  class Controller {

    public isWaiting = false;
    public pagination: any = {};
    public runOnLoad = false;

    public dataResult: any;
    public viewMode: any;
    public entityType: any;

    // track rendered view
    public renderedViewMode = "";
    public renderedPageNo = 1;
    public renderedPageSize = 50;

    public gridClass: any;

    public baseState: string;
    protected baseOptions: any;

    public gridOptions: any;
    public gridApi: any;

    public columnWidths: any;

    private _isEditing = false;
    public isEditableView = false;
    public insertable = false;      // can insert rows when in editing mode

    public onSortClear;
    public onSort;
    public onShowPage;
    public onAction;

    // provide a setup for singe row selection, that can be turned on or off
    // use the ui-grid selection module
    //http://ui-grid.info/docs/#/api/ui.grid.selection.api:GridOptions
    // enableFooterTotalSelected
    // enableFullRowSelection
    // enableRowHeaderSelection
    // enableRowSelection
    // enableSelectAll
    // enableSelectionBatchEvent
    // multiSelect

    public get isEditing() {
      return this._isEditing;
    }
    public set isEditing(editing) {
      this._isEditing = editing;
      if (this.gridApi) {
        this.gridApi.grid.refresh();
      };
    }

    /**
     * OnRegisterApi
     * performs the setup of all grid related paramters after the grid is initialised
     * @param gridApi
     */
    protected onRegisterApi(gridApi) {
      if (this.gridApi) {
        return;
      }

      this.gridApi = gridApi;
      // set up the ui-grid handlers for sorting and pagination changes
      // these just reflect these into the enclosing context by raising the appropirate event
      gridApi.core.on.sortChanged(this.scope, (grid, sortColumns) => {
        // only requery if useExternalSorting
        if (this.pagination.enablePagination) {
          if (sortColumns.length === 0) {
            //this.theFilter.SortOn(this.findConfig.current);
            this.onSortClear();
          } else {
            //this.theFilter.SortOn(this.findConfig.current, sortColumns[0].field, sortColumns[0].sort.direction);
            this.onSort(sortColumns[0].field, sortColumns[0].sort.direction);
          }
        }
      });
      gridApi.pagination.on.paginationChanged(this.scope, (newPage, pageSize) => {
        if (this.pagination.enablePagination) {
          if (+newPage !== +this.renderedPageNo || +pageSize !== +this.renderedPageSize) {
            //this.theFilter.ShowPage(this.findConfig.current, newPage, pageSize);
            this.onShowPage(newPage, pageSize);
          };
        };
      });

      gridApi.rowEdit.on.saveRow(this.scope, this.onSave);
      gridApi.cellNav.on.navigate(this.scope, this.onNavigate);
      gridApi.grid.options.rowEditWaitInterval = -1;  // force manual saving
      // register a column processor for the row edit column 
      gridApi.core.registerColumnsProcessor((columns) => {
        columns.some((column) => {
          if (column.name === "rowHeader") {
            column.visible = this.isEditing;
            return true;
          }
          return false;
        });
        return columns;
      });

      // finally appply any initial viewMode and dataresult
      if (this.viewMode) {
        this.applyViewMode(this.viewMode);
      }
      if (this.dataResult) {
        this.applyData(this.dataResult);
      }
    }

    protected isRowSelectable = (gridRow) => {
      return (this.gridOptions.enableRowSelection ? true : false);
    }

    static $inject = ['$scope', '$state', '$q', "$compile", "$timeout", "gridUtil", 'Restangular', 'Lookups'
      , "PermAuthorization", "PermPermissionMap"];
    constructor(public scope, public $state: ng.ui.IStateService,
      public q: ng.IQService,
      public compile: ng.ICompileService,
      public timeout: ng.ITimeoutService,
      public gridUtil: any,         // ui-grid gridUtil service
      public restangular: restangular.IService

      , public lookups: Sw.Lookups.LookupService
      , public permAuthorization: any
      , public permPermissionMap: any) {

      var statedata = $state.current.data;

      if (statedata) {
        if (statedata.pagination) {
          this.pagination = statedata.pagination;
        };
        this.runOnLoad = statedata.runOnLoad || false;
      }

      if (this.pagination.enablePagination === undefined) {
        this.pagination.enablePagination = true;
      }

      // get a pointer to the underlying list state
      // this may not be the current state, if this controller was loaded
      // by navigating the Url to a child of that list state (e.g. .item)
      // 4 7 2016 - account for lists not named .list
      if ($state.current.name.indexOf(".list") >= 0) {
        this.baseState = $state.current.name.substring(0, $state.current.name.indexOf(".list") + 5);
      } else {
        this.baseState = $state.current.name;
      }

      // utilities for grid management
      this.columnWidths = colwidths;

      this.baseOptions = {
        paginationPageSizes: [10, 25, 50, 100, 500],
        paginationPageSize: 25,

        enablePagination: this.pagination.enablePagination,
        enablePaginationControls: this.pagination.enablePagination,
        useExternalPagination: this.pagination.enablePagination,
        useExternalSorting: this.pagination.enablePagination,
        // v 3 introduces various options about sorting that are overly complex and have to be removed 7 2 2016
        enableColumnMenus: false,

        // selection defaults are off
        enableRowSelection: false,
        enableRowHeaderSelection: false,
        enableFullRowSelection: false,
        isRowSelectable: this.isRowSelectable,
        multiSelect: false,

        // support for dynamic row template - initialise to the default
        rowTemplate: "ui-grid/ui-grid-row",
        rowHeight: 30,
        onRegisterApi: (gridApi) => {
          this.onRegisterApi.call(this, gridApi);
        }
      };

      this.gridOptions = angular.copy(this.baseOptions);
    };

    public action = (actionName, columnField, rowData) => {
      this.onAction(actionName, columnField, rowData);
    };

    public $onChanges(changes) {
      if (changes.viewMode.currentValue && this.gridApi) {
        this.applyViewMode(this.viewMode);
      }
      if (changes.dataResult && this.gridApi) {
        this.applyData(this.dataResult);
      }

    }
    /**
  * searchComplete
  * @description
  * handler to receive new data
  *
  * @param {event} event the broadcast event
  * @param {object} resultpack the data package returned from the server
  **/
    protected applyData = (result) => {

      let d = this.q.defer();
      d.resolve();
      let p: ng.IPromise<any> = d.promise;


      if (this.viewMode !== this.renderedViewMode) {
        p = this.applyViewMode(this.viewMode);
      }
      // before changing the data we have a chance to get the selectedRows 
      // they are gone when the data changes
      p.then(() => {
        //// TO DO
        let idField = null;
        //let idField = this.theFilter.defaultId();
        let selectedId = null;
        //////if (this.selectionCount() === 1) {
        //////  selectedId = this.selectedRows()[0][idField];
        //////}
        // insert the data int the options
        this.gridOptions.data = this.dataResult;

        // pagination info is supplied by the incoming data too
        this.gridOptions.totalItems = this.dataResult.numResults;
        if (this.dataResult.numResults > 0 && this.pagination.enablePagination) {

          if (this.gridOptions.paginationCurrentPage !== this.dataResult.pageNo) {

            this.gridOptions.paginationCurrentPage = this.dataResult.pageNo;
          }
          if (this.gridOptions.paginationPageSize !== this.dataResult.pageSize) {
            this.gridOptions.paginationPageSize = this.dataResult.pageSize;
          }
          this.renderedPageNo = this.dataResult.pageNo;
          this.renderedPageSize = this.dataResult.pageSize;
        };
        // temporary fix ? rowHeight does not seem to get propogated from gridOptions
        // to grid.options (see GridRow in UI source code)
        // so we can push it here
        var liveOptions = this.gridApi.grid.options;
        angular.extend(liveOptions, this.gridOptions);
        // reinstate the selection
        let e: any;
        if (this.dataResult.length === 1) {
          e = this.dataResult[0];
        }
        else if (selectedId) {
          e = _.find(this.dataResult, r => (r[idField] === selectedId));
        }
        if (e) {
          this.timeout(() => { this.gridApi.selection.selectRow(e) }, 0);
        }
      })
        .then(() => {
          this.isWaiting = false;
        });
    };
    protected applyViewMode(vm) {
      // set the gridClass from the view mode
      this.gridClass = vm.gridClass;

      // is this viewmode editable?
      // defaults to false, editable property can be true , false or array of roles
      // get a promise representing the editable state...
      let vmEditable: boolean;
      let dEditable = this.q.defer();
      let pEditable: ng.IPromise<any>;
      if (vm.editable === undefined) {
        vmEditable = false;
        dEditable.reject();
        pEditable = dEditable.promise;
      } else {

        if (vm.editable.only || vm.editable.except) {
          // in angular-permission: this unusual structure:
          // the service "PermPermissionMap" does not return an object, but a "constructor" function
          // when the "service" is retrieved via angular injection, we have that function...
          // so we can use "new" to get an "instance" of that 'class' 
          let permissionMap = new this.permPermissionMap(vm.editable);
          pEditable = this.permAuthorization.authorizeByPermissionMap(permissionMap);
        } else {
          vmEditable = (vm.editable ? true : false);
          if (vm.editable) {
            dEditable.resolve();
          } else {
            dEditable.reject();
          }
          pEditable = dEditable.promise;
        }
      }

      // return the promise
      return pEditable
        .then(() => {
          vmEditable = true;
        }, () => {
          vmEditable = false;
        })
        // vmEditable is established
        .then(() => {
          this.isEditableView = vmEditable;       // change the UI if it is potentially editable
          this.insertable = vmEditable && vm.insertable;   // by default can't insert

          // tracking columns are standard columns for EditUser and EditDateTime
          // derived from property includeTrackingColumns - if not present, true if layout is editable
          let vmIncludeTrackingColumns: boolean = false;
          if (vm.includeTrackingColumns === undefined) {
            vmIncludeTrackingColumns = vmEditable;
          } else {
            vmIncludeTrackingColumns = vm.includeTrackingColumns;
          }
          // do we need the Delete icon column?
          let vmDeletable = vmEditable ? vm.deletable : false; // no deleting without editing
          let vmIncludeDeleteColumn = false;
          if (vm.includeDeleteColumn === undefined) {
            vmIncludeDeleteColumn = vmDeletable;
          } else {
            // the view mode may make its own ui arrangements for delete?
            vmIncludeDeleteColumn = vm.vmIncludeDeleteColumn;
          }


          // set up the columndef array
          var cd = new Array();
          // if its an editable view mode, add the row header
          if (vmEditable) {
            cd.push(rowHeaderCol);
          }


          // add the viewmode's particular columndefs
          cd = cd.concat(vm.gridOptions.columnDefs);
          // if the viewMode specifies deleteable, add the Delete icon
          if (vmIncludeDeleteColumn) {
            cd = cd.concat(deleteCol);
          }
          // if the viewMode specifies editable
          // add the tracking columns (Edited by... on..) if the viewmode asks for that
          if (vmIncludeTrackingColumns) {
            cd = cd.concat(trackingCols);
          }

          // configure each column with some defaults, and some custom settings
          cd.forEach(c => {
            if (c.enableSorting === undefined) {
              c.enableSorting = true;
            }
            if (c.enableSorting && c.sortDirectionCycle === undefined) {
              c.sortDirectionCycle = ["asc", "desc"];
            }
            // lookup is the name of a lookup table in the Lookup cache
            // if defined on a column, it is used to
            // - filter that column
            // - define a dropdown when editing
            if (c.lookup) {
              c.editDropdownOptionsFunction = (row, col) => {
                return this.lookups.getList(c.lookup);
              };
              c.editableCellTemplate = c.editableCellTemplate || 'customGrid/dropdownEditor';
              c.editDropdownIdLabel = c.editDropdownIdLabel || 'C';
              c.editDropdownValueLabel = c.editDropdownValueLabel || 'N';
              c.cellFilter = "lookup:'" + c.lookup + "'";
            }
            // is the column editable?
            let colEditable: boolean = false;
            if (vmEditable) {
              if (c.editable === undefined || c.editable == "new") {
                colEditable = true;   // default the whole column to editable when viewmode is editable
              } else {
                if (Array.isArray(c.editable)) {
                  // TO DO
                  // use angular permissions to check the array of roles?
                } else {
                  colEditable = (c.editable ? true : false);
                }
              }
            }

            c.enableCellEdit = colEditable;
            if (c.editable == "new") {
              c.cellEditableCondition = (scope) => this.isEditing && scope.row.entity.isNew;
            } else {
              c.cellEditableCondition = () => this.isEditing;
            }
            c.enableCellEditOnFocus = c.cellEditOnFocus || vm.defaultCellEditOnFocus || c.enableCellEdit;
            // can the column accept the focus?
            c.allowCellFocus = c.cellFocus || (c.enableCellEdit ? true : vm.defaultAllowCellFocus || false);
          });
          var newOptions: any = angular.copy(this.baseOptions);

          // support selection
          switch (vm.selectable) {
            case "single":
              // implement single select
              newOptions.enableRowSelection = true;
              newOptions.multiSelect = false;
              newOptions.enableRowHeaderSelection = true;
              newOptions.enableFullRowSelection = true;
              break;

            case "multi":
              newOptions.enableRowSelection = true;
              newOptions.multiSelect = true;
              newOptions.enableRowHeaderSelection = false;
              newOptions.enableFullRowSelection = true;
              break;
            default:
              // clear any current selection, no longer applicable
              newOptions.enableRowSelection = false;
              newOptions.multiSelect = false;
              newOptions.enableRowHeaderSelection = false;
              newOptions.enableFullRowSelection = false;
              this.gridApi.selection.clearSelectedRows();
              break;

          }
          // note the viewMode can explicitly override the selection settings in its gridOptions
          // if something more customised is wanted
          angular.extend(newOptions, vm.gridOptions);
          newOptions.columnDefs = cd;
          this.gridOptions = angular.copy(newOptions);
          // but this seems to be not enough.....
          let rowTemplateChanged = (this.gridApi.grid.options.rowTemplate == this.gridOptions.rowTemplate ? false : true);
          angular.extend(this.gridApi.grid.options, this.gridOptions);
          if (rowTemplateChanged) {
            this.setUpRowTemplate(this.gridApi.grid);
          }
          this.renderedViewMode = vm;
        });
    }

    // Selection support
    public selectionCount() {
      // this can get out of kilter
      //return this.gridApi.selection.getSelectedCount();
      // this is more reliable
      return this.gridApi.selection.getSelectedRows().length;
    }
    public selectedRows() {
      return this.gridApi.selection.getSelectedRows();
    }

    // row template support
    // this code is copied from ui-grid GridCalssFactory
    // this is needed if we want to dynamically alter the rowTemplate
    private setUpRowTemplate(grid) {
      var rowTemplateFnPromise = this.q.defer();
      grid.getRowTemplateFn = rowTemplateFnPromise.promise;

      this.gridUtil.getTemplate(grid.options.rowTemplate)
        .then(
        (template) => {
          var rowTemplateFn = this.compile(template);
          rowTemplateFnPromise.resolve(rowTemplateFn);
        },
        (res) => {
          // Todo handle response error here?
          throw new Error("Couldn't fetch/use row template '" + grid.options.rowTemplate + "'");
        });
    }

    /************************************************************************************
    * Row edit support
    *************************************************************************************/
    private entityToSave;

    // callable from the view to manually invoke an immediate save
    public saveRow = (row) => {
      this.entityToSave = row.entity;
      this.setDirty([row.entity]);
      this.gridApi.rowEdit.flushDirtyRows();
    };

    public onNavigate = (newRolCol, oldRowCol) => {
      if (newRolCol.row === oldRowCol.row) {
        return;
      }
      if (oldRowCol.row.isDirty && !oldRowCol.row.isSaving) {
        this.saveRow(oldRowCol.row);
      }
    };

    // callback from rowEdit
    // rowEdit requires this method to return a promise, or else it fails
    // 
    public onSave = (rowEntity) => {
      if (this.entityToSave && rowEntity !== this.entityToSave) {
        // ui-grid has already set isSaving = true - so turn that off
        this.gridApi.grid.rowEdit.dirtyRows.forEach((row) => {
          if (row.entity === rowEntity) {
            delete row.isSaving;
          }
        });
        return;     // only do a single record at a time
      }

      delete rowEntity.errorData;
      let promise: ng.IPromise<any> = null;
      // identify a new row in a grid

      //we assume that restangular has added the "put" method to this object t update itself
      // that is, the collection we are editing has been "restangularised"
      if (rowEntity.isNew) {
        promise = rowEntity.post();
      } else {
        promise = rowEntity.put();       // update
      }
      promise.then((newData) => {

        console.log("post success");
        console.log(newData);
        // newData is a simple collection of the fields that were passed in, with new values
        angular.extend(rowEntity, newData.plain());
        if (rowEntity.isNew) {
          delete rowEntity.isNew;
        }
        // gonto says this is necessary.... :(
        // https://github.com/mgonto/restangular/issues/367
        // this.restangular.restangularizeElement(rowEntity.parentResource, rowEntity, rowEntity.route);
        // gonto says we need to rebuild the restangular link
        // 8 5 2016 see link above for my comment and why newData.plain() is the fix
      }, // error response
        (errorData) => {
          rowEntity.errorData = errorData;
          return this.q.reject(errorData);
        }
      );
      this.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };

    public setDirty = (rowEntities) => {
      this.gridApi.rowEdit.setRowsDirty(rowEntities);
    };
    /******************************************************************
    * Insert
    *******************************************************************/
    public canInsert() {
      return this.isEditing && this.insertable;
    }
    public newRow() {
      // create an object whose property names are inferred from either:
      // the first element of the existing array
      // or the fields bound into the grid
      let data: any[] = this.gridApi.grid.options.data;

      let newRow: any = { isNew: true };
      this.restangular.restangularizeElement(null, newRow, this.entityType);
      //this.restangular.restangularizeElement(data[0].parentResource, newRow, data[0].route);
      data.push(newRow);
      // to do find some way to flag this is a new row
    }

    public newRowFn = (scope) => scope.row.entity.isNew;

    /******************************************************************
    * Delete  Row                                                     *
    *******************************************************************/

    /**
     * Test if the row can be deleted
     * This is primarily to ng-enable the delete button
     * {param} row - the ui-grid row
     */
    public canDelete(row) {
      if (row.isSaving || row.isDeleting) {
        return false;
      }
      return true;
    }
    public delete = (row) => {
      // check that we are not trying to save this data, and 
      const DELETE_DELAY = 250;
      if (!this.canDelete(row)) {
        return;
      }
      row.isDeleting = true;
      let promise: ng.IPromise<any> = null;
      // identify a new row in a grid

      //we assume that restangular has added the "put" method to this object t update itself
      // that is, the collection we are editing has been "restangularised"
      let rowEntity = row.entity;
      delete rowEntity.errorData;
      delete row.isDeleteError;

      promise = rowEntity.remove();
      promise.then((response) => {
        delete row.isDeleting;
        row.isDeleted = true;
        this.timeout(DELETE_DELAY).then(() => {
          let index = this.gridOptions.data.indexOf(rowEntity);
          this.gridOptions.data.splice(index, 1);
        });
      }, // error response
        (errorData) => {
          row.isDeleteError = true;
          rowEntity.errorData = errorData;
          return this.q.reject(errorData);
        });
    }
    public onDelete = (rowEntity) => {
      if (this.entityToSave && rowEntity !== this.entityToSave) {
        // ui-grid has already set isSaving = true - so turn that off
        this.gridApi.grid.rowEdit.dirtyRows.forEach((row) => {
          if (row.entity === rowEntity) {
            delete row.isSaving;
          }
        });
        return;     // only do a single record at a time
      }

      delete rowEntity.errorData;
      let promise: ng.IPromise<any> = null;
      // identify a new row in a grid

      //we assume that restangular has added the "put" method to this object t update itself
      // that is, the collection we are editing has been "restangularised"
      promise = rowEntity.remove();
      promise.then((response) => {
        let index = this.gridOptions.data.indexOf(rowEntity);
        this.gridOptions.data.splice(index, 1);
      }, // error response
        (errorData) => {
          rowEntity.errorData = errorData;
          return this.q.reject(errorData);
        }
      );
      this.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;
    public require;

    constructor() {
      this.bindings = {
        dataResult: "<",
        viewMode: "<",
        entityType: "<",
        isEditing: "<",
        // bindings to 'events' of the grid       
        onSortClear: "&",
        onSort: "&",
        onShowPage: "&",
        onAction: "&",
        saving: "&",
        saved: "&"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "directive/supergrid";
    }
  }

  angular
    .module("sw.common")
    .component("superGrid", new Component());
} 