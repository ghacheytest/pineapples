﻿module Sw {
  class PagedListEditableController extends PagedListController {
    private entityToSave: any;    // the entity requestsed to be saved
    protected onRegisterApi(gridApi) {
      super.onRegisterApi(gridApi);
      // required for saving
      gridApi.rowEdit.on.saveRow(this.scope, this.onSave);
      gridApi.cellNav.on.navigate(this.scope, this.onNavigate);
      gridApi.grid.options.rowEditWaitInterval = -1;  // force manual saving
    }

    // callable from the view to manually invoke an immediate save
    public saveRow = (row) => {
      this.entityToSave = row.entity;
      this.setDirty([row.entity]);
      this.gridApi.rowEdit.flushDirtyRows();
    };

    public onNavigate = (newRolCol, oldRowCol) => {
      if (newRolCol.row === oldRowCol.row) {
        return;
      }
      if (oldRowCol.row.isDirty && !oldRowCol.row.isSaving) {
        this.saveRow(oldRowCol.row);
      }
    };

    // callback from rowEdit
    public onSave = (rowEntity) => {
      if (this.entityToSave && rowEntity !== this.entityToSave) {
        // ui-grid has already set isSaving = true - so turn that off
        this.gridApi.grid.rowEdit.dirtyRows.forEach((row) => {
          if (row.entity === rowEntity) {
            delete row.isSaving;
          }
        });
        return;     // only do a single record at a time
      }
      delete rowEntity.errorData;
      let promise: ng.IPromise<any> = null;
      // identify a new row in a grid

      promise = rowEntity.put();       // update
      // promise = rowEntity.post();       // create

      promise.then((newData) => {

        console.log("post success");
        console.log(newData);
        // newData is a simple collection of the fields that were passed in, with new values
        angular.extend(rowEntity, newData.plain());
        // gonto says this is necessary.... :(
        // https://github.com/mgonto/restangular/issues/367
        // this.restangular.restangularizeElement(rowEntity.parentResource, rowEntity, rowEntity.route);
        // gonto says we need to rebuild the restangular link
        // 8 5 2016 see link above for my comment and why newData.plain() is the fix
      }, // error response
        (errorData) => {
          rowEntity.errorData = errorData;
          return this.q.reject(errorData);
        }
      );
      this.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };

    public setDirty = (rowEntities) => {
      this.gridApi.rowEdit.setRowsDirty(rowEntities);
    };

    public newRow() {
      // create an object whose property names are inferred from either:
      // the first element of the existing array
      // or the fields bound into the grid
      let data: any[] = this.gridApi.grid.options.data;

      let newRow: any = {};
      this.restangular.restangularizeElement(data[0].parentResource, newRow, data[0].route);
      data.push(newRow);
      // to do find some way to flag this is a new row
    }
  }

  angular
    .module('sw.common')
    .controller('PagedListEditableController', PagedListEditableController);
}
