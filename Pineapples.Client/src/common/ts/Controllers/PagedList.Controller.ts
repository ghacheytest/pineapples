﻿module Sw {

  let trackingCols = [
    {
      fields: "pEditUser",
      name: "pEditUser",
      displayName: "Edited by...",
      editable: false,
      width: 120,
    },
    {
      fields: "pEditDateTime",
      name: "pEditDateTime",
      displayName: "edited on...",
      editable: false,
      cellFilter: 'date:"dd-MMM-yyyy hh:nn"',
      width: 120,
    }
  ];

  // modelled on the ui-grid row header, but that cannot be turned off once it is on,
  // so fake it with a regular column
  let rowHeaderCol = {
    name: 'rowHeader',
    displayName: '',
    cellTemplate: '<row-edit-manager row="row"></row-edit-manager>',
    // headerCellTemplate: '<a href="" ng-click="grid.appScope.listvm.newRow()">Add</a>',
    editable: false,
    cellClass: "ui-grid-row-header-cell",
    width: 30,
    pinnedLeft: true
  };

  /**
   * @name colwidths
   * @description calculate the width of a set of columns
   * assumes each columndef has a width attribute, in px
   * 
   * TODO - Does not seem like it is being used. Also relying on a
   * hard coded width is not a good thing since specific width should only
   * ever be used when the data is of known size and equal among all pineapples
   * deployment in the Pacific.
   *
   * Also duplicate of this function in LookupsEditController.ts
   */
  var colwidths = function (columndefs, start, end) {
    var tot = 0;
    for (var i = start; i <= end; i++) {
      tot += parseFloat(columndefs[i].width);
    }
    return tot;
  };

  /**
   * @class PagedListController
   * @ngdoc controller
   * @name 
   * 
   * @description manages the display of a list of applications
   */
  export class PagedListController {

    public isWaiting = false;
    public pagination: any = {};
    public runOnLoad = true;

    public resultSet: any;

    // track rendered view
    public renderedViewMode = "";
    public renderedPageNo = 1;
    public renderedPageSize = 50;

    public gridClass: any;

    public baseState: string;
    protected baseOptions: any;

    public gridOptions: any;
    public gridApi: any;

    public columnWidths: any;

    protected onRegisterApi(gridApi) {
      this.gridApi = gridApi;
      gridApi.core.on.sortChanged(this.scope, (grid, sortColumns) => {
        // only requery if useExternalSorting
        if (this.pagination.enablePagination) {
          if (sortColumns.length === 0) {
            this.theFilter.SortOn(this.findConfig.current);
          } else {
            this.theFilter.SortOn(this.findConfig.current, sortColumns[0].field, sortColumns[0].sort.direction);
          }
        }
      });
      gridApi.pagination.on.paginationChanged(this.scope, (newPage, pageSize) => {
        if (this.pagination.enablePagination) {
          if (+newPage !== +this.renderedPageNo || +pageSize !== +this.renderedPageSize) {
            this.theFilter.ShowPage(this.findConfig.current, newPage, pageSize);
          };
        };
      });
      // at this point we can see if we need to run
      // also, if this Filter has already been searched, tell it we want to search again

      if (this.runOnLoad || this.findConfig.prepared) {
        this.theFilter.FindNow(this.findConfig.current);
      }
    }

    /**
     * searchComplete
     * @description
     * handler to receive new data
     *
     * @param {event} event the broadcast event
     * @param {object} resultpack the data package returned from the server
     **/
    protected searchComplete = (event, resultpack) => {

      if (resultpack.entity === this.theFilter.entity && resultpack.method === 'search') {

        this.resultSet = resultpack.resultset; // this is now a restangular object
        if (this.findConfig.current.viewMode !== this.renderedViewMode) {
          let modeName = this.findConfig.current.viewMode;

          var vm = this.theFilter.GetViewMode(modeName);
          if (!vm) {
            vm = this.theFilter.ViewModes[0];
          }
          this.applyViewMode(vm);
        }

        this.gridOptions.data = this.resultSet;
        this.gridOptions.totalItems = this.resultSet.numResults;
        if (this.resultSet.numResults > 0 && this.pagination.enablePagination) {

          if (this.gridOptions.paginationCurrentPage !== this.resultSet.pageNo) {

            this.gridOptions.paginationCurrentPage = this.resultSet.pageNo;
          }
          if (this.gridOptions.paginationPageSize !== this.resultSet.pageSize) {
            this.gridOptions.paginationPageSize = this.resultSet.pageSize;
          }
          this.renderedPageNo = this.resultSet.pageNo;
          this.renderedPageSize = this.resultSet.pageSize;
        };
        // temporary fix ? rowHeight does not seem to get propogated from gridOptions
        // to grid.options (see GridRow in UI source code)
        // so we can push it here
        var liveOptions = this.gridApi.grid.options;
        angular.extend(liveOptions, this.gridOptions);
      }
      this.isWaiting = false;
    };

    protected searchError = (event, resultpack) => {
      if (resultpack.entity === this.theFilter.entity) {
        // no point waiting around - this train aint comin'
        this.isWaiting = false;
      }
    };

    protected findNow = (event, data: Sw.Filter.IFindNowBroadcast) => {
      if (data.filter.entity === this.theFilter.entity) {
        data.actions.push(Sw.Filter.Request.search);
        this.isWaiting = true;
      }
    };

    static $inject = ['$scope', '$state', '$q', 'Restangular', 'findConfig', 'theFilter', 'Lookups'];
    constructor(public scope, $state, public q: ng.IQService,
      public restangular: restangular.IService,
      public findConfig: Sw.Filter.FindConfig, public theFilter: Sw.Filter.IFilter
      , public lookups: Sw.Lookups.LookupService) {

      var statedata = $state.current.data;

      if (statedata) {
        if (statedata.pagination) {
          this.pagination = statedata.pagination;
        };
        //this.runOnLoad = statedata.runOnLoad || false;
      }

      if (this.pagination.enablePagination === undefined) {
        this.pagination.enablePagination = true;
      }

      // get a pointer to the underlying list state
      // this may not be the current state, if this controller was loaded
      // by navigating the Url to a child of that list state (e.g. .item)
      this.baseState = $state.current.name.substring(0, $state.current.name.indexOf(".list") + 5);

      // utilities for grid management
      this.columnWidths = colwidths;

      this.baseOptions = {
        paginationPageSizes: [10, 25, 50, 100, 500],
        paginationPageSize: 25,

        enablePagination: this.pagination.enablePagination,
        enablePaginationControls: this.pagination.enablePagination,
        useExternalPagination: this.pagination.enablePagination,
        useExternalSorting: this.pagination.enablePagination,
        // v 3 introduces various options about sorting that are overly complex and have to be removed 7 2 2016
        enableColumnMenus: false,
        enableColumnResizing: true,

        onRegisterApi: (gridApi) => {
          this.onRegisterApi.call(this, gridApi);
        }
      };

      this.gridOptions = angular.copy(this.baseOptions);

      scope.$on('SearchComplete', this.searchComplete);

      scope.$on('SearchError', this.searchError);

      scope.$on('FindNow', this.findNow);
    };

    public action = (actionName, columnField, rowData) => {
      this.theFilter.Action(actionName, columnField, rowData, this.baseState);
    };

    protected applyViewMode(vm) {
      // set the gridClass from the view mode
      this.gridClass = vm.gridClass;

      // is this viewmode editable?
      // defaults to false, editable property can be true , false or array of roles
      let vmEditable: boolean;
      if (vm.editable === undefined) {
        vmEditable = false;
      } else {
        if (Array.isArray(vm.editable)) {
          // TO DO
          // use angular permissions to check the array of roles
        } else {
          vmEditable = (vm.editable ? true : false);
        }
      }

      // tracking columns are standard columns for pEditUser and pEditDateTime
      // derived from property includeTrackingColumns - if not present, true if layout is editable
      let vmIncludeTrackingColumns: boolean = false;
      if (vm.includeTrackingColumns === undefined) {
        vmIncludeTrackingColumns = vmEditable;
      } else {
        vmIncludeTrackingColumns = vm.includeTrackingColumns;
      }

      // set up the columndef array
      var cd = new Array();
      // if its an editable view mode, add the row header
      if (vmEditable) {
        cd.push(rowHeaderCol);
      }
      // add in the default columns that belong to every viewmode
      if (this.theFilter.ViewDefaults.columnDefs) {
        // append these columns to the set
        cd = cd.concat(this.theFilter.ViewDefaults.columnDefs);
      }
      // add the viewmode's particular columndefs
      cd = cd.concat(vm.gridOptions.columnDefs);
      // add the tracking columns (Edited by... on..) if the viewmode asks for that
      if (vmIncludeTrackingColumns) {
        cd = cd.concat(trackingCols);
      }

      // configure each column with some defaults, and some custom settings
      cd.forEach(c => {
        if (c.enableSorting === undefined) {
          c.enableSorting = true;
        }
        if (c.enableSorting && c.sortDirectionCycle === undefined) {
          c.sortDirectionCycle = ["asc", "desc"];
        }
        // lookup is the name of a lookup table in the Lookup cache
        // if defined on a column, it is used to
        // - filter that column
        // - define a dropdown when editing
        if (c.lookup) {
          c.editDropdownOptionsFunction = (row, col) => {
            return this.lookups.getList(c.lookup);
          };
          c.editableCellTemplate = c.editableCellTemplate || 'ui-grid/dropdownEditor';
          c.editDropdownIdLabel = c.editDropdownIdLabel || 'C';
          c.editDropdownValueLabel = c.editDropdownValueLabel || 'N';
          c.cellFilter = "lookup:'" + c.lookup + "'";
        }
        // is the column editable?
        let colEditable: boolean = false;
        if (vmEditable) {
          if (c.editable === undefined) {
            colEditable = true;   // default the whole column to editable when viewmode is editable
          } else {
            if (Array.isArray(c.editable)) {
              // TO DO
              // use angular permissions to check the array of roles
            } else {
              colEditable = (c.editable ? true : false);
            }
          }
        }

        c.enableCellEdit = colEditable;
        c.enableCellEditOnFocus = c.cellEditOnFocus || vm.defaultCellEditOnFocus || c.enableCellEdit;
        // can the column accept the focus?
        c.allowCellFocus = c.cellFocus || (c.enableCellEdit ? true : vm.defaultAllowCellFocus || false);
      });
      var newOptions: any = {};
      angular.extend(newOptions, this.baseOptions, vm.gridOptions);
      newOptions.columnDefs = cd;
      this.gridOptions = angular.copy(newOptions);
      this.renderedViewMode = this.findConfig.current.viewMode;
      // step through other properties of the vm.gridOptions, and apply them to the current gridOptions
    }
  }

  angular
    .module('sw.common')
    // theFilter will be injected by ui-router resolve
    .controller('PagedListController', PagedListController);
}
