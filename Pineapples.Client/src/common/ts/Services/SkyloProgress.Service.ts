﻿namespace Sw {
  // provides a wrapper around 'skylo' progress bar
  class Service {

    public start() {
        $.skylo('start');
    }

    public set(position) {
      $.skylo('set', position);
    }

    public end() {
      $.skylo('end');
    }

    public unwind() {
        $.skylo('unwind');
    };

    public get() {
        return $.skylo('get');
    }
    public inch(amount) {
      $.skylo('show', function () {
        $.skylo('inch', amount);
      });
    }
  }

  angular
    .module("sw.common")
    .service("skylo", Service);

}