﻿namespace Pineapples.Api {

  export interface IIndicatorsApi {
    vermdata(): ng.IPromise<any>;
    vermdataDistrict(districtCode: string): ng.IPromise<any>;
    refreshWarehouse(): ng.IPromise<any>;
  }

  let factory = (restAngular, errorService, cacheFactory) => {
    let svc = restAngular.all("indicators");
    svc.vermdata = () => {
      return svc.withHttpConfig({ cache: true }).customGET('vermdata');
    };
    // get the vermdata for a specific district
    svc.vermdataDistrict = (districtCode) => {
      return svc.withHttpConfig({ cache: true }).customGET('vermdatadistrict/' + districtCode);
    };

    svc.refreshWarehouse = () => {
      
      return svc.customGET("makewarehouse").then((d) => {
        // update the cached vermdata
        cacheFactory.get("$http").put('makewarehouse', d)
      });
    }
    svc.refreshVermData = () => {

      return svc.customGET("makevermdata").then((d) => {
        // update the cached vermdata
        cacheFactory.get("$http").put('vermdata', d)
      });
    }
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("IndicatorsAPI", ['Restangular', 'ErrorService', '$cacheFactory', factory]);
}
