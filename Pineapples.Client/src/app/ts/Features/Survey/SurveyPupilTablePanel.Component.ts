﻿namespace Pineapples.SurveyEntry {

  interface IBindings {
    schoolNo: string;       // the school no
    year: number;     // the survey year
    tableName: string;      // table name as in metaPupilTableDefs
    tableDescriptiveName: string; // full name (e.g. Enrolment)
  }

  class Controller implements IBindings {

    public gridForm: ng.IFormController;   // angular matches this to the name of the form in the view

    public schoolNo: string;
    public year: number;
    public tableName: string;
    public tableDescriptiveName: string;

    public isWaiting = false;
    public isEditing = false;

    public setEditing(editing: boolean) {
      this.isEditing = editing;
    }

    static $inject = ["schoolsAPI"];

    private _pt: PupilTable;
    private _ptInit: PupilTable;

    constructor(private _api) {
      console.log("gridpage");
    }

    public $onChanges(changes: any) {
      console.log(changes);
      this.getPupilTable();
    }

    public get pupilTable(): PupilTable {
      return this._pt;
    };


    public getPupilTable() {
      this._api.pupilTable(this.schoolNo, this.year, this.tableName)
        .then((data: PupilTable) => {
          this._pt = data;
          this.schoolNo = data.schoolNo;
          this.tableDescriptiveName = data.schoolNo;
        });
    }

    public save() {
      this.isWaiting = true;
      this._api.savePupilTable(this.pupilTable)
        .then((data: PupilTable) => {
          this.isWaiting = false;
          this._pt = data;
          this.schoolNo = data.schoolNo;
          this.pristine();
        });
    }

    public undo() {
      // TO DO not quite going to work with New, becuase modelInit does not have all the properties that model has
      angular.extend(this._pt, this._ptInit);
      this.pristine();
    }

    public refresh() {
      this.getPupilTable();
    }

    private pristine() {
      if (this.gridForm) {
        this.gridForm.$setPristine();
        this._ptInit = angular.copy(this.pupilTable);      // if we want to implement Undo
        this.setEditing(false);
      }
    }
  }

  class PupilTablePanel implements ng.IComponentOptions {
    public bindings: any = {
      schoolNo: "@",
      year: "@",
      tableName: "@"

    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    public templateUrl = "survey/pupiltablepanel";
  }

  angular
    .module("pineapples")
    .component("pupilTablePanel", new PupilTablePanel());
}
