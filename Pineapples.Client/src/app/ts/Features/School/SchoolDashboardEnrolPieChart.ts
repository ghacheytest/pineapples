﻿namespace Pineapples.Dashboard {

  interface IBindings {
    school: any;
    ngDisabled: boolean;
  }

  class Controller extends Sw.Charts.SizeableChartController implements IBindings {

    // injections
    static $inject = ["$compile", "$timeout", "$scope", "$element"];
    constructor($compile: ng.ICompileService, $timeout: ng.ITimeoutService, scope: ng.IScope, element) {
      super($compile, $timeout, scope, element);
    }

    // bindings
    public school: any;
    public ngDisabled: boolean;

    private _tip;

    public $onChanges(changes: any) {
      console.log(changes);
    }

    /**
     * @method newSize
     * @override Sw.Charts.SizeableComponentController's newSize
     * @description apply some rounding so this is not respnding to tiny fluctuations
     */
    public setSize(newSize: Sw.Charts.Size) {
      let scrollwidth = ((window.innerWidth - $(window).width())) || 0;
      this.size.width = Math.floor(newSize.width / 10) * 10 - (20 - scrollwidth);
      this.size.height = Math.floor(this.size.width / 3);       //3:1 aspect ratio is the default
    }

    /**
     * @method newSize
     * @override Sw.Charts.SizeableComponentController's newSize
     */
    public onResize(element) {
      if (this.size.height === 0) {
        return;
      }
      if (this.chart) {
        this.chart.redraw();
      } else {
        this.makeChart(element);
      }
    }

    private pie;
    private chart: Plottable.Component;

    public clickHandler(d) {
      alert(d.svyYear);
    }
    public makeChart = (element) => {
      let colorScale = new Plottable.Scales.Color();
      let legend = new Plottable.Components.Legend(colorScale);
      
      let ds = new Plottable.Dataset([
        { Name: "Male", Total: this.school.LatestSurvey.ssEnrolM },
        { Name: "Female", Total: this.school.LatestSurvey.ssEnrolF }
      ]);

      let tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html((d, i) => {
          if (d.Name === "Female") {
            return "F: " + d.Total;
          }
          return "M: " + d.Total;
        });
      this._tip = tip;      // set aside so we can destroy it later...

      let callbackAnimate = function (jr: Plottable.Drawers.JoinResult, attr, drawer) {
        (<d3.selection.Update<any>>jr.merge)
          .on("mouseover", tip.show)
          .on("mouseout", tip.hide)
          .on("click", self.clickHandler)
          .call(function () {
            compile(this[0].parentNode)(scope);
          });

        this.innerAnimate(jr, attr, drawer);
      };

      let innerAnimator = new Plottable.Animators.Easing()
        .stepDuration(50)
        .stepDelay(0);

      let animator = new Plottable.Animators.Callback()
        .callback(callbackAnimate)
        .innerAnimator(innerAnimator);

      this.pie = new Plottable.Plots.Pie()

      this.pie
        .addDataset(ds)
        .attr("fill", (d) => d.Name, colorScale)
        .attr("opacity", .8)
        .sectorValue((d) => d.Total)
        .labelsEnabled(true)
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, animator);

      let compile = this.$compile;
      let scope = this.scope;
      let self = this;

      this.chart = new Plottable.Components.Table([[this.pie, legend]]);
      
      let div = $(element).find("div")
      let svg = $(element).find("div>svg");
      let d3svg = d3.selectAll(svg.toArray());
      d3svg.call(<any>tip);
      this.chart.renderTo(d3svg);
    }
  }

  class EnrolPieChart implements ng.IComponentOptions {
    public bindings: any = {
      school: "<",
      ngDisabled: "="
    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    // angular doesn't watch ngOptions - so we have to make sure the right string is in the template when
    // it is first rendered
    public template = `<div><svg width="{{vm.size.width}}" height="{{vm.size.height}}"></svg></div>`;
  }

  angular
    .module("pineapples")
    .component("enrolPieChart", new EnrolPieChart());
}
