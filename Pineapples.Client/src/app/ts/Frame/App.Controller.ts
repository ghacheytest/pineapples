﻿namespace Pineapples {
  class Controller {

    static $inject = ["GlobalSettings", "identity", "authenticationService", "$mdSidenav"];

    constructor(private global: Pineapples.GlobalSettingsService,
      public identity: Sw.Auth.Identity,
      private authenticationService: Sw.Auth.IAuthenticationService,
      private mdSideNav: ng.material.ISidenavService) { }

    public get style_fullscreen() {
      return this.global.get('fullscreen');
    }
    public get style_navbarCollapsed() {
      return this.global.get('navbarCollapsed');
    }
 
    public get style_isSmallScreen() {
      return this.global.get('smallscreen');
    }

    public toggleNavBar() {
      if (this.style_isSmallScreen) {
        this.sidenavOpen = !this.sidenavOpen;
      } else {
        // on a large screen, toggle is betwen full and collapsed
        this.global.set('navbarCollapsed', !this.style_navbarCollapsed);
      }
      // this.mdSideNav("navBar").toggle();
    };

    public get sidenavOpen() {
      return this.global.get("navbarShown");
    }
    public set sidenavOpen(newvalue) {
      this.global.set("navbarShown", newvalue);
    }

    public logOut = this.authenticationService.logOut;

    // TO DO probably easier to attach identity to rootScope, and allow all scopes access to identity.etc ?
    public isLoggedIn() {
      //return $auth.userIsAuthenticated();  // this seems to cause endless digesting?
      return this.identity.isAuthenticated;
    }

    public userName() {
      return this.identity.userName;
    }
  };

  angular
    .module("pineapples")
    .controller('AppController', Controller);

}