namespace Pineapples {
  angular
    .module("pineapples")

    .directive('slideOut', function() {
      return {
        restrict: 'A',
        scope: {
          show: '=slideOut'
        },
        link: function (scope, element, attr) {
          element.hide();
          scope.$watch('show', function (newVal, oldVal) {
            if (newVal !== oldVal) {
              element.slideToggle({
                complete: function () { scope.$apply(); }
              });
            }
          });
        }
      }
    })
    .directive('slideOutNav', ['$timeout', 'GlobalSettings', function($t, global) {
      return {
        restrict: 'A',
        scope: {
          show: '=slideOutNav'
        },
        link: function (scope, element, attr) {
          scope.$watch('show', function (newVal, oldVal) {
            if (global.get("navbarCollapsed")) {
              if (newVal == true) {
                element.css('display', 'block');
              }
              else {
                element.css('display', 'none');
              }
              return;
            }
            let slideOpts = {
              complete: function () {
                $t(function () { scope.$apply() })
              }
            };
            if (newVal == true) {
              element.slideDown(slideOpts);
            } else if (newVal == false) {
              element.slideUp(slideOpts);
            }
          });
        }
      }
    }])
 
    .directive('fitHeight', ['$window', '$timeout', '$location', function ($window, $timeout, $location) {
      return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {
          scope.docHeight = $(document).height();
          var setHeight = function (newVal) {
            var diff = $('header').height();
            if ($('body').hasClass('layout-horizontal')) diff += 112;
            if ((newVal-diff)>element.outerHeight()) {
              element.css('min-height', (newVal-diff)+'px');
            } else {
              element.css('min-height', $(window).height()-diff);
            }
          };
          scope.$watch('docHeight', function (newVal, oldVal) {
            setHeight(newVal);
          });
          scope.$watch(function() { return $('header').height(); }, function (newVal, oldVal) {
            element.css('margin-top', newVal - 40);             // urggh that's kludgy! 
          });

          $(window).on('resize', function () {
            setHeight($(document).height());
          });
          var resetHeight = function () {
            scope.docHeight = $(document).height();
            // 23 11 2014 not sure if this is really needed? seems to create endless digest loop
            $timeout(resetHeight, 1000);
          }
          $timeout(resetHeight , 1000);
        }
      };
    }])

    .directive('backToTop', function () {
      return {
        restrict: 'AE',
        link: function (scope, element, attr) {
          element.click( function (e) {
            $('body').scrollTop(0);
          });
        }
      }
    })

}