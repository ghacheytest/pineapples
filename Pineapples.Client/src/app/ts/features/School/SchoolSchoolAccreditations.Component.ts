﻿namespace Pineapples.Schools {

  interface IBindings {
    schoolaccreditations: any;
  }

  class Controller implements IBindings {
    public schoolaccreditations: any;

    constructor() { }

    public $onChanges(changes) {
      if (changes.schoolaccreditations) {
        console.log('$onChanges: ', this.schoolaccreditations);
      }
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        schoolaccreditations: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "school/schoolaccreditations";
    }
  }

  angular
    .module("pineapples")
    .component("schoolAccreditations", new Component());
}