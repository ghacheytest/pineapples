﻿module Pineapples {
  interface ISchoolRowColDatum extends Sw.IRowColDatum {

    E: number;        // enrolment
    F: number;        // of females
    S: number;        // estimate;
  }

  export class SchoolTableCalculator extends Sw.TableCalculator {
    public getValue(datum: ISchoolRowColDatum, dataItem) {
      if (datum) {
        // check if we are returning an absolute or ratio or percentage
        let v: number = null;
        switch (dataItem) {
          case "enrol":
            v = datum.E;
            break;
          case "percF":
            v = datum.F / datum.E;
            break;
          default:
            v = super.getValue(datum, dataItem);
        }
        return v;
      }
      return null;
    }
  }
}
