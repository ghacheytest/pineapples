﻿namespace Pineapples.Documents {
  export class DocumentBaseController extends Pineapples.Documents.DocumentRenderer {
    public document: any;
    public height: number;
    public showMenu: boolean;

    static $inject = ["identity", "documentsAPI"]
    constructor(public identity: Sw.Auth.IIdentity, docAPI) {
      super(docAPI, "");
    }

    public $onChanges(changes) {
    }

    public $onInit() {
      this.showMenu = (this.showMenu === undefined ? true : this.showMenu);
    }

  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        document: '<',
        height: '<',
        showMenu: "<",
        documentChanged: "&"
      };
      this.controller = DocumentBaseController;
      this.controllerAs = "vm";
      this.templateUrl = "teacherlink/thumbnail";
    }
  }
  angular
    .module("pineapples")
    .component("componentThumbnail", new Component());
}