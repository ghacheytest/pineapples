﻿namespace Pineapples.Documents {
  export class DocumentRenderer {

    private documentChanged: (document) => void;

    constructor(public docApi, public missingImage: string) { }


    public identity: Sw.Auth.IIdentity;
    public isWord(docType) {
      // for fun and learning, do this with regex
      // these regex are set up to match either
      // a full path (ie including a dot) or the extension on its own
      // the two cases are need to 
      // - identfy the type of a selected local file
      // - identify the type of a document object, where the file type is in the property docType
      if (docType) {
        let reg = /^(doc|docx|docm|dot|dotx|dotm)$|\.(doc|docx|docm|dot|dotx|dotm)$/
        return reg.test(docType);
      }
      return false;
    }

    public isExcel(docType) {

      if (docType) {
        let reg = /^(xls|xlsx|xlsm)$|\.(xls|xlsx|xlsm)$/
        return reg.test(docType.toLowerCase());
      }
      return false;
    }

    public isExcelOpenXml(docType) {

      if (docType) {
        let reg = /^(xlsx|xlsm)$|\.(xlsx|xlsm)$/
        return reg.test(docType.toLowerCase());
      }
      return false;
    }

    public isPdf(docType) {
      if (docType) {
        let reg = /^(pdf)$|\.(pdf)$/
        return reg.test(docType.toLowerCase());
      }
      return false;
    }

    public isXml(docType) {
      if (docType) {
        let reg = /^(xml)$|\.(xml)$/
        return reg.test(docType.toLowerCase());
      }
      return false;
    }

    public isImage(docType: string) {
      if (docType) {
        let reg = /^(jpg|jpeg|png|tif|tiff|ico|bmp|gif)$|\.(jpg|jpeg|png|tif|tiff|ico|bmp|gif)$/
        return reg.test(docType.toLowerCase());
      }
      return false;
    }

    public isExecutable(docType: string) {
      if (docType) {
        let reg = /^(exe|com|dll|cpl|scr|bat|js|vbs|wsh)$|\.(exe|com|dll|cpl|scr|bat|js|vbs|wsh)$/
        return reg.test(docType.toLowerCase());
      }
      return false
    }
 
    /// construct the url to retrieve the given document
    public documentPath(doc: any): string;
    public documentPath(doc: any, height: number): string;
    public documentPath(doc: any, height?: number): string {

      if (!this.identity.isAuthenticated) {
        return "";
      }

      if (!height) {
        height = 1200;    // default
      }

      if (doc.docDeleted) {
        return null;
      }

      // these routes will be handled by imageprocessor if necessary
      // otherwise, will dropthrough to LibraryController 
      let url: string = "library/" + doc.docID
      url = url + "/" + doc.docRotate;
      if (height) {
        url = url + "/" + height;    // the server will determine the height when 0 is passed
      }
      return url + "?&t=" + this.identity.tokenhash(doc.docID);;
    }

    public thumbPath(doc, height?: number) {
      if (!this.identity.isAuthenticated) {
        return "";
      }

      if (!doc) {
        return this.missingImage;
      }
      let url: string = "thumb/";
      // for rendering suport during uploaders, support thumb from a file name - just to get the extension
      if (doc.docID) {
        url += doc.docID;
        url = url + "/" + doc.docRotate;
        if (height) {
          url = url + "/" + height;    // the server will determine the height when 0 is passed
        } 
        return url + "?&t=" + this.identity.tokenhash(doc.docID);
      } else {
        // it a simple file name e.g. of an uploaded file, or else a file token
        url += doc + '/0';
        if (height) {
          url = url + "/" + height;    // the server will determine the height when 0 is passed
        } 
        return url + "?&t=" + this.identity.tokenhash(doc);
      }
    }

    // allow a document to get rotated via this api
    public rotate(ev, doc, newRotate) {
      if (doc.docRotate) {
        doc.docRotate += newRotate;
      } else {
        doc.docRotate = newRotate;
      }

      if (doc.docRotate >= 360) {
        doc.docRotate -= 360;
      }
      this.docApi.update(doc).then(newData => {
        // this is needed to pick up the new rowversion
        angular.extend(doc, newData.plain());
        if (this.documentChanged) {
          this.documentChanged({ document: doc });
        }
      });
    }


  }
}