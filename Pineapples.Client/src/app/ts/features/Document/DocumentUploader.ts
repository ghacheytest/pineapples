﻿namespace Pineapples.Documents {
  export class DocumentUploader extends DocumentRenderer {

    // relies on the fileuploader component
    public uploader: angularFileUpload.FileUploader;     // the uploader created by fileuploader
    public model: any;        // data that will go with the uploaded file - bound from the ui

    constructor(public identity: Sw.Auth.IIdentity, docApi: any, private FileUploader:angularFileUpload.UploaderContructor, public mdDialog: ng.material.IDialogService ) {
      super(docApi, "assets/img/person-icon-5.png");
      this.uploader  = new FileUploader();
      // bind - normally we would use the typescript lambda form to do the bind(this)
      // However, that won't work in the circumstance where we want to call this base class implementation
      // from the child class using super.onBeforeUploadItem
      // in that case it has to be a function
      // but assigning that function to uploader makes uploader 'this' when it is called
      // adding bind(this) explicitly here means this is locked at the value it has when this code is executed
      // and that is the child class calling the super constructor
      this.uploader.onBeforeUploadItem = this.onBeforeUploadItem.bind(this);
      this.uploader.onErrorItem = this.onErrorItem.bind(this);
      this.uploader.onCancelItem = this.onCancelItem.bind(this);
      this.uploader.onCompleteItem = this.onCompleteItem.bind(this);
      this.uploader.onCompleteAll = this.onCompleteAll.bind(this);
      this.uploader.onWhenAddingFileFailed = this.onWhenAddingFileFailed.bind(this);
      this.uploader.onAfterAddingFile = this.onAfterAddingFile.bind(this);
      this.uploader.onAfterAddingAll = this.onAfterAddingAll.bind(this);
      this.uploader.onProgressItem = this.onProgressItem.bind(this);
      this.uploader.onProgressAll = this.onProgressAll.bind(this);
      this.uploader.onSuccessItem = this.onSuccessItem.bind(this);

      // filters - don;t allow any executable to be uploaded
      this.uploader.filters.push({
        name: "executable", fn: (file) => (!this.isExecutable(file.name)),
        msg: "Executable files cannot be uploaded"
      });
      this.uploader.queueLimit = 1; // only 1 at a time

    }
    public upload() {
      if (this.identity.isAuthenticated) {
        this.uploader.headers.Authorization = 'Bearer ' + this.identity.token;
      }
      this.uploader.uploadAll();
    }

    protected onWhenAddingFileFailed = (item, filter, options) => {
      console.info('onWhenAddingFileFailed', item, filter, options);
      let ariaLabel: string;
      let title: string;
      let msg: string;
      switch (filter.name) {
        case "queueLimit":
          title = "Upload queue full";
          ariaLabel = title;
          if (this.uploader.queueLimit == 1) {
            msg = "There is already a file in the upload queue.";
            msg += " Upload or clear the queue before loading a second file.";
          } else {
            msg = "The Upload queue is full (" + this.uploader.queueLimit + " maximum).";
            msg += " Upload or clear the queue before loading another file.";
          }
      
          break;
        default:
          msg = filter.msg;
          title = "File not valid";
          ariaLabel = msg;
      }
      this.mdDialog.show(
        this.mdDialog.alert()
          .clickOutsideToClose(true)
          .title(title)
          .textContent(msg)
          .ariaLabel(ariaLabel)
          .multiple(true)
          .ok('Close')
      );
    };
    protected onAfterAddingFile(fileItem: angularFileUpload.FileItem) {
      console.info('onAfterAddingFile', fileItem);
    };
    protected onAfterAddingAll(addedFileItems) {
      console.info('onAfterAddingAll', addedFileItems);
    };
    protected onProgressItem(fileItem:angularFileUpload.FileItem, progress) {
      console.info('onProgressItem', fileItem, progress);
    };
    protected onProgressAll(progress) {
      console.info('onProgressAll', progress);
    };
    protected onSuccessItem(fileItem: angularFileUpload.FileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
    };
    protected onCompleteAll() {
      console.info('onCompleteAll');
    };

    protected onBeforeUploadItem(item) {
      // fileuploader works directly with XmlHttpRequest, not via $http service
      // so, the angular interceptor that normally adds the bearer token is bypassed
      // hence, we have to treat this here....
      if (this.identity.isAuthenticated) {
        item.headers.Authorization = 'Bearer ' + this.identity.token;
      }
      // note that bindings that are not initialised in the component tag are still pushed to the controller as undefined
      item.formData.push({ "model": JSON.stringify(this.model) });
    }

    protected onErrorItem(fileItem: angularFileUpload.FileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
      let msg = response.Message;
      let title = "Error Uploading File";
      let ariaLabel = "Upload Error";
      this.mdDialog.show(
        this.mdDialog.alert()
          .clickOutsideToClose(true)
          .title(title)
          .textContent(msg)
          .ariaLabel(ariaLabel)
          .multiple(true)
          .ok('Close')
      );
    };

    protected onCancelItem(fileItem: angularFileUpload.FileItem, response, status, headers) {
      console.info('onCancelItem', fileItem, response, status, headers);
      this.uploader.removeFromQueue(fileItem);
    };

    protected onCompleteItem(fileItem: angularFileUpload.FileItem, response, status, headers) {
      console.info('onCompleteItem', fileItem, response, status, headers, this);
      this.uploader.removeFromQueue(fileItem);
    };

    // helper functions for dialogs
    public closeDialog(args) {
      this.mdDialog.cancel(args);
    }
  }
}