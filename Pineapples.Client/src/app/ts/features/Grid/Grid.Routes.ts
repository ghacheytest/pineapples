﻿module Pineapples {

  // TODO - These routes will be obsolete as the feature of surveys move towards work in feature/Survey/

  let routes = ($stateProvider: angular.ui.IStateProvider) => {
    $stateProvider
      .state("site.enrolment", {
        url: "/enrolment",
        views: {
          "@": {
            templateUrl: "survey/grid",
            controller: "gridController",
            controllerAs: "vm"
          }
        },
      });
    $stateProvider
      .state("site.gridpanel", {
        url: "^/gridpanel/{schoolNo}/{year}/{tableName}",
        views: {
          "@": {
            template: `<grid-panel school-no="{{shim.schoolNo}}" year="{{shim.year}}" table-name="{{shim.tableName}}"></grid-panel>`,
            controller: ['$stateParams', function (stateparams: ng.ui.IStateParamsService) {
              this.schoolNo = stateparams["schoolNo"];
              this.year = stateparams["year"];
              this.tableName = stateparams["tableName"];
            }],
            controllerAs: "shim"
          }
        }
      });
  };

  angular
    .module("pineapples")
    .config(["$stateProvider", routes]);
}
