﻿/* ------------------------------------------
    lookupsAPI
    ----------------------------------------
*/
namespace Pineapples.Lookups {

  class LookupsApi extends Sw.Lookups.LookupsApi {
    public pa = () => this._wreqService.customGET("collection/pa");
  }
  angular
    .module('pineapples')
    .service('lookupsAPI', LookupsApi);
}
