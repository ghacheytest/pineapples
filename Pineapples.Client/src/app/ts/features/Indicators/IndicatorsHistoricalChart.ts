﻿namespace Pineapples.Indicators {

  /**
   * @interface IBindings
   * @description What bindings are expected in the component's controller
   */
  interface IBindings {
    indicators: any;
    title: string;
    chartType: string;
    indicator: string;
    valueFormat: string;
    meta: Array<string>;
    ngDisabled: boolean;
  }

  /**
   * @interface IChartUnitData
   * @description A single atomic point on the chart. A Javascript item would look like
   * ```javascript
   * {
   *   svyYear: 2015,
   *   value: 0.98
   * }
   * ```
   */
  interface IChartUnitData {
    svyYear: number;
    value: number;
  }

  /**
   * @interface IChartDataset
   * @description A given dataset with associated metadata. It is is packaged in a way ready to be passed
   * to a Plottable.Dataset easily. A single item would look like
   * ```javascript
   * {
   *   ds: [{svyYear: 2015, value: 0.98}, {svyYear: 2016, value: 0.985}]
   *   meta: "NER (Male)"
   * }
   * ```  
   * @see IChartUnitData
   * @example new Plottable.Dataset(chartData.ds[0]) where chartData: IChartDataset
   */
  interface IChartDataset {
    ds: Array<IChartUnitData>;
    meta: string;
  }

  /**
   * @interface IChartData
   * @description Contains the chart datasets and some other information for the chart
   * @see IChartDataset
   */
  interface IChartData {
    title: string;
    indicator: string;
    datasets: Array<IChartDataset>;
  }

  class ChartController extends Sw.Charts.SizeableComponentController implements IBindings {

    // bindings
    public indicators: any;
    public title: string;
    public chartType: string;
    public indicator: string;
    public valueFormat: string;
    public meta: Array<string>;
    public ngDisabled: boolean;

    private _chartData: IChartData;
    private _tip;
    private lineChart: Plottable.XYPlot<any, any>;
    private dataPointChart: Plottable.XYPlot<any, any>;       // add a scatter chart to identify the data points on the lines
    private chartGroup: Plottable.Component;
    private chart: Plottable.Component;

    private paintAttr: string;                   // attribute that color is assigned to

    private indicatorsMgr: IndicatorsMgr;
    private indicatorCalc: IndicatorCalc;
    private selectedEdLevel: string;
    private selectedSector: string;
    private selectedDistrict: string;
    private years: Array<number>;


    // injections
    static $inject = ["$compile", "$timeout", "$scope", "$element"];

    constructor($compile: ng.ICompileService, $timeout: ng.ITimeoutService, scope: ng.IScope, element) {
      super($compile, $timeout, scope, element);

    }

    public $onChanges(changes: any) {
      console.log('$onChanges: ', changes);
      this.indicatorsMgr = this.indicators.indicatorsMgr;
      this.indicatorCalc = this.indicators.indicatorCalc;
      this.selectedEdLevel = this.indicators.selectedEdLevel;
      this.selectedSector = this.indicators.selectedSector;
      this.selectedDistrict = this.indicators.selectedDistrict;
      this.years = this.indicatorCalc.getYearsWithData();
      this._chartData = {
        title: this.title,
        indicator: this.indicator,
        datasets: [
          { ds: [], meta: this.meta[0] },
          { ds: [], meta: this.meta[1] },
          { ds: [], meta: this.meta[2] }
        ]
      };
    }

    /**
     * @method newSize
     * @override Sw.Charts.SizeableComponentController's newSize
     * @description applies some rounding so this is not responding to tiny fluctuations
     */
    public setSize(newSize: Sw.Charts.Size) {
      let scrollwidth = ((window.innerWidth - $(window).width())) || 0;

      this.size.width = Math.floor(newSize.width / 100) * 100 - (20 - scrollwidth);
      this.size.height = Math.floor((this.size.width * 2) / 5); // 2.5:1 aspect ratio is the default
    }

    /**
     * @method onResize
     * @override Sw.Charts.SizeableComponentController's onResize
     */
    public onResize(element) {
      if (this.size.height === 0) {
        return;
      }
      if (this.chart) {
        this.chart.redraw();
      } else {
        this.makeChart(element);
      }
    }

    public clickHandler(d) {
      alert(d.svyYear);
    }

    private format(value: number) {
      if (this.valueFormat) {
        return numeral(value).format(this.valueFormat);
      }
      return value.toString();
    }

    public makeChart = (element) => {
      let chartData: IChartData = this._chartData;
      let categoryScaleV = new Plottable.Scales.Category();
      let valueScaleV = new Plottable.Scales.Linear();
      let colorScaleV = new Plottable.Scales.Color();
      let categoryAxisV = new Plottable.Axes.Category(categoryScaleV, "bottom");
      categoryAxisV.tickLabelAngle(-90);
      let valueAxisV = new Plottable.Axes.Numeric(valueScaleV, "left");
      let legendV = new Plottable.Components.Legend(colorScaleV)
        .maxEntriesPerRow(3)
        .xAlignment('left');

      let indicatorsData: any = {}; // TODO - remove me when done

      // For each year and the current selectedEdLevel/selectedSector retrieve the data and fill up
      // the chartData
      this.years.forEach((year) => {
        let yearData = this.indicatorsMgr.get(this.indicatorCalc, year, this.selectedEdLevel, this.selectedSector, this.selectedDistrict);
        indicatorsData[year] = yearData; // TODO - remove me when done

        chartData.datasets[0].ds.push({ svyYear: year, value: yearData[chartData.indicator].M });
        chartData.datasets[1].ds.push({ svyYear: year, value: yearData[chartData.indicator].F });
        chartData.datasets[2].ds.push({ svyYear: year, value: yearData[chartData.indicator].T });
      });

      // Remove empty years (i.e. NaN returned from indicatorsMgr.get) from datasets. 
      // This is done here as this may vary 
      // (e.g.surveys submitted with some data but not all - repeaters)
      // Not so optimize but not sure what sort of data to expect across
      // countries, etc.
      chartData.datasets.forEach((i) => {
        if (i.ds) {
          _.remove(i.ds, function (unit: IChartUnitData) {
            return _.isNaN(unit.value);
          });
        }
      });

      var dsM = new Plottable.Dataset(chartData.datasets[0].ds).metadata(chartData.datasets[0].meta);
      var dsF = new Plottable.Dataset(chartData.datasets[1].ds).metadata(chartData.datasets[1].meta);
      var dsT = new Plottable.Dataset(chartData.datasets[2].ds).metadata(chartData.datasets[2].meta);

      let tooltipper = (d, i) => {
        return d.svyYear + ": " + this.format(d.value);
      };
      let tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(tooltipper);
      this._tip = tip;      // set aside so we can destroy it later...

      let compile = this.$compile;
      let scope = this.scope;
      let self = this;
      let callbackAnimate = function (jr: Plottable.Drawers.JoinResult, attr, drawer) {
        (<d3.selection.Update<any>>jr.merge)
          .on("mouseover", tip.show)
          .on("mouseout", tip.hide)
          .on("click", self.clickHandler)
          .call(function () {
            compile(this[0].parentNode)(scope);
          });

        this.innerAnimate(jr, attr, drawer);
      };
      let innerAnimator = new Plottable.Animators.Easing()
        .stepDuration(500)
        .stepDelay(0);
      let animator = new Plottable.Animators.Callback()
        .callback(callbackAnimate) // Deactivate tooltip and fix this in new issue
        .innerAnimator(innerAnimator);

      switch (this.chartType) {
        case "line":
          this.lineChart = new Plottable.Plots.Line();
          this.paintAttr = "stroke";
          break;
        case "stackedarea":
          this.lineChart = new Plottable.Plots.StackedArea()
          this.paintAttr = "fill";
          break;
        default:
          this.lineChart = new Plottable.Plots.Line();
          this.paintAttr = "stroke";
      }
      this.dataPointChart = new Plottable.Plots.Scatter();
     


      this.lineChart
        .addDataset(dsM)
        .addDataset(dsF)
        .addDataset(dsT)

      this.lineChart
        .x((d) => d.svyYear, categoryScaleV)
        .y((d) => d.value, valueScaleV)
        .attr(this.paintAttr, (d, i, dataset) => dataset.metadata(), colorScaleV)
        .attr("opacity", 1)
        .animated(false)

      this.dataPointChart
        .addDataset(dsM)
        .addDataset(dsF)
        .addDataset(dsT)

      this.dataPointChart
        .x((d) => d.svyYear, categoryScaleV)
        .y((d) => d.value, valueScaleV)
        .attr("fill", (d, i, dataset) => dataset.metadata(), colorScaleV)
        .attr("opacity", 1)
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, animator);

      this.chartGroup = new Plottable.Components.Group([this.lineChart, this.dataPointChart]);

      this.chart = new Plottable.Components.Table([
        [valueAxisV, this.chartGroup],
        [null, categoryAxisV],
        [null, legendV]
      ]);

      let div = $(element).find("div")
      let svg = $(element).find("div>svg");
      let d3svg = d3.selectAll(svg.toArray());
      d3svg.call(<any>tip);
      this.chart.renderTo(d3svg);
    }

  }

  class Chart implements ng.IComponentOptions {
    public bindings: any = {
      indicators: "<",
      title: "@",
      chartType: "@",
      indicator: "@",
      meta: "<",
      ngDisabled: "=",
      valueFormat: "@"
    };
    public controller: any = ChartController;
    public controllerAs: string = "vm";
    public template = `<div><svg width="{{vm.size.width}}" height="{{vm.size.height}}"></svg></div>`;
  }

  angular
    .module("pineapples")
    .component("chart", new Chart());

}