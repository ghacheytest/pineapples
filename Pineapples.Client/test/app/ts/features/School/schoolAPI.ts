﻿describe("At A Glance Manager", () => {
  beforeEach(() => {
    angular.mock.module("pineapples");
  });
  it("is defined in pineapples", () => {
    inject((_AtAGlanceMgr_) => {
      expect(_AtAGlanceMgr_).toBeDefined();
    });
  });
});