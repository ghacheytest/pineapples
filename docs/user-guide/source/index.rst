.. Pacific EMIS User Guide documentation master file, created by
   sphinx-quickstart on Wed May 04 10:00:15 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pacific EMIS User Guide
=======================

Contents:

.. toctree::
   :maxdepth: 2

.. _esurvey:

TODO Develop a generic simple Pacific EMIS User Guide for the online
application that can be used by any country adopting the Pacific
EMIS. This user guide should be simple; only including description of
how to use the system and essentially nothing else. James wrotes some
bits of it embedded in some early reports though the application's UI
changed and will continue to change as the product improves over
time. What can be taken from James work will and much will have to be
rewritten.

Pacific EMIS Overview
=====================

TODO

Brief History
-------------

TODO

Accessing and Login
-------------------

TODO

Overview of Features (Modules)
------------------------------

TODO

Education Indicators (At a Glance)
==================================

TODO

Managing Schools
================

TODO

Managing Teachers
=================

TODO

Managing Performance Assessment
===============================

TODO


Managing Surveys
================

Online Web Application Survey Submission
----------------------------------------

TODO

PDF eSurvey Submission
----------------------

The PDF eSurvey is simply a PDF document that teachers and principals
can use to enter the data of their surveys instead of completing this
process on paper. This new technology as a number of advantages:

* Easier work for data entry officers as the need to re-enter all data
  in the database is now no longer necessary. Data entry officers can
  now better use their time for verification and more creative
  analysis and use of the Pacific EMIS.
* PDF eSurveys can be completed by anyone with a tablet or laptop and
  there is no need for a reliable Internet connection

  



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

