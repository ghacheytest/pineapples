\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Project Overview}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Project Structure}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Programming Language}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Development Web Stack}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Database and Operating System}{3}{section.1.5}
\contentsline {chapter}{\numberline {2}Setting Up the Development Environment}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Main Development Tools}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Software Configuration Management}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Visual Studio Setup}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Databases Setup}{6}{section.2.4}
\contentsline {section}{\numberline {2.5}Building the Solution}{7}{section.2.5}
\contentsline {section}{\numberline {2.6}Frontend Development Tools}{8}{section.2.6}
\contentsline {chapter}{\numberline {3}Development Work-flow}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Contribute New Code in Branches Uninterrupted}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Contribute New Code in Branches Interrupted}{14}{section.3.2}
\contentsline {section}{\numberline {3.3}Database Schema Changes}{14}{section.3.3}
\contentsline {section}{\numberline {3.4}Testing Code}{14}{section.3.4}
\contentsline {chapter}{\numberline {4}High Level Architecture}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Data Retrieval Service}{15}{section.4.1}
\contentsline {section}{\numberline {4.2}Templates}{17}{section.4.2}
\contentsline {section}{\numberline {4.3}Theme (Look and Feel)}{18}{section.4.3}
\contentsline {section}{\numberline {4.4}Navigation and Module System}{18}{section.4.4}
\contentsline {section}{\numberline {4.5}Lookups and Configuration}{21}{section.4.5}
\contentsline {chapter}{\numberline {5}Low Level Documentation and API}{23}{chapter.5}
\contentsline {chapter}{\numberline {6}eSurvey Technology}{25}{chapter.6}
\contentsline {section}{\numberline {6.1}Creating/Editing the PDF eSurvey}{25}{section.6.1}
\contentsline {section}{\numberline {6.2}PDF eSurvey Manager}{26}{section.6.2}
\contentsline {section}{\numberline {6.3}PDF eSurvey Process}{26}{section.6.3}
\contentsline {chapter}{\numberline {7}Security Considerations}{29}{chapter.7}
\contentsline {section}{\numberline {7.1}Authentication}{29}{section.7.1}
\contentsline {section}{\numberline {7.2}Authorization}{30}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Implementing Authorization as Claims}{30}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Server-side Implementation}{31}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Client-side Implementation}{31}{subsection.7.2.3}
\contentsline {subsection}{\numberline {7.2.4}Assigning Permissions}{31}{subsection.7.2.4}
\contentsline {section}{\numberline {7.3}SSL/TLS Encryption}{31}{section.7.3}
\contentsline {section}{\numberline {7.4}Latest Top 10 Security Risks}{31}{section.7.4}
\contentsline {section}{\numberline {7.5}Integrated Penetration Testing}{31}{section.7.5}
\contentsline {chapter}{\numberline {8}Project Documentation}{33}{chapter.8}
\contentsline {chapter}{\numberline {9}Indices and tables}{35}{chapter.9}
\contentsline {chapter}{Bibliography}{37}{chapter*.6}
