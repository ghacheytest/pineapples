SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paIndicatorLevels_](
	[paplID] [int] IDENTITY(1,1) NOT NULL,
	[paindID] [int] NULL,
	[paplCode] [nvarchar](1) NULL,
	[paplValue] [int] NULL,
	[paplDescription] [nvarchar](50) NULL,
	[paplCriterion] [nvarchar](400) NULL,
	[paplSort] [nvarchar](3) NULL
) ON [PRIMARY]
GO

