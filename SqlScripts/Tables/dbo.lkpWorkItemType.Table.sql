SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpWorkItemType](
	[codeCode] [nvarchar](15) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[witUseBuilding] [int] NOT NULL,
	[witBuildingType] [nvarchar](50) NULL,
	[witCreateBuilding] [int] NOT NULL,
	[witEstUnitCost] [money] NULL,
	[witEstUOM] [nvarchar](10) NULL,
	[witEstFormula] [nvarchar](100) NULL,
	[witEstIM] [bit] NOT NULL,
	[witGroup] [nvarchar](10) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [lkpWorkItemType_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpWorkItemType] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpWorkItemType] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpWorkItemType] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpWorkItemType] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpWorkItemType] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [codeDescription] ON [dbo].[lkpWorkItemType]
(
	[codeDescription] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [codeDEscriptionL1] ON [dbo].[lkpWorkItemType]
(
	[codeDescriptionL1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [codeDescriptionL2] ON [dbo].[lkpWorkItemType]
(
	[codeDescriptionL2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[lkpWorkItemType] ADD  CONSTRAINT [DF_lkpWorkItemType_witUseBuilding]  DEFAULT ((0)) FOR [witUseBuilding]
GO
ALTER TABLE [dbo].[lkpWorkItemType] ADD  CONSTRAINT [DF_lkpWorkItemType_witCreateBuilding]  DEFAULT ((0)) FOR [witCreateBuilding]
GO
ALTER TABLE [dbo].[lkpWorkItemType] ADD  CONSTRAINT [DF_lkpWorkItemType_witestIM]  DEFAULT ((0)) FOR [witEstIM]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=dont prompt for building,1=prompt for building,2=building is mandatory;9=building not allowed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWorkItemType', @level2type=N'COLUMN',@level2name=N'witUseBuilding'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'default building type for this type of work' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWorkItemType', @level2type=N'COLUMN',@level2name=N'witBuildingType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=don''t prompt to create building;1=prompt to create building;2=bulding mandatory at completion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWorkItemType', @level2type=N'COLUMN',@level2name=N'witCreateBuilding'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of work that may be done on a work item. The work item type defines a range of characteristic of the work: how to cost it, whether a building is required, whether a new building may be created, how to count the buildings that will be created if any. Foreign key on WorkITems - mandatory.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWorkItemType'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Work Orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWorkItemType'
GO

