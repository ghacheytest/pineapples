SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpEducationLevels](
	[codeCode] [nvarchar](50) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[edlMinYear] [int] NULL,
	[edlMaxYear] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpEducationLevels1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpEducationLevels] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpEducationLevels] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpEducationLevels] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpEducationLevels] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpEducationLevels] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpEducationLevels] ADD  CONSTRAINT [DF__lkpEducat__edlMi__160F4887]  DEFAULT ((0)) FOR [edlMinYear]
GO
ALTER TABLE [dbo].[lkpEducationLevels] ADD  CONSTRAINT [DF__lkpEducat__edlMa__17036CC0]  DEFAULT ((0)) FOR [edlMaxYear]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Education Levels represent chronolgical devisions of the education system, based on Year of Edcuation. Each level defines a min and max year of education. A class Level belogns to an Edcuation Level if its Year of Edcuation is between this Min and Max. 
Year of Education 1 is first year of primary. Year 0 is ECE.
Enrolment Ratio reporting is done by Education Level, since the chornoligical dvisions allows the calcuation of the relevant "official age" population.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpEducationLevels'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpEducationLevels'
GO

