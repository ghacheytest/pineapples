SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpShifts](
	[shiftCode] [nvarchar](10) NOT NULL,
	[shiftName] [nvarchar](50) NULL,
	[shiftNameL1] [nvarchar](50) NULL,
	[shiftNameL2] [nvarchar](50) NULL,
	[shiftSeq] [int] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_lkpShifts] PRIMARY KEY CLUSTERED 
(
	[shiftCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

