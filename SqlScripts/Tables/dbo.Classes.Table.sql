SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Classes](
	[pcID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[pcCat] [nvarchar](1) NULL,
	[pcSeq] [int] NULL,
	[pcHrsWeek] [numeric](18, 2) NULL,
	[pcLang] [nvarchar](5) NULL,
	[rmID] [int] NULL,
	[subjCode] [nvarchar](10) NULL,
	[shiftCode] [nvarchar](10) NULL,
 CONSTRAINT [aaaaaPrimaryClass1_PK] PRIMARY KEY NONCLUSTERED 
(
	[pcID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Classes] TO [pEnrolmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Classes] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Classes] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Classes] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Classes] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_PrimaryClass_SSID] ON [dbo].[Classes]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Classes] ADD  CONSTRAINT [DF__PrimaryCla__ssID__324172E1]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[Classes] ADD  CONSTRAINT [DF__PrimaryCl__pcSeq__3335971A]  DEFAULT ((0)) FOR [pcSeq]
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD  CONSTRAINT [FK_Classes_lkpLanguage] FOREIGN KEY([pcLang])
REFERENCES [dbo].[lkpLanguage] ([langCode])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Classes] CHECK CONSTRAINT [FK_Classes_lkpLanguage]
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD  CONSTRAINT [FK_Classes_lkpShifts] FOREIGN KEY([shiftCode])
REFERENCES [dbo].[lkpShifts] ([shiftCode])
GO
ALTER TABLE [dbo].[Classes] CHECK CONSTRAINT [FK_Classes_lkpShifts]
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD  CONSTRAINT [FK_Classes_lkpSubjects] FOREIGN KEY([subjCode])
REFERENCES [dbo].[lkpSubjects] ([subjCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Classes] CHECK CONSTRAINT [FK_Classes_lkpSubjects]
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD  CONSTRAINT [PrimaryClass_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Classes] CHECK CONSTRAINT [PrimaryClass_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Classes holds a record for each class conducted in the school. The class record specifies the subject, hours per week, language of instruction, and Room. 
Related tables ClassTeachers and ClassLevels hold the teacher(s) of the class, and enrolments in the class in each level.
Therefore, this format can handle composite and joint teacher classes, general primary classes, secondary subject specific classes etc. 
Eacerly version of this data used the Primary Class format. Secondary capture was introduced in the VU 2010 survey in this format. This data is also imported from Pineapples@School' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Classes'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'xml in: pEnrolmentWrite.updateClasses

xml: /Survey/Classes/Class' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Classes'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Survey form, Pineapples@School' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Classes'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Classes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Classes'
GO

