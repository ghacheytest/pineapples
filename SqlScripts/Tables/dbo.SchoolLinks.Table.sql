SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolLinks](
	[lnkID] [int] IDENTITY(1,1) NOT NULL,
	[schNo] [nvarchar](50) NULL,
	[lnkDate] [datetime] NULL,
	[lnkTitle] [nvarchar](80) NULL,
	[lnkSummary] [nvarchar](255) NULL,
	[lnkSrc] [nvarchar](50) NULL,
	[lnkPath] [nvarchar](255) NULL,
	[lnkImg] [bit] NOT NULL,
	[lnkKeywords] [nvarchar](255) NULL,
	[bldID] [int] NULL,
 CONSTRAINT [aaaaaSchoolLinks1_PK] PRIMARY KEY NONCLUSTERED 
(
	[lnkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolLinks] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolLinks] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolLinks] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolLinks] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolLinks] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SchoolLinks_SchNo] ON [dbo].[SchoolLinks]
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolLinks] ADD  CONSTRAINT [DF__SchoolLin__lnkIm__27C3E46E]  DEFAULT ((0)) FOR [lnkImg]
GO
ALTER TABLE [dbo].[SchoolLinks]  WITH CHECK ADD  CONSTRAINT [FK_SchoolLinks_Building] FOREIGN KEY([bldID])
REFERENCES [dbo].[Buildings] ([bldID])
GO
ALTER TABLE [dbo].[SchoolLinks] CHECK CONSTRAINT [FK_SchoolLinks_Building]
GO
ALTER TABLE [dbo].[SchoolLinks]  WITH CHECK ADD  CONSTRAINT [SchoolLinks_FK00] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SchoolLinks] CHECK CONSTRAINT [SchoolLinks_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main table of school images. Each image is linked to one school, and optionally one building in that school. 
Additional 0object may link to the same image or file by creating an objectlink record to reference the record in SchoolLinks.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolLinks'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Images' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolLinks'
GO

