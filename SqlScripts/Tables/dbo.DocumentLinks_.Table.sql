SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentLinks_](
	[lnkID] [int] IDENTITY(1,1) NOT NULL,
	[schNo] [nvarchar](50) NULL,
	[tID] [int] NULL,
	[docID] [uniqueidentifier] NOT NULL,
	[lnkFunction] [nvarchar](20) NULL,
	[lnkHidden] [int] NOT NULL,
	[pCreateTag] [uniqueidentifier] NULL,
 CONSTRAINT [PK_DocumentLinks_] PRIMARY KEY CLUSTERED 
(
	[lnkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SchoolLinks_School] ON [dbo].[DocumentLinks_]
(
	[schNo] ASC,
	[docID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TeacherLinks_Teacher] ON [dbo].[DocumentLinks_]
(
	[tID] ASC,
	[docID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DocumentLinks_] ADD  CONSTRAINT [DF_SchoolLinks_dlnkHidden]  DEFAULT ((0)) FOR [lnkHidden]
GO
ALTER TABLE [dbo].[DocumentLinks_] ADD  CONSTRAINT [DF__SchoolLin__pCrea__0683B78B]  DEFAULT (newid()) FOR [pCreateTag]
GO
ALTER TABLE [dbo].[DocumentLinks_]  WITH CHECK ADD  CONSTRAINT [FK_DocumentLinks__Schools] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[DocumentLinks_] CHECK CONSTRAINT [FK_DocumentLinks__Schools]
GO
ALTER TABLE [dbo].[DocumentLinks_]  WITH CHECK ADD  CONSTRAINT [FK_DocumentLinks__Teachers] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
GO
ALTER TABLE [dbo].[DocumentLinks_] CHECK CONSTRAINT [FK_DocumentLinks__Teachers]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link Key' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DocumentLinks_', @level2type=N'COLUMN',@level2name=N'lnkID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'School Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DocumentLinks_', @level2type=N'COLUMN',@level2name=N'schNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Document identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DocumentLinks_', @level2type=N'COLUMN',@level2name=N'docID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category of this document' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DocumentLinks_', @level2type=N'COLUMN',@level2name=N'lnkFunction'
GO

