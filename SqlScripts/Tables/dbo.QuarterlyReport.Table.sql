SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuarterlyReport](
	[qrID] [int] NOT NULL,
	[qrNumDaysOpen] [real] NULL,
	[qrAggDaysAtt] [real] NULL,
	[qrAggDaysAbs] [real] NULL,
	[qrAggDaysMem]  AS ([qrAggDaysAtt]+[qrAggDaysAbs]),
	[qrAvgDailyAtt]  AS ([qrAggDaysAtt]/[qrNumDaysOpen]),
	[qrAvgDailyMem] [real] NULL,
	[qrTotEnrolToDate] [int] NULL,
	[qrPupilEnrolLastDay] [int] NULL,
	[qrAggDaysAttM] [real] NULL,
	[qrAggDaysAttF] [real] NULL,
	[qrAggDaysAbsM] [real] NULL,
	[qrAggDaysAbsF] [real] NULL,
	[qrAggDaysMemM]  AS ([qrAggDaysAttM]+[qrAggDaysAbsM]),
	[qrAggDaysMemF]  AS ([qrAggDaysAttF]+[qrAggDaysAbsF]),
	[qrAvgDailyAttM]  AS ([qrAggDaysAttM]/[qrNumDaysOpen]),
	[qrAvgDailyAttF]  AS ([qrAggDaysAttF]/[qrNumDaysOpen]),
	[qrAvgDailyMemM] [real] NULL,
	[qrAvgDailyMemF] [real] NULL,
	[qrTotEnrolToDateM] [int] NULL,
	[qrTotEnrolToDateF] [int] NULL,
	[qrDropoutM] [int] NULL,
	[qrDropoutF] [int] NULL,
	[qrDropout] [int] NULL,
	[qrTrinM] [int] NULL,
	[qrTrinF] [int] NULL,
	[qrTrin] [int] NULL,
	[qrTroutM] [int] NULL,
	[qrTroutF] [int] NULL,
	[qrTrout] [int] NULL,
	[qrGrad8M] [int] NULL,
	[qrGrad8F] [int] NULL,
	[qrGrad8] [int] NULL,
	[qrGrad12M] [int] NULL,
	[qrGrad12F] [int] NULL,
	[qrGrad12] [int] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NOT NULL,
	[pCreateTag] [uniqueidentifier] NULL,
 CONSTRAINT [PK__Quarterl__FF4B98F4B72E5C26] PRIMARY KEY CLUSTERED 
(
	[qrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrAgg__114071C9]  DEFAULT (NULL) FOR [qrAggDaysAttM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrAgg__12349602]  DEFAULT (NULL) FOR [qrAggDaysAttF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrAgg__1328BA3B]  DEFAULT (NULL) FOR [qrAggDaysAbsM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrAgg__141CDE74]  DEFAULT (NULL) FOR [qrAggDaysAbsF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrAvg__151102AD]  DEFAULT (NULL) FOR [qrAvgDailyMemM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrAvg__160526E6]  DEFAULT (NULL) FOR [qrAvgDailyMemF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrTot__16F94B1F]  DEFAULT (NULL) FOR [qrTotEnrolToDateM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrTot__17ED6F58]  DEFAULT (NULL) FOR [qrTotEnrolToDateF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrDro__18E19391]  DEFAULT (NULL) FOR [qrDropoutM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrDro__19D5B7CA]  DEFAULT (NULL) FOR [qrDropoutF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrDro__1AC9DC03]  DEFAULT (NULL) FOR [qrDropout]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrTri__1BBE003C]  DEFAULT (NULL) FOR [qrTrinM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrTri__1CB22475]  DEFAULT (NULL) FOR [qrTrinF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrTri__1DA648AE]  DEFAULT (NULL) FOR [qrTrin]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrTro__1E9A6CE7]  DEFAULT (NULL) FOR [qrTroutM]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrTro__1F8E9120]  DEFAULT (NULL) FOR [qrTroutF]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrTro__2082B559]  DEFAULT (NULL) FOR [qrTrout]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrGra__2176D992]  DEFAULT (NULL) FOR [qrGrad8M]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrGra__226AFDCB]  DEFAULT (NULL) FOR [qrGrad8F]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrGra__235F2204]  DEFAULT (NULL) FOR [qrGrad8]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrGra__2453463D]  DEFAULT (NULL) FOR [qrGrad12M]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrGra__25476A76]  DEFAULT (NULL) FOR [qrGrad12F]
GO
ALTER TABLE [dbo].[QuarterlyReport] ADD  CONSTRAINT [DF__Quarterly__qrGra__263B8EAF]  DEFAULT (NULL) FOR [qrGrad12]
GO
ALTER TABLE [dbo].[QuarterlyReport]  WITH CHECK ADD  CONSTRAINT [FK__QuarterlyR__qrID__71E7C201] FOREIGN KEY([qrID])
REFERENCES [dbo].[SchoolInspection] ([inspID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[QuarterlyReport] CHECK CONSTRAINT [FK__QuarterlyR__qrID__71E7C201]
GO

