SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherUnidentified](
	[tuID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[seq] [int] NULL,
	[PayrollNo] [nvarchar](50) NULL,
	[FamilyName] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NULL,
	[Gender] [nvarchar](1) NULL,
	[DoB] [date] NULL,
	[MaritalStatus] [nvarchar](50) NULL,
	[Dep] [int] NULL,
	[Citizenship] [nvarchar](50) NULL,
	[HomeIsland] [nvarchar](2) NULL,
	[House] [nvarchar](1) NULL,
	[Role] [nvarchar](50) NULL,
	[Duties] [nvarchar](2) NULL,
	[ClassMin] [nvarchar](50) NULL,
	[ClassMax] [nvarchar](50) NULL,
	[Qual] [nvarchar](20) NULL,
	[QualEd] [nvarchar](20) NULL,
	[YearStarted] [int] NULL,
	[YearsTeaching] [int] NULL,
	[InserviceYear] [int] NULL,
	[Inservice] [nvarchar](50) NULL,
	[FP] [nvarchar](1) NULL,
	[Days] [int] NULL,
	[EmpStatus] [nvarchar](2) NULL,
	[PaidBy] [nvarchar](50) NULL,
	[Salary] [money] NULL,
	[tID] [int] NULL,
	[tuUpdateIdentity] [int] NULL,
 CONSTRAINT [PK_TeacherUnidentified] PRIMARY KEY CLUSTERED 
(
	[tuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[TeacherUnidentified] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherUnidentified] TO [pSurveyWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherUnidentified] TO [pTeacherRead] AS [dbo]
GO
ALTER TABLE [dbo].[TeacherUnidentified] ADD  CONSTRAINT [DF_TeachersUnidentified_tuUpdateIdentity]  DEFAULT ((0)) FOR [tuUpdateIdentity]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'if 1 , update the Identity record with the values on here when loading the survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherUnidentified', @level2type=N'COLUMN',@level2name=N'tuUpdateIdentity'
GO

