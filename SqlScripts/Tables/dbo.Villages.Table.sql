SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Villages](
	[vilID] [int] IDENTITY(1,1) NOT NULL,
	[vilName] [nvarchar](50) NULL,
	[iCode] [nvarchar](2) NULL,
 CONSTRAINT [aaaaaVillages1_PK] PRIMARY KEY NONCLUSTERED 
(
	[vilID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON [dbo].[Villages] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Villages by ID. Not used. May be required for a future census.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Villages'
GO

