SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpLanguage](
	[langCode] [nvarchar](5) NOT NULL,
	[langName] [nvarchar](50) NULL,
	[langGrp] [nvarchar](5) NULL,
	[langSystem] [bit] NULL,
	[langNameL1] [nvarchar](50) NULL,
	[langNameL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpLanguage1_PK] PRIMARY KEY NONCLUSTERED 
(
	[langCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpLanguage] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpLanguage] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpLanguage] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpLanguage] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpLanguage] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpLanguage] ADD  CONSTRAINT [DF__lkpLangua__langS__56B3DD81]  DEFAULT ((0)) FOR [langSystem]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Languages spoken. May categorise teachers, classes, and schools.
Users may also be assigned a language, which can determine the system language for display.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpLanguage'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Language' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpLanguage'
GO

