SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnrolmentProjectionData](
	[epdID] [int] IDENTITY(1,1) NOT NULL,
	[epID] [int] NULL,
	[epdAge] [int] NULL,
	[epdLevel] [nvarchar](10) NULL,
	[epdM] [int] NULL,
	[epdF] [int] NULL,
	[epdU] [int] NULL,
	[epdSum]  AS ((isnull([epdM],(0))+isnull([epdF],(0)))+isnull([epdU],(0))),
 CONSTRAINT [aaaaaEnrolmentProjectionData1_PK] PRIMARY KEY NONCLUSTERED 
(
	[epdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[EnrolmentProjectionData] TO [pEnrolmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[EnrolmentProjectionData] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EnrolmentProjectionData] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EnrolmentProjectionData] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EnrolmentProjectionData] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_EnrolmentProjectionData_epID] ON [dbo].[EnrolmentProjectionData]
(
	[epID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EnrolmentProjectionData] ADD  CONSTRAINT [DF__EnrolmentP__epID__2C538F61]  DEFAULT ((0)) FOR [epID]
GO
ALTER TABLE [dbo].[EnrolmentProjectionData] ADD  CONSTRAINT [DF__Enrolment__epdAg__2D47B39A]  DEFAULT ((0)) FOR [epdAge]
GO
ALTER TABLE [dbo].[EnrolmentProjectionData] ADD  CONSTRAINT [DF__EnrolmentP__epdM__2E3BD7D3]  DEFAULT ((0)) FOR [epdM]
GO
ALTER TABLE [dbo].[EnrolmentProjectionData] ADD  CONSTRAINT [DF__EnrolmentP__epdF__2F2FFC0C]  DEFAULT ((0)) FOR [epdF]
GO
ALTER TABLE [dbo].[EnrolmentProjectionData]  WITH CHECK ADD  CONSTRAINT [EnrolmentProjectionData_FK00] FOREIGN KEY([epID])
REFERENCES [dbo].[EnrolmentProjection] ([epID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EnrolmentProjectionData] CHECK CONSTRAINT [EnrolmentProjectionData_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EnrolmentProjectionData holds the projection numbers. This may be split by Gender, and/or class level on this table. enrolmentProjectionData has epID as foreign key, link to the EnrolmentProjection that defines the target area (school, district) and year.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentProjectionData'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Enrolment Scenario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentProjectionData'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment Projection' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnrolmentProjectionData'
GO

