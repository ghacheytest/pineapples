SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[tableDisability](
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[GenderCode] [nvarchar](1) NOT NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[SchoolTypeCode] [nvarchar](10) NOT NULL,
	[DisCode] [nvarchar](50) NULL,
	[Disability] [nvarchar](50) NULL,
	[Disab] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Disability information.
Normalised by Gender, aggregated by District, Authority, School Type, Class Level.

Allows split by disability code, or aggregate for total disabled regardless of disability type.
' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'tableDisability'
GO

