SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EFTableLevels](
	[efTable] [nvarchar](10) NOT NULL,
	[eflLevel] [nvarchar](10) NOT NULL,
 CONSTRAINT [EFTableLevels_PK] PRIMARY KEY NONCLUSTERED 
(
	[efTable] ASC,
	[eflLevel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EFTableLevels] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[EFTableLevels] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EFTableLevels] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EFTableLevels] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EFTableLevels] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EFTableLevels] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [EFTablesEFTableLevels] ON [dbo].[EFTableLevels]
(
	[efTable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [lkpLevelsEFTableLevels] ON [dbo].[EFTableLevels]
(
	[eflLevel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EFTableLevels]  WITH CHECK ADD  CONSTRAINT [EFTablesEFTableLevels] FOREIGN KEY([efTable])
REFERENCES [dbo].[EFTables] ([efTable])
GO
ALTER TABLE [dbo].[EFTableLevels] CHECK CONSTRAINT [EFTablesEFTableLevels]
GO
ALTER TABLE [dbo].[EFTableLevels]  WITH CHECK ADD  CONSTRAINT [lkpLevelsEFTableLevels] FOREIGN KEY([eflLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
GO
ALTER TABLE [dbo].[EFTableLevels] CHECK CONSTRAINT [lkpLevelsEFTableLevels]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The set of class levels that make up an Establishment Formula Table. The total enrolment for the table is summed across these levels, then used to determine the Band.
A level can and often is assigned to more than one Table. Pineapples selects any table that includes some enrolments; and from each table group; selects the table with the smallest set of Levels that incudes all levels appearing against any Table for that group in which the school has enrolment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTableLevels'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Enrolment Formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTableLevels'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment Formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTableLevels'
GO

