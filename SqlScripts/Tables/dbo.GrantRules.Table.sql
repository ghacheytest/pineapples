SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GrantRules](
	[grCode] [nvarchar](20) NOT NULL,
	[grName] [nvarchar](50) NULL,
	[grSchType] [nvarchar](10) NULL,
	[grdID] [nvarchar](5) NULL,
	[grlstName] [nvarchar](20) NULL,
	[grlstValue] [nvarchar](10) NULL,
	[grNameL1] [nvarchar](50) NULL,
	[grNameL2] [nvarchar](50) NULL,
	[grBoarder] [nvarchar](1) NULL,
	[grRuleClass] [nvarchar](1) NULL,
	[grExtensionAction] [nchar](1) NOT NULL,
 CONSTRAINT [GrantRules_PK] PRIMARY KEY NONCLUSTERED 
(
	[grCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[GrantRules] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[GrantRules] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[GrantRules] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[GrantRules] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[GrantRules] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [DistrictsGrantRules] ON [dbo].[GrantRules]
(
	[grdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [ListsGrantRules] ON [dbo].[GrantRules]
(
	[grlstName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [SchoolTypesGrantRules] ON [dbo].[GrantRules]
(
	[grSchType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GrantRules] ADD  CONSTRAINT [DF__GrantRule__grRul__427A3CAF]  DEFAULT ('S') FOR [grRuleClass]
GO
ALTER TABLE [dbo].[GrantRules] ADD  CONSTRAINT [DF_GrantRules_grExtensionAction]  DEFAULT (N'D') FOR [grExtensionAction]
GO
ALTER TABLE [dbo].[GrantRules]  WITH CHECK ADD  CONSTRAINT [DistrictsGrantRules] FOREIGN KEY([grdID])
REFERENCES [dbo].[Districts] ([dID])
GO
ALTER TABLE [dbo].[GrantRules] CHECK CONSTRAINT [DistrictsGrantRules]
GO
ALTER TABLE [dbo].[GrantRules]  WITH CHECK ADD  CONSTRAINT [ListsGrantRules] FOREIGN KEY([grlstName])
REFERENCES [dbo].[Lists] ([lstName])
GO
ALTER TABLE [dbo].[GrantRules] CHECK CONSTRAINT [ListsGrantRules]
GO
ALTER TABLE [dbo].[GrantRules]  WITH CHECK ADD  CONSTRAINT [SchoolTypesGrantRules] FOREIGN KEY([grSchType])
REFERENCES [dbo].[SchoolTypes] ([stCode])
GO
ALTER TABLE [dbo].[GrantRules] CHECK CONSTRAINT [SchoolTypesGrantRules]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'how to treat this rule when applied to an extension school' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantRules', @level2type=N'COLUMN',@level2name=N'grExtensionAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GrantRules, together with GrantValues, provide the metadata used to calculate school grants.
Rules fall into 2 classes, those relating to school grants, those relating to Authority Grants.

Rules can be restricted to a School Type, a District, or schools that are members of a named List. The Grant Rule also specifies whether to calculate the amount based on total enrolments, or boarders only.

GrantValues specifies the value of the Grant in each year.

When the grant is calculated for a school, the value is calculated according to each Grant Rule. The rule creates a line item in the Grant (SchoolGrantItems). This line item is annotated with the Grant Rule name.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantRules'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'Linked table/bound' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantRules'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Rules are maintained from the Lookup table group Funding and Finance.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantRules'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'School Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantRules'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'School Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantRules'
GO

