SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ObjectLinks](
	[xlinkID] [int] IDENTITY(1,1) NOT NULL,
	[objectType] [nvarchar](20) NOT NULL,
	[objectID] [int] NOT NULL,
	[lnkID] [int] NOT NULL,
	[xlinkNote] [nvarchar](255) NULL,
 CONSTRAINT [ObjectLinks_PK] PRIMARY KEY NONCLUSTERED 
(
	[xlinkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[ObjectLinks] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinks] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinks] TO [pAdminWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[ObjectLinks] TO [pExamWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinks] TO [pExamWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinks] TO [pExamWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinks] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinks] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[ObjectLinks] TO [pInspectionWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinks] TO [pInspectionWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinks] TO [pInspectionWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ObjectLinks] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ObjectLinks] TO [pSchoolWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[ObjectLinks] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ObjectLinks] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [lnkID] ON [dbo].[ObjectLinks]
(
	[lnkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [objectID] ON [dbo].[ObjectLinks]
(
	[objectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ObjectLinks]  WITH CHECK ADD  CONSTRAINT [SchoolLinksObjectLinks] FOREIGN KEY([lnkID])
REFERENCES [dbo].[SchoolLinks] ([lnkID])
GO
ALTER TABLE [dbo].[ObjectLinks] CHECK CONSTRAINT [SchoolLinksObjectLinks]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointers to couple objects from other tables - Work Orders, Assessments, with SchoolLink records that define a picture or linked file. ObjectType defines the source of the object, ObjectId is its primary key in that table. Therfore, this cannot be formally a foreign key.
lnkID is the primary key in SchoolLinks.
This format allows various objects to reference the same file. However, because the school ( and building) are on the SchoolLinks, only one school can reference a single SchoolLink.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ObjectLinks'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Images' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ObjectLinks'
GO

