SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpBuildingValuationClass](
	[bvcCode] [nvarchar](10) NOT NULL,
	[bvcDescription] [nvarchar](50) NULL,
	[bvcDescriptionL1] [nvarchar](50) NULL,
	[bvcDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_lkpBuildingValuationClass] PRIMARY KEY CLUSTERED 
(
	[bvcCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpBuildingValuationClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpBuildingValuationClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpBuildingValuationClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpBuildingValuationClass] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpBuildingValuationClass] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Calssification of buildings for the purpose of valuation. Foreign key on Buildings and BuildingReview' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingValuationClass'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingValuationClass'
GO

