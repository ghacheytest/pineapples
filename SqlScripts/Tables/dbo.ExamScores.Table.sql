SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamScores](
	[exsID] [int] IDENTITY(1,1) NOT NULL,
	[exID] [int] NULL,
	[schNo] [nvarchar](50) NULL,
	[exsGender] [nvarchar](1) NULL,
	[exsBand] [nvarchar](50) NULL,
	[exsCandidates] [int] NULL,
	[exsScore] [int] NULL,
	[exsMin] [int] NULL,
	[exsMax] [int] NULL,
	[exsSch] [int] NULL,
 CONSTRAINT [aaaaaExamScores1_PK] PRIMARY KEY NONCLUSTERED 
(
	[exsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ExamScores] TO [pExamRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ExamScores] TO [pExamWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ExamScores] TO [pExamWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ExamScores] TO [pExamWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ExamScores] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ExamScores] ADD  CONSTRAINT [DF__ExamScores__exID__1A34DF26]  DEFAULT ((0)) FOR [exID]
GO
ALTER TABLE [dbo].[ExamScores] ADD  CONSTRAINT [DF__ExamScore__exsCa__1B29035F]  DEFAULT ((0)) FOR [exsCandidates]
GO
ALTER TABLE [dbo].[ExamScores] ADD  CONSTRAINT [DF__ExamScore__exsSc__1C1D2798]  DEFAULT ((0)) FOR [exsScore]
GO
ALTER TABLE [dbo].[ExamScores] ADD  CONSTRAINT [DF__ExamScore__exsMi__1D114BD1]  DEFAULT ((0)) FOR [exsMin]
GO
ALTER TABLE [dbo].[ExamScores] ADD  CONSTRAINT [DF__ExamScore__exsMa__1E05700A]  DEFAULT ((0)) FOR [exsMax]
GO
ALTER TABLE [dbo].[ExamScores] ADD  CONSTRAINT [DF__ExamScore__exsSc__1EF99443]  DEFAULT ((0)) FOR [exsSch]
GO
ALTER TABLE [dbo].[ExamScores]  WITH CHECK ADD  CONSTRAINT [ExamScores_FK00] FOREIGN KEY([exID])
REFERENCES [dbo].[Exams] ([exID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ExamScores] CHECK CONSTRAINT [ExamScores_FK00]
GO
ALTER TABLE [dbo].[ExamScores]  WITH CHECK ADD  CONSTRAINT [ExamScores_FK01] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ExamScores] CHECK CONSTRAINT [ExamScores_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Exam Scores holds the aggregate scores achieved by students in an exam. exId is the foreign key, linking to Exams.

Data is stored by School and Gender, and score band. The number of candidates at the school, of the gender who scored in that band is held, toegheter with various aggregates for those students ( enabling calculation of averages). 
Aggregate score is a function of the scoring model asscoiated to the exam.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamScores'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamScores'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamScores'
GO

