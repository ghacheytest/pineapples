SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpCodesMisc](
	[codeType] [nvarchar](50) NULL,
	[codeCode] [nvarchar](50) NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeNum] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpCodesMisc] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpCodesMisc] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpCodesMisc] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpCodesMisc] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpCodesMisc] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous codes that do not have their own lookup table. CodesMisc is categorised by a codeType field.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpCodesMisc'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Admin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpCodesMisc'
GO

