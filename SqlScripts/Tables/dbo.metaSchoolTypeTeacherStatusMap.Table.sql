SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTypeTeacherStatusMap](
	[ststatID] [int] IDENTITY(1,1) NOT NULL,
	[stCode] [nvarchar](10) NULL,
	[statusCode] [nvarchar](5) NULL,
	[ststatSort] [int] NULL,
 CONSTRAINT [aaaaametaSchoolTypeTeacherStatusMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ststatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTypeTeacherStatusMap] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTypeTeacherStatusMap] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeTeacherStatusMap] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTypeTeacherStatusMap] TO [pTeacherAdmin] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTypeTeacherStatusMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherStatusMap] ADD  CONSTRAINT [DF__metaSchoo__ststa__7BE56230]  DEFAULT ((0)) FOR [ststatSort]
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherStatusMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeTeacherStat_FK00] FOREIGN KEY([statusCode])
REFERENCES [dbo].[lkpTeacherStatus] ([statusCode])
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherStatusMap] CHECK CONSTRAINT [metaSchoolTypeTeacherStat_FK00]
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherStatusMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeTeacherStat_FK01] FOREIGN KEY([stCode])
REFERENCES [dbo].[SchoolTypes] ([stCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherStatusMap] CHECK CONSTRAINT [metaSchoolTypeTeacherStat_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TeacherStatus values that may apply to a school type. Foreign keys are teacher status and school type. Controls dropdowns on Teacher Survey form.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeTeacherStatusMap'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeTeacherStatusMap'
GO

