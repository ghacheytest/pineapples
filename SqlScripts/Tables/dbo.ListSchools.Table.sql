SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListSchools](
	[lstName] [nvarchar](20) NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[lstValue] [nvarchar](10) NULL,
 CONSTRAINT [aaaaaListSchools1_PK] PRIMARY KEY NONCLUSTERED 
(
	[lstName] ASC,
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ListSchools] TO [pSchoolReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[ListSchools] TO [pSchoolWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[ListSchools] TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ListSchools] TO [pSchoolWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ListSchools] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ListSchools] ADD  CONSTRAINT [DF__ListSchoo__lstVa__14E61A24]  DEFAULT ('Y') FOR [lstValue]
GO
ALTER TABLE [dbo].[ListSchools]  WITH CHECK ADD  CONSTRAINT [ListSchools_FK00] FOREIGN KEY([lstName])
REFERENCES [dbo].[Lists] ([lstName])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ListSchools] CHECK CONSTRAINT [ListSchools_FK00]
GO
ALTER TABLE [dbo].[ListSchools]  WITH CHECK ADD  CONSTRAINT [ListSchools_FK01] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ListSchools] CHECK CONSTRAINT [ListSchools_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'School in a list. Foreign keys are the list code and the SchNo. There may be a values associated with membership of the list, or a simple Y  (ie IN in the list or not)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ListSchools'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ListSchools'
GO

