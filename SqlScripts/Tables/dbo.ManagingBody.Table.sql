SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManagingBody](
	[symbID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[symbName] [nvarchar](100) NULL,
	[symbRole] [nvarchar](250) NULL,
	[symbMeet] [int] NULL,
 CONSTRAINT [aaaaaManagingBody1_PK] PRIMARY KEY NONCLUSTERED 
(
	[symbID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ManagingBody] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ManagingBody] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ManagingBody] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ManagingBody] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ManagingBody] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_ManagingBody_SSID] ON [dbo].[ManagingBody]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ManagingBody]  WITH CHECK ADD  CONSTRAINT [ManagingBody_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ManagingBody] CHECK CONSTRAINT [ManagingBody_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On tertiary survey, list of committees or organisations at the institution. ssId is the foreign key' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ManagingBody'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ManagingBody'
GO

