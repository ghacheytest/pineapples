SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [workflow].[Flows](
	[flowID] [int] IDENTITY(1,1) NOT NULL,
	[flowName] [nvarchar](50) NULL,
	[flowInitialStatus] [nvarchar](10) NULL,
	[flowExistingTeacher] [bit] NOT NULL,
	[flowNewTeacher] [bit] NOT NULL,
	[flowDescription] [ntext] NULL,
	[flowRoleCreate] [nvarchar](50) NULL,
	[flowActive] [bit] NOT NULL,
 CONSTRAINT [metawrkFlows_PK] PRIMARY KEY NONCLUSTERED 
(
	[flowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [workflow].[Flows] ADD  CONSTRAINT [DF_Flows_flowExistingTeacher]  DEFAULT ((1)) FOR [flowExistingTeacher]
GO
ALTER TABLE [workflow].[Flows] ADD  CONSTRAINT [DF_Flows_flowNewTeacher]  DEFAULT ((0)) FOR [flowNewTeacher]
GO
ALTER TABLE [workflow].[Flows] ADD  CONSTRAINT [DF_Flows_flowActive]  DEFAULT ((1)) FOR [flowActive]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A Flow is a workflow ,composed of a set of actions, that may generate particular Outcomes, advancing the status of the flow. 

Used by POR to describe various pathways of PORs.' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'Flows'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Workflow and POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'Flows'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'Flows'
GO

