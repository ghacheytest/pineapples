SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpChildProtection](
	[cpCode] [nvarchar](10) NOT NULL,
	[cpDesc] [nvarchar](50) NULL,
	[cpGrp] [nvarchar](10) NULL,
	[cpSeq] [int] NULL,
	[cpDescL1] [nvarchar](50) NULL,
	[cpDescL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpChildProtection1_PK] PRIMARY KEY NONCLUSTERED 
(
	[cpCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpChildProtection] TO [pEnrolmentAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpChildProtection] TO [pEnrolmentAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpChildProtection] TO [pEnrolmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpChildProtection] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpChildProtection] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Child protection groups and items for the Vanuatu Child Protection survey. Appears on PupilTables.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpChildProtection'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpChildProtection'
GO

