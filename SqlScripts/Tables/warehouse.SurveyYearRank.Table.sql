SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[SurveyYearRank](
	[SurveyYear] [int] NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[rankDistrict] [nvarchar](10) NULL,
	[rankSchType] [nvarchar](10) NOT NULL,
	[Enrol] [int] NULL,
	[EnrolM] [int] NULL,
	[EnrolF] [int] NULL,
	[Estimate] [bit] NULL,
	[rankDistrictSchoolType] [int] NULL,
	[rankDistrictSchoolTypeDec] [nvarchar](2) NULL,
	[rankDistrictSchoolTypeQtl] [nvarchar](2) NULL,
	[rankSchoolType] [int] NULL,
	[rankSchoolTypeDec] [nvarchar](2) NULL,
	[rankSchoolTypeQtl] [nvarchar](2) NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Warehouse source for ranking by size.

Ranks are calculated at both District and national level, but always within school type.

At both levels, the able holds 3 values:

rank - numeric value which is the rank by enrolment
rankDec - value in 01 02 03 .........10 which is the decile of the school based on enrolment

rankQtl - value in Q1 Q2 Q3 Q4 which is the quartile of the school based on enrolment.

Table also holds the rankDistict and rankSchType which are the sdistrict and school type used when making this calculation.

Enrol and EnrolM EnrolF are here to allow agreggation by Decile or Quartile.

This table can also be considered as providing the Dec and Qartiles as classifiers for the school; for example, you could report on relative level of teacher qualification by decile.

' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'SurveyYearRank'
GO

