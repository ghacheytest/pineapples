SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpInfrastructureClass](
	[isCode] [nvarchar](10) NOT NULL,
	[isDesc] [nvarchar](50) NULL,
	[isDescL1] [nvarchar](50) NULL,
	[isDescL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaalkpInfrastructureClass1_PK] PRIMARY KEY NONCLUSTERED 
(
	[isCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpInfrastructureClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpInfrastructureClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpInfrastructureClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpInfrastructureClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpInfrastructureClass] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpInfrastructureClass] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Class of a school for NIS. Together with Size, determines the NIS Standards that will apply to the school' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInfrastructureClass'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInfrastructureClass'
GO

