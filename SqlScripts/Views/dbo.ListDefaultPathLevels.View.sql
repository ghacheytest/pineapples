SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW ListDefaultPathLevels
AS

SELECT EducationPathLevels.levelCode
, TRLevels.codeDescription AS [Level]
, TRLevels.lvlYear AS YearOfEd
FROM EducationPaths
INNER JOIN EducationPathLevels
	ON EducationPaths.pathCode = EducationPathLevels.pathCode
INNER JOIN TRLevels
	ON TRLevels.codeCode = EducationPathLevels.levelCode

WHERE EducationPaths.pathDefault=1
GO

