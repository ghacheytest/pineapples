SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolLifeYearsRoomType]
AS
SELECT
schoolLifeYears.*,
lkpRoomTypes.codeCode AS rmType
FROM lkpRoomTypes, schoolLifeYears
GO

