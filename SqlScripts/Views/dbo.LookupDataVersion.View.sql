SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LookupDataVersion]
AS
SELECT     MAX(TableVersion) AS DataVersion
FROM         dbo.TableDataVersion
GO

