SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblSizeEst]
AS
SELECT
ESTIMATE.LifeYear AS [Survey Year],
ESTIMATE.schNo,
ESTIMATE.bestYear AS [Year OF Data],
ESTIMATE.Estimate,
ESTIMATE.Offset AS [Age Of Data],
ESTIMATE.bestssID,
SchoolSurvey.ssSizeSite,
SchoolSurvey.ssSizePlayground,
SchoolSurvey.ssSizeFarm,
SchoolSurvey.ssSizeRating
FROM
ESTIMATE_BestSurveySize
AS ESTIMATE INNER JOIN SchoolSurvey
ON ESTIMATE.bestssID = SchoolSurvey.ssID
GO

