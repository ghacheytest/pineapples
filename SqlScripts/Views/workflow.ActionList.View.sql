SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [workflow].[ActionList]
as


SELECT
	POR.*
	, isnull(tFullName,portFullName) porFullName
	, Schools.schName
	, TeacherIdentity.tRegister
	, TeacherIdentity.tPayroll
	, TeacherIdentity.tProvident
	, TeacherIdentity.tNamePrefix
	, TeacherIdentity.tGiven
	, TeacherIdentity.tMiddleNames
	, TeacherIdentity.tSurname
	, TeacherIdentity.tNameSuffix
	, TeacherIdentity.tFullName
	, Flows.flowName
	, FlowActions.wfqID
	, porStatus.statusDesc
	, Actions.actID
	, Actions.actName
	, Actions.actRole
	, Actions.actType
	, Actions.actArg
	, UserRoleMembership.urIntray urIntray
	, case when UserRoleMembership.userName is null then 0 else 1 end CanAction
	, suser_sname() as suser_sname
FROM
	POR
	INNER JOIN workflow.Flows Flows
		ON POR.flowID = Flows.flowID
	LEFT JOIN workflow.FlowActions FlowActions
		on POR.flowID = FlowActions.flowID
		AND POR.porStatus = FlowActions.wfqStatus
	LEFT JOIN workflow.Actions Actions
		ON FlowActions.actID = Actions.actID
	INNER JOIN workflow.porStatus porStatus
		ON POR.porStatus = porStatus.statusCode
	INNER JOIN Schools
		ON POR.schNo = Schools.schNo
	LEFT JOIN
		workflow.UserRoles UserRoleMembership

		ON Actions.actRole = UserRoleMembership.roleName
		AND UserRoleMembership.userName = suser_sname()
	LEFT JOIN TeacherIdentity
		ON POR.tID = TeacherIdentity.tID
GO

