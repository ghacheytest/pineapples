SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tchsIDQualifiedCertified]
as
-- this commented code finds the sector from the Role
----Select tchsID
----, tchRole
----, secCode
----, SS.svyYear
----, case
----	when TQE.ytqCertified = 1 then 'Y'
----	when TQNE.ytqCertified = 1 then 'Y'
----	else 'N'
----end Certified
----,  case
----	when TQE.ytqQualified = 1 then 'Y'
----	when TQNE.ytqQualified = 1 then 'Y'
----	else 'N'
----end Qualified
----from TeacherSurvey TS
----LEFT JOIN SchoolSurvey SS
----	ON TS.ssID = SS.ssID
----LEFT JOIN lkpTeacherRole TR
----	ON TS.tchRole = TR.codeCode
----LEFT JOIN SurveyYearTeacherQual TQE
----	ON TQE.svyYEar = SS.svyYear
----	AND TQE.ytqSector = TR.secCode
----	AND TQE.ytqQual = TS.tchEdQual
----LEFT JOIN SurveyYearTeacherQual TQNE
----	ON TQNE.svyYEar = SS.svyYear
----	AND TQNE.ytqSector = TR.secCode
----	AND TQNE.ytqQual = TS.tchQual
Select tchsID
, tchRole
, tchSector secCode
, SS.schNo
, SS.ssID
, SS.svyYear
, case
	when TQE.ytqCertified = 1 then 'Y'
	when TQNE.ytqCertified = 1 then 'Y'
	else 'N'
end Certified
,  case
	when TQE.ytqQualified = 1 then 'Y'
	when TQNE.ytqQualified = 1 then 'Y'
	else 'N'
end Qualified
from TeacherSurvey TS
LEFT JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID

LEFT JOIN SurveyYearTeacherQual TQE
	ON TQE.svyYEar = SS.svyYear
	AND TQE.ytqSector = TS.tchSector
	AND TQE.ytqQual = TS.tchEdQual
LEFT JOIN SurveyYearTeacherQual TQNE
	ON TQNE.svyYEar = SS.svyYear
	AND TQNE.ytqSector = TS.tchSector
	AND TQNE.ytqQual = TS.tchQual
GO

