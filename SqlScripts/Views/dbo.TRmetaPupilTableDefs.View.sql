SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRmetaPupilTableDefs]
AS
SELECT     tdefCode, ISNULL(CASE dbo.userLanguage() WHEN 0 THEN tdefName WHEN 1 THEN tdefNameL1 WHEN 2 THEN tdefNameL2 END, tdefName)
                      AS tdefName, tdefDataCode, tdefRows, tdefCols, tdefGen, tdefRowTotals, tdefColTotals
				, tdefHideCol1, tdefSrc
				, tdefShading
				, tdefNameL1, tdefNameL2
				,tdefPermissionsReqR, tdefPermissionsReqW
FROM         dbo.metaPupilTableDefs
GO
GRANT SELECT ON [dbo].[TRmetaPupilTableDefs] TO [public] AS [dbo]
GO

