SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblChildProtection]
AS
SELECT     ptID,ssID,
			ptCode,
			ptRow cpItem ,
           ptLevel,ptM,ptF
FROM       dbo.PupilTables
Where	dbo.PupilTables.ptCode Between 'CP:' And 'CP:ZZ'
GO

