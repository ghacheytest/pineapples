SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Teacher Count
--
-- Teachers, disaggregated by District, SchoolType, Authority, Sector
-- age group, gender
-- When used in a cube, this allows aggregation on any of these dimensions.
-- Shows total number of teachers, number certified, number qualified, number both certified and qualified
-- This is a wrapper around warehouse.schoolTeacherCount, which has the same data by school
-- =============================================
CREATE VIEW [warehouse].[TeacherCount]
AS
Select SurveyYear
, DistrictCode
, SchoolTypeCode
, AuthorityCode
, GenderCode
, AgeGroup
, Sector
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual		-- both certified and qualified
from warehouse.schoolTeacherCount
GROUP BY SurveyYear,  DistrictCode, SchoolTypeCode, AuthorityCode, GenderCode, Sector, AgeGroup
GO

