SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2007
-- Description:	Dimension Language
--
-- Im systems where classes may be classified by language of instruction, provides
-- a dimension for reporting and grouping on language / language group hierarchy
-- =============================================
CREATE VIEW [dbo].[DimensionLanguage]
AS
SELECT		LangCode LanguageCode,
			langName [Language],
			langGrp LanguageGroupCode,
			lnggrpName LanguageGroup

FROM
	dbo.TRLanguage
	Inner JOIN TRLanguageGroup
		ON dbo.TRLanguage.langGrp = dbo.TRLanguageGroup.lngGrp
GO
GRANT SELECT ON [dbo].[DimensionLanguage] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[DimensionLanguage] TO [public] AS [dbo]
GO

