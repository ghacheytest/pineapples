SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qrySurveyList]
as
/*
	simple view of surveys and total enrolments
*/

SELECT top 100 percent
SchoolSurvey.ssID,
Schools.schName,
SchoolSurvey.svyYear,
SchoolSurvey.schNo,
SchoolSurvey.ssSchType,
schoolSurvey.ssEnrol Enrol,
Schools.schType
FROM Schools INNER JOIN SchoolSurvey
	ON Schools.schNo = SchoolSurvey.schNo
ORDER BY SchoolSurvey.svyYear
GO

