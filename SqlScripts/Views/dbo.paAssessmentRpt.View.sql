SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paAssessmentRpt]
WITH VIEW_METADATA
AS
Select A.*
, s.iCode
, T.tDoB
, T.tSex
, T.tPayroll
, I.iName
, D.dID
, D.dName
, AUTH.*
, schType
from paAssessment A
	INNER JOIN TeacherIdentity T
		ON A.tID = T.tID
	INNER JOIN Schools S
		ON A.paSchNo = S.schNo
	INNER JOIN DimensionAuthority AUTH
			ON AUTH.AuthorityCode = S.schAuth
	LEFT JOIN Islands I
		ON S.iCode = I.iCode
	LEFT JOIN Districts D
		ON I.iGroup = D.dID
GO

