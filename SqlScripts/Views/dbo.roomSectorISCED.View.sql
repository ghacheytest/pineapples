SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	busness rules for determining the sector and isced classification of a room
--
-- =============================================
-- Uses the following fields:
--- from Rooms, the rmSchLevel ( for school teaching as multiple school types)
--- rmLevel ( the level taught in the room)
---- the school type from the school survey
--- LOGIC:
-- 1) If every class level taught in the school type has the same ISCED use that ISCED
-- 2) if rmLevel is specified, use the ISCED for that level
-- 3) if rmLevel not specified, but a room school type (rmSchLevel),
----   a) use the ICED of the minimum level matching the rmschlevel ,that is taught in the school that year
----   b) if no enrolments, use the ISCED of the minimum level matching the rmschlevel
-- 4) use the ICED of the minimum level matching the school type code ,that is taught in the school that year
-- 5) if no enrolments,  use the ISCED of the minimum level matching the school type code
CREATE VIEW [dbo].[roomSectorISCED]
AS
SELECT
rmID,
svyYear,
schNo,
ssSchType,
rmSchLevel,
rmLevel,
case
	when not (stILSCode is null) then stILSCode
	when not (LevelILSCode is null) then LevelILSCode
	when not (RoomstMinilsCode is null) then RoomstMinilsCode
	else stMinILSCode
end roomISCED,

case
	when not (stGV is null) then stGV
	when not (LevelGV is null) then LevelGV
	when not (RoomstGV is null) then RoomstGV
	else stGV
end roomGV
FROM
(
SELECT R.rmID,
SS.svyYear,
SS.schNo,
SS.ssSchType,
R.rmSchLevel,
R.rmLevel,
lkpLevels.ilsCode LevelilsCode,
lkpLevels.lvlGV LevelGV,


SILS.UniqueILSCode stILSCode,
SILS.MinILSCode stMinILSCode,
SILS.UniqueGV stGV ,
RILS.UniqueILSCode RoomstILSCode,
RILS.MinILSCode RoomstMinILSCode,
RILS.UniqueGV RoomstGV

FROM Rooms R
INNER JOIN SchoolSurvey SS
	ON r.ssID = SS.ssID
INNER JOIN SchoolTypeILSCode SILS
	ON SS.ssSchType = SILS.stCode
LEFT JOIN SchoolTypeILSCode RILS
	on r.rmschLevel = RILS.stCode
LEFT JOIN lkpLevels
	ON R.rmLevel = lkpLevels.codeCode
) subQ
GO

