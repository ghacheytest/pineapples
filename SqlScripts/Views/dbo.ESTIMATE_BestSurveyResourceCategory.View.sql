SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ESTIMATE_BestSurveyResourceCategory]
AS
SELECT     schNo, LifeYear, subQ2.resName, ActualssID,
			bestssID,
			bestYear, LifeYear - bestYear AS Offset,
			bestssqLevel,
			QI.ssqLevel ActualssqLevel,
			isnull(bestssID, ActualssID) SurveydimensionssID,
		CASE
			WHEN (BestYear IS NULL) THEN NULL
            WHEN LifeYear = BestYear THEN 0
			WHEN QI.ssqLevel = 2 then 2
			ELSE 1
		END AS Estimate
FROM
	(SELECT     schNo, LifeYear, subQ.resName, ActualssID,
		MIN(xBestData) AS BestData,
		dbo.fnextractBestssID(MIN(xBestData)) AS bestssID,
		dbo.fnextractBestYear(MIN(xBestData)) AS bestYear,
		dbo.fnExtractBestSSQLevel(MIN(xBestData)) AS bestSSQLevel

     FROM  (SELECT     L.schNo, L.svyYear AS LifeYear, L.ActualssID,
			R.mresCatCode as resName, D.svyYear AS DataYear,
			dbo.fnMakeBestDataStr(L.svyYear, D.svyYear, D.ssID, D.ssqLevel) AS xBestData
			FROM dbo.SchoolLifeYears  AS L
				Cross JOIN dbo.MetaResourceCategories as R
				LEFT OUTER JOIN
				dbo.schoolYearHasDataResourceCategory AS D
				ON L.schNo = D.schNo
				AND r.mresCatCode = d.resName
				AND D.svyYear BETWEEN
                L.svyYear - dbo.sysParamInt(N'EST_RESOURCE_FILL_FORWARD') AND L.svyYear + dbo.sysParamInt(N'EST_RESOURCE_FILL_BACKWARD')
			) AS subQ
	 GROUP BY schNo, LifeYear, ActualssID, subQ.resName
	) AS subQ2
	LEFT OUTER JOIN dbo.tfnQualityIssues('Facilities',null) QI
	ON subQ2.ActualssID = QI.ssID
GO

