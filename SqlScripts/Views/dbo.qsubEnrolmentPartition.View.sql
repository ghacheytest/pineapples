SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qsubEnrolmentPartition]
AS
SELECT
dbo.Partitions.ptSet,
dbo.Partitions.ptName,
--dbo.qsumSchoolEnrollments.ssID
-- in sql we maintain the totals for the schoolsurvey using a trigger on enrolments
-- so the calculation is not required here
ssID
FROM
--dbo.qsumSchoolEnrollments,
SchoolSurvey,
dbo.Partitions
WHERE
(((dbo.Partitions.ptSet)='Enrolment')
AND
((SchoolSurvey.ssEnrol) Between [ptMin] And [ptMax]))
GO
GRANT SELECT ON [dbo].[qsubEnrolmentPartition] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[qsubEnrolmentPartition] TO [public] AS [dbo]
GO

