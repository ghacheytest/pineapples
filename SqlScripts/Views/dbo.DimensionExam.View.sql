SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Dimension - Exams
--
-- Provides descriptive information about exams
-- Used in building warehouse.
-- Join to a data source on exCode or exID
-- =============================================
CREATE VIEW [dbo].[DimensionExam]
AS
SELECT
	dbo.Exams.exCode AS [Exam Code]
	, dbo.lkpExamTypes.exName AS [Exam Name]
	, dbo.Exams.exYear AS [Exam Year]
	, dbo.Exams.exID AS [Exam ID]
FROM
	dbo.lkpExamTypes
	INNER JOIN
		dbo.Exams ON dbo.lkpExamTypes.exCode = dbo.Exams.exCode
GO

