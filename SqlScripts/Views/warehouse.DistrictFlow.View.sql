SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	District Flow
--
-- Wrapper around warehouse.districtCohort to include flow calculations
-- ie PromoteRate, RepeatRate, SurvivalRate (single year survival aka "transition rate")
-- This version is intended to be used in reports, use districtCohort for "cubes"
-- (pivot tables, tableau) where ratios should be calculated based on current aggregations.
-- TRANSFER versions:
-- field named xxxxTR calculate the same ratios but attempt to take into account internal transfers
-- These should be used with caution as internal transfer data may not be very reliable
--
-- =============================================
CREATE VIEW [warehouse].[DistrictFlow]
WITH VIEW_METADATA
AS

Select S.*
, 1 - RepeatRate - PromoteRate DropoutRate
, 1 - RepeatRateTR - PromoteRateTR DropoutRateTR
, case when RepeatRate = 1 then null else PromoteRate / (1 - RepeatRate) end SurvivalRate
, case when RepeatRateTR = 1 then null else PromoteRateTR / (1 - RepeatRateTR) end SurvivalRateTR
FROM
(
	Select C.*
	, convert(float,RepNY) /Enrol RepeatRate
	, convert(float,RepNY) / (Enrol - isnull(TroutNY,0)) RepeatRateTR
	, ( convert(float,EnrolNYNextLevel) - RepNYNextLevel)  / Enrol PromoteRate
	, ( convert(float,EnrolNYNextLevel) - RepNYNextLevel - - isnull(TrinNYNextLevel,0))  / ( Enrol - isnull(TroutNY,0)) PromoteRateTR
	from warehouse.DistrictCohort C

) S
GO

