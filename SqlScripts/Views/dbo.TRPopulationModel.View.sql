SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRPopulationModel]
AS
SELECT     popmodCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN popmodName
	WHEN 1 THEN popmodNameL1
	WHEN 2 THEN popmodNameL2

END,popmodName) AS popmodName,popmodDesc,popmodSource
, popmodDefault
, popmodEFA

FROM         dbo.PopulationModel
GO
GRANT SELECT ON [dbo].[TRPopulationModel] TO [public] AS [dbo]
GO

