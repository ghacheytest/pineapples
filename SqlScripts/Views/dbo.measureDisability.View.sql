SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measureDisability]
AS
SELECT schNo
, LifeYear SurveyYear
, Estimate
, SurveyDimensionssID SurveyDimensionID
, ptLevel ClassLevel
, disCode
, ptM RepM
, ptF RepF
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN vtblDisabilities R
		ON B.bestssID = R.ssID
GO

