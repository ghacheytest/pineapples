SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblResourcesEst]
AS
SELECT
ESTIMATE.LifeYear AS [Survey Year],
ESTIMATE.schNo,
ESTIMATE.bestYear AS [Year Of Data],
ESTIMATE.Estimate,
ESTIMATE.Offset AS [Age Of Data],
ESTIMATE.bestssID,
ESTIMATE.ActualssID,
ESTIMATE.SurveyDimensionssID,
Resources.*,
ESTIMATE.bestssqLevel AS DataQualityLevel,
ESTIMATE.ActualssqLevel AS SurveyYearQualityLevel
FROM
ESTIMATE_BestSurveyResourceCategory AS ESTIMATE
INNER JOIN Resources ON (ESTIMATE.resName = Resources.resName)
AND (ESTIMATE.bestssID = Resources.ssID)
GO
GRANT SELECT ON [dbo].[vtblResourcesEst] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[vtblResourcesEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[vtblResourcesEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[vtblResourcesEst] TO [pSchoolWrite] AS [dbo]
GO

