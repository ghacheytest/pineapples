SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSchoolSurveySub]
AS
SELECT
dbo.Schools.schNo,
ss.ssID,
dbo.Schools.iCode,
dbo.Schools.schType,
IsNull(ss.ssSchType,[schType]) AS ssSchType,
IsNull(ss.ssAuth,[schAuth]) AS ssAuth,
IsNull(ss.ssLang,[schLang]) AS ssLang,
IsNull(ss.ssElectL,[schElectL]) AS ssElectL,
IsNull(ss.ssElectN,[schElectN]) AS ssElectN
FROM
dbo.Schools LEFT JOIN dbo.SchoolSurvey AS ss ON Schools.schNo = ss.schNo
GO

