SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[schoolYearHasDataEnrolments]
AS
SELECT     S.schNo, S.svyYear,
			S.ssID EnrolssID,
			S.ssSchType ,
			S.ssEnrol SSenrol,
			Q.ssqLevel
FROM         dbo.SchoolSurvey AS S LEFT OUTER JOIN
                      dbo.tfnQualityIssues('Pupils', 'Enrol') AS Q ON S.ssID = Q.ssID
WHERE     ssEnrol > 0

			and (q.ssqLevel is null or q.ssqLevel <2)
GO

