SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblReaders]
AS
SELECT
Resources.resID,
Resources.ssID,
Resources.resName,
Resources.resLevel,
Resources.resSplit,
Resources.resNumber,
Resources.resCondition
FROM dbo.Resources
WHERE (((Resources.resName)='Readers'))
GO

