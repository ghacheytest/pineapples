SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRSwitchboard]
AS
SELECT     SwitchboardID,
ItemNumber,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN ItemText
	WHEN 1 THEN ItemTextL1
	WHEN 2 THEN ItemTextL2

END,ItemText) AS ItemText,Command,Argument,PermissionGroup

FROM         dbo.[Switchboard Items]
GO
GRANT SELECT ON [dbo].[TRSwitchboard] TO [public] AS [dbo]
GO

