SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblRooms]
AS
SELECT
Rooms.rmID,
Rooms.ssID,
Rooms.rmType,
Rooms.rmNo,
Rooms.rmSchLevel,
Rooms.rmSize,
Rooms.rmYear,
Rooms.rmMaterials,
Rooms.rmCondition,
Rooms.rmLevel,
Rooms.rmShareType,
lkpMaterialTypes.codeDescription AS Material,
lkpMaterialTypes_1.codeDescription AS MaterialRoof,
lkpMaterialTypes_2.codeDescription AS MaterialFloor
FROM ((Rooms LEFT JOIN lkpMaterialTypes
ON Rooms.rmMaterials = lkpMaterialTypes.codeCode)
LEFT JOIN lkpMaterialTypes AS lkpMaterialTypes_1
ON Rooms.rmMaterialRoof = lkpMaterialTypes_1.codeCode)
LEFT JOIN lkpMaterialTypes AS lkpMaterialTypes_2
ON Rooms.rmMaterialFloor = lkpMaterialTypes_2.codeCode
GO

