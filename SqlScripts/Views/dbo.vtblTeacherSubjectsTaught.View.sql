SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblTeacherSubjectsTaught]
AS
SELECT     dbo.TeacherSurvey.tchsID,
CASE
	WHEN [num] = 1 THEN [tchSubjectMajor]
	ELSE [tchSubjectMinor]
	END AS Subject,
CASE
	WHEN [num] = 1 THEN 'Major'
	WHEN [num] = 2 THEN 'Minor'
	ELSE 'Minor'
END AS Workload

FROM         dbo.TeacherSurvey CROSS JOIN dbo.metaNumbers
WHERE
(CASE
	  WHEN [num] = 1 THEN 'Major'
      WHEN [num] = 2 THEN 'Minor'
      ELSE 'Minor' END IS NOT NULL) AND
(CASE
	  WHEN [num] = 1 THEN [tchSubjectMajor]
      ELSE [tchSubjectMinor]
 END IS NOT NULL)
AND (dbo.metaNumbers.num BETWEEN 1 AND 3)
GO

