SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paFields]
WITH VIEW_METADATA
AS


select 'School' c
, 0 seq
UNION
select 'Island'
, 1 seq
UNION
select 'District'
, 2 seq
GO

