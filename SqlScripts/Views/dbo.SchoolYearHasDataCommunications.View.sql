SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolYearHasDataCommunications]
AS
SELECT
SchoolSurvey.svyYear,
SchoolSurvey.schNo,
SchoolSurvey.ssID
FROM
SchoolSurvey INNER JOIN vtblCommunications
ON SchoolSurvey.ssID = vtblCommunications.ssID
GO

