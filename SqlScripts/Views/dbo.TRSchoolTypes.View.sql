SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRSchoolTypes]
AS
SELECT     stCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN stDescription
	WHEN 1 THEN stDescriptionL1
	WHEN 2 THEN stDescriptionL2

END, stDescription) AS stDescription,stAgeMin,stAgeMax
	,stOfficialMin,stOfficialMax,stTypeGrp,stForm
	,stSort

FROM         dbo.SchoolTypes
GO
GRANT SELECT ON [dbo].[TRSchoolTypes] TO [public] AS [dbo]
GO

