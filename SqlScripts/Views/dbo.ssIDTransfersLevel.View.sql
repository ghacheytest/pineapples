SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 12 2007
-- Description:	transfer totals in and out, by survey/level/destination
-- =============================================
-- in fact, does not do much becuase the sum is presently
-- only over 1 record per group
CREATE VIEW [dbo].[ssIDTransfersLevel]
AS

SELECT
PupilTables.ssID,
PupilTables.ptCode,
PupilTables.ptLevel AS LevelCode,
Sum(PupilTables.ptM) AS trM,
Sum(PupilTables.ptF) AS trF,
PupilTables.ptRow AS TrfrCode,
TF.TrfrName
FROM PupilTables
	LEFT JOIN dbo.tfnTransferCodes() TF
		ON PupilTables.ptRow = TF.TrfrCode
WHERE ptCode in ('TRIN','TROUT')
GROUP BY ssID, ptCode, ptLevel, ptRow, TF.TrfrName
GO

