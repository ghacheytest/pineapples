SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsWaterSupplySO]
AS
SELECT
Resources.resID ID,
Resources.ssID,
Resources.resSplit AS [Type],
case Resources.resAvail when -1 then 'Y' when 0 then 'N' else null end Available,
Resources.resNumber Number,
Resources.resCondition,
Resources.resQty AS TankCapacity,
Case
	When [resCondition]= -1 Then 'Y'
	When [resCondition]= 0 Then 'N'
	When [resCondition] is null Then null
	Else Null
End AS DrinkableYN,
case
	when resMaterial = 'C' then 'Chlorine'
	when resMaterial = 'T' then 'Aqua Tabs'
	when resMaterial = 'X' then 'Other'
end WaterTreatment
FROM
dbo.Resources LEFT JOIN dbo.lkpWaterSupplyTypes
ON dbo.Resources.resSplit = dbo.lkpWaterSupplyTypes.wsType
WHERE ((dbo.Resources.resName='Water Supply'))
GO

