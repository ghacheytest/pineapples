SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Fundamental view of repeaters by school, year, age and class level
-- Normalised by gender
-- this view includes repeaters, and "virtualises" the raw repeaters data held in table PupilTables
-- Used in the creation of data warehouse
-- =============================================
CREATE VIEW [dbo].[measureRepeatersG]
AS
SELECT schNo
, SurveyYear
, Estimate

, SurveyDimensionID
, ClassLevel
, Age
, G.genderCode
, case G.genderCode
	when 'M' then RepM
	when 'F' then RepF
end Rep
from measureRepeaters R
	CROSS JOIN DimensionGender G
GO

