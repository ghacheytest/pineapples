SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Teacher Counts
--
-- Teacher counts aggregated by district and sector.
-- Derived from warehouse.schoolTeacherCount table.
-- =============================================
CREATE VIEW [warehouse].[DistrictTeacherCount]
AS
Select SurveyYear
, DistrictCode
, GenderCode
, AgeGroup
, Sector
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual
from warehouse.schoolTeacherCount
GROUP BY SurveyYear,  DistrictCode, GenderCode, Sector, AgeGroup
GO

