SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pEnrolmentRead].[ssIDEnrolmentLevel]
as
SELECT     ssID, enLevel AS LevelCode,
SUM(enM) AS EnrolM,
SUM(enF) AS enrolF,
SUM(isnull(enM,0) + isnull(enF,0)) AS Enrol
FROM   dbo.Enrollments
GROUP BY ssID, enLevel
GO

