SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Dimension Gender
--
-- Simple dimension to all gender name to be used in place of gender code
-- Simply renames the lookup table fields.
-- =============================================
CREATE VIEW [dbo].[DimensionGender]
AS
SELECT     codeCode AS GenderCode, codeDescription AS Gender
FROM         dbo.TRGender
GO

