SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolTypeSectorILSCode]
as
/*
Determines an ILSCode, SecCode and GV value for a school type,
based on the class levels for that school type.
If all class level return the same values for ilscode, sector, gv,
that is the value for the School Type. Otherwise, the unique value for the
school type is left null.
This is used to determine the ilscode for a teacher, when we know
onl the information that can be collected on a school survey;
ie school type, min level, max level taught. (Sector taught is taken account of
in SchoolTypeSectorILSCode)
*/

SELECT SchoolTypes.stCode,
lkpLevels.secCode,
Min(lkpLevels.ilsCode) AS MinILSCode,
Max(lkpLevels.ilsCode) AS MaxILSCode,
--IIf([MinILSCode]=[MaxILSCode],[MinILSCode],Null) AS UniqueILSCode,
-- ilscode is the same for every level
case when Min(lkpLevels.ilsCode) = Max(lkpLevels.ilsCode)
	then Max(lkpLevels.ilsCode)
	else null
end UniqueILSCode,

Min(lkpLevels.lvlGV) AS MinGV,
Max(lkpLevels.lvlGV) AS MaxGV,
--(IIf([minGV]=[maxGV],[minGV],Null)) AS UniqueGV
--gv is the same for every level taght in this schol type
case when Min(lkpLevels.lvlGV) = Max(lkpLevels.lvlGV)
	then Max(lkpLevels.lvlGV)
	else null
end UniqueGV

FROM
	SchoolTypes
	INNER JOIN metaSchoolTypeLevelMap
	 ON SchoolTypes.stCode = metaSchoolTypeLevelMap.stCode
	INNER JOIN
	lkpLevels ON lkpLevels.codeCode =metaSchoolTypeLevelMap.tlmLevel
GROUP BY SchoolTypes.stCode, lkpLevels.secCode
GO

