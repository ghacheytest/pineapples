SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRPayPointCodes]
AS
SELECT     payptCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN payptDesc
	WHEN 1 THEN payptDescL1
	WHEN 2 THEN payptDescL2

END,payptDesc) AS payptDesc

FROM         dbo.PayPointCodes
GO
GRANT SELECT ON [dbo].[TRPayPointCodes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TRPayPointCodes] TO [public] AS [dbo]
GO

