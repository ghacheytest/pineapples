SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSurveyRead].[SchoolLatestSurvey]
AS
Select SchNo, Name, SchoolType, Island, District, LatestSurvey, Enrol
FROM
(
Select S.schNo SchNo, S.schName Name,S.schType SchoolType,IName Island, DName District, SS.svyYear LatestSurvey, SS.ssEnrol Enrol, row_number() OVER (PArtition by S.schNo ORDER BY svyYEar DESC) r
FROM Schools S
LEFT JOIN SchoolSurvey SS
	ON S.schNo = SS.schNo
LEFT JOIN Islands I
	ON S.iCode = I.iCode
LEFT JOIN Districts D
	ON I.iGroup = D.dID
) SUB
WHERE r = 1
GO

