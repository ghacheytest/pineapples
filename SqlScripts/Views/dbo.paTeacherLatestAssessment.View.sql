SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paTeacherLatestAssessment]
WITH VIEW_METADATA
AS
Select A.*
, S.schName
, TI.tFullName
, TI.tSurname
, TI.tGiven
, PS.paScore
from TeacherIdentity TI
LEFT JOIN
	(
		select  ROW_NUMBER() over (PARTITION BY tID ORDER BY paDate DESC, paID DESC) seq
		, *
		from paAssessment_
	 ) A
		ON A.tID = TI.tID
		AND A.seq = 1
	LEFT JOIN Schools S
		ON A.paSchNo = S.schNo
	LEFT JOIN paAssessmentScore PS
		ON A.paID = PS.paID
GO

