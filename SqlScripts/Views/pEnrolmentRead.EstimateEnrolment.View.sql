SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 5 2013
-- Description:	View to show estimates enrolments by school by year
-- =============================================
CREATE VIEW [pEnrolmentRead].[EstimateEnrolment]
WITh VIEW_METADATA
AS
select schNo
, LifeYear
, Estimate
, Offset
, E.enAge
, E.enLevel
, E.enM
, E.enF
, E.enSum
, bestssID
, surveyDimensionssID
from dbo.tfnESTIMATE_BestSurveyEnrolments() EBSE
INNER JOIN Enrollments E
	ON EBSE.bestssID = E.ssID
GO

