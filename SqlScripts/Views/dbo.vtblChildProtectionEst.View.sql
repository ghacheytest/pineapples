SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblChildProtectionEst]
AS
SELECT     ESTIMATE.LifeYear AS [Survey Year], ESTIMATE.schNo, ESTIMATE.bestYear AS [Year OF Data], ESTIMATE.Estimate,
                      ESTIMATE.Offset AS [Age Of Data], ESTIMATE.bestssID, ESTIMATE.ActualssID, ESTIMATE.SurveydimensionssID, vtbl.ptID, vtbl.ssID, vtbl.cpItem,
                      vtbl.ptLevel, vtbl.ptM, vtbl.ptF, ESTIMATE.tdefCode, dbo.TRmetaPupilTableDefs.tdefName, dbo.TRChildProtection.cpDesc, vtbl.ptCode
FROM         dbo.vtblChildProtection AS vtbl INNER JOIN
                      dbo.TRChildProtection ON vtbl.ptCode = dbo.TRChildProtection.cpCode RIGHT OUTER JOIN
                      dbo.TRmetaPupilTableDefs RIGHT OUTER JOIN
                      dbo.ESTIMATE_BestSurveyPupilTable AS ESTIMATE ON dbo.TRmetaPupilTableDefs.tdefCode = ESTIMATE.tdefCode ON vtbl.ssID = ESTIMATE.bestssID
GO

