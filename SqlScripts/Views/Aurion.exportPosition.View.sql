SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Aurion].[exportPosition]
AS
Select
	E.*
	, Schools.schName
	, RG.roleCode RoleCode
	, R.secCode
	, isnull(A.authSuperOrgUnitNumber,
		 case common.SysParamInt('Aurion_Model',0)
			-- the thin model puts the position under Authority Type/Authority/Sector
			when 0 then OrgModel0.OrgUnitNumber
			-- the deep model puts the position under Authority Type/Authority/SchoolType/School/Sector
			when 1 then OrgModel1.OrgUnitNumber
		  end
		  ) OrgUnitNumber
	, isnull(MAXSAL.salExternal,'L10') ExternalMax
	, isnull(MINSAL.salExternal,'L1') ExternalMin
	, isnull(D.dPayrollID,'HON') dPayrollID

FROM
	Establishment E
	LEFT JOIN RoleGRades RG
		ON E.estpRoleGrade = RG.rgCode
	LEFT JOIN lkpTeacherRole R
		ON RG.roleCode = R.codeCode
	LEFT JOIN Aurion.SectorCodeOrgUnits ORGModel1
		ON ORGModel1.secCode = R.secCode
		AND ORGModel1.schNo = E.schNo
	LEFT JOIN Authorities A
		ON E.estpAuth = A.authCode			-- this is to get the Authority Sup Org Unit Number
	LEFT JOIN
		(
		 Select schNo
			, OU.secCode
			, OU.orgUnitNumber
		 FROM Schools S
			INNER JOIN Aurion.AuthoritySectorOrgUnits OU
				ON S.schAuth = OU.authCode
		) ORGModel0
			ON ORGModel0.secCode = R.secCode
			AND ORGModel0.schNo = E.schNo
	LEFT JOIN lkpSalaryLevel MAXSAL
		ON RG.rgSalaryLevelMax = MAxSAL.salLevel
	LEFT JOIN lkpSalaryLevel MINSAL
		ON RG.rgSalaryLevelMin = MINSAL.salLevel
	LEFT JOIN Schools
		ON E.schNo = Schools.schNo
	LEFT JOIN lkpIslands I
		ON Schools.iCode = I.iCode
	LEFT JOIN Districts D
		ON I.iGRoup = D.dID
GO

