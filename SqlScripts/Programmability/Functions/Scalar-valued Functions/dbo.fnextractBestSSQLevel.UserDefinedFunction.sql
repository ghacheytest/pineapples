SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[fnextractBestSSQLevel]
(
	-- Add the parameters for the function here
	@BestData nchar(50)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	declare @char nchar(1)
	-- Return the result of the function
	select @char = substring(@BestData,12,1)
	if (@char = 'x')
		RETURN NULL

	return  cast(@char as int)

END
GO
GRANT EXECUTE ON [dbo].[fnextractBestSSQLevel] TO [public] AS [dbo]
GO

