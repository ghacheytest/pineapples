SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[currentUser]
(

)
RETURNS nvarchar(30)
AS
	-- =============================================
	-- Author:		Brian Lewis
	-- Create date:
	-- Description:
	-- =============================================BEGIN
	-- Declare the return variable here
BEGIN
	DECLARE @Result nvarchar(30)
	DECLARE @Domain int
	select @Result = system_user
	select @Domain = patindex('%\%', @Result)
	select @Result = substring(@Result, @Domain + 1, len(@result) - @domain)


	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON [dbo].[currentUser] TO [public] AS [dbo]
GO

