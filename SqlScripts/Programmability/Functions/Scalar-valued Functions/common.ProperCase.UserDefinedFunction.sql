SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [common].[ProperCase](@Text as nvarchar(max))
returns varchar(8000)
as
begin
declare @Reset bit;
declare @Ret nvarchar(max);
declare @i int;
declare @c nchar(1);

select @Reset = 1, @i=1, @Ret = '';

while (@i <= len(@Text))
select @c= substring(@Text,@i,1),
@Ret = @Ret + case when @Reset=1 then UPPER(@c) else LOWER(@c) end,
@Reset = case when @c like '[a-zA-Z]' then 0 else 1 end,
@i = @i +1
-- for non-english - pacific, African usage
-- Balipa'a M'Baye etc
-- after a single quote -
-- don;t capitalize a vowel unless there an abbreviation 'Ere We Go!

Select @Ret = replace(@Ret,'''A','''a')
Select @Ret = replace(@Ret,'''E','''e')
Select @Ret = replace(@Ret,'''I','''i')
Select @Ret = replace(@Ret,'''O','''o')
Select @Ret = replace(@Ret,'''U','''u')

return @Ret
end
GO

