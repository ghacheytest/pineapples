SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [common].[sysParam]
(
	-- Add the parameters for the function here
	@parmName nvarchar(30)
)
RETURNS nvarchar(150)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(150)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = paramText from sysParams
		WHERE paramName = @parmName

	-- Return the result of the function
	RETURN @Result

END
GO

