SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Function version of this
-- =============================================
CREATE FUNCTION [pSchoolRead].[tfnSchoolDataSetTeacherQualCertCount]
(
	-- Add the parameters for the function here
	@Year int
	, @SchoolNo nvarchar(50) = null
	, @GovtOther nvarchar(10) = null
	, @ReportQualCert nvarchar(1) = 'Q'			-- or 'C'

)
RETURNS
@schoolData TABLE
(
	-- Add the column definitions for the TABLE variable here
	schNo nvarchar(50)
	, dataValue float
	, Estimate int
	, Quality int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	INSERT INTO @schoolData


	Select EE.schNo, count(DISTINCT TI.tID), Estimate, ActualssqLevel
	FROM dbo.tfnESTIMATE_BestSurveyTeachers(@Year, DEFAULT) EE
		INNER JOIN TeacherSurvey TS
			ON EE.bestssID = TS.ssID
		INNER JOIN TeacherIdentity TI
			ON TS.tID = TI.tID
		LEFT JOIN tchsIDQualifiedCertified QC
			ON Qc.tchsID = TS.tchsID

	WHERE lifeYear = @Year
		AND (EE.SchNo = @SchoolNo or @SchoolNo is null)
		AND ((TI.tPayroll is not null and @GovtOther = 'Y')
				or (TI.tPayroll is null and @GovtOther = 'N')
				or @GovtOther is null
			)
		AND (
				case @ReportQualCert
					when 'Q' then QC.Qualified
					when 'C' then QC.certified
					when 'X' then left(nullif(QC.Qualified,'N') + Nullif(QC.Certified,'N'),1)
				end = 'Y'
			)
	GROUP BY EE.SchNo, Estimate, ActualssqLevel
	RETURN
END
GO

