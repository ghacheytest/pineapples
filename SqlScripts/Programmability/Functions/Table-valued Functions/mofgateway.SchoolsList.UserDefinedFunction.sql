SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [mofgateway].[SchoolsList]
(

)
RETURNS
@list TABLE
(
	[schNo] [nvarchar](50) NOT NULL,
	[schName] [nvarchar](50) NULL,
	[schEst] [smallint] NULL,
	[schClosed] [smallint] NULL,
	[schType] [nvarchar](10) NULL,
	[schLang] [nvarchar](10) NULL,
		LanguageName nvarchar(50) NULL,
	[schVillage] [nvarchar](50) NULL,
	[iCode] [nvarchar](2) NULL,
		Island nvarchar(50) NULL,
		Province nvarchar(10) NULL,
		ProvinceName nvarchar(50) NULL,
	[schAddr1] [nvarchar](50) NULL,
	[schAddr2] [nvarchar](50) NULL,
	[schAddr3] [nvarchar](50) NULL,
	[schAddr4] [nvarchar](50) NULL,
	[schPh1] [nvarchar](50) NULL,
	[schPh2] [nvarchar](50) NULL,
	[schFax] [nvarchar](50) NULL,
	[schEmail] [nvarchar](50) NULL,
	[schElectN] [nvarchar](10) NULL,
		ElectorateN nvarchar(50) NULL,
	[schAuth] [nvarchar](10) NULL,
		AuthName nvarchar(100) NULL,
	LastSurveyYear int NULL,
	Enrolment int NULL


)
AS
BEGIN
	-- Fill the table variable with the rows for your result set


INSERT INTO @List

Select Schools.schNo
, schName
, schEst
, schClosed
, schType
, schLang
, Lang.langName LanguageName
, schVillage
, Schools.icode
, iName
, iGroup
, dName
, schAddr1
, schAddr2
, schAddr3
, schAddr4
, schPh1
, schPh2
, schFax
, schEmail
, schElectN
, En.codeDescription ElectorateN
--, schElectL
--, EL.codeDescription ElectorateL
, schAuth
, authName
--, schLandOwner
--, LO.codeDescription LandOwner
--, schLandUse
--, LU.codeDescription LandUse
, bestYear LastSurveyData
, bestEnrol Enrolment
from Schools
INNER JOIN Authorities A
	ON Schools.schAuth 	= A.authCode
LEFT JOIN TRLanguage Lang
	on schLang = Lang.langCode
LEFT JOIN lkpElectorateN EN
	ON schElectN = EN.codeCode
LEFT JOIN lkpElectorateL EL
	ON schElectL = EL.codeCode
LEFT JOIN TRLandOwnership LO
	ON schLandOwner = LO.codeCode

LEFT JOIN TRLandUSe LU
	ON schLandUse = LU.codeCode
LEFT JOIN Islands I
	on schools.iCode =I.iCode
LEFT JOIN Districts D
	ON I.iGRoup = d.dID
LEFT JOIN dbo.tfnESTIMATE_BEstSurveyEnrolments() EE
	ON Schools.schNo = EE.schNo
	AND year(common.today()) = EE.LifeYEar


RETURN
END
GO

