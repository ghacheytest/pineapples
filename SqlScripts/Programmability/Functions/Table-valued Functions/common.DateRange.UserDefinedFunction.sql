SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [common].[DateRange]
(
	@Startdate datetime
	, @EndDate datetime
)
RETURNS
@DateSet TABLE
(
	dt datetime
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	declare @i int
	SELECT @i = DateDiff(d,@Startdate, @EndDate)
	INSERT INTO @DateSet
	Select dateadd(d,num, @StartDate)
	from
		metaNumbers
	WHERE num between 0 and @i

	RETURN
END
GO

