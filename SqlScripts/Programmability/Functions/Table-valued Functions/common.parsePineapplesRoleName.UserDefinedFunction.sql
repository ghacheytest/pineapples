SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 10 2009
-- Description:	Break up the name of a pineapples role into its logical parts
-- =============================================
CREATE FUNCTION [common].[parsePineapplesRoleName]
(
	-- Add the parameters for the function here
	@RoleName sysname
)
RETURNS
@Table_Var TABLE
(
	-- Add the column definitions for the TABLE variable here
	DataArea nvarchar(50),
	DataAccess nvarchar(10)
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set


	INSERT INTO @Table_Var
	SELECT

		case
			when patindex('%Admin',@RoleNAme) > 0 then substring(@RoleNAme,2,patindex('%Admin',@RoleNAme)-2)
			when patindex('%Read%',@RoleNAme) > 0 then substring(@RoleNAme,2,patindex('%Read%',@RoleNAme)-2)
			when patindex('%Write%',@RoleNAme) > 0 then substring(@RoleNAme,2,patindex('%Write%',@RoleNAme)-2)
			when patindex('%Maint',@RoleNAme) > 0 then substring(@RoleNAme,2,patindex('%Maint',@RoleNAme)-2)
			when patindex('%Ops',@RoleNAme) > 0 then substring(@RoleNAme,2,patindex('%Ops',@RoleNAme)-2)


		end

		, case when patindex('%ReadXX',@RoleNAme) > 0 then 'ReadXX'
			when patindex('%ReadX',@RoleNAme)> 0  then 'ReadX'
			when patindex('%Read',@RoleNAme)> 0  then 'Read'
		    when patindex('%WriteXX',@RoleNAme) > 0 then 'WriteXX'
			when patindex('%WriteX',@RoleNAme)> 0  then 'WriteX'
			when patindex('%Write',@RoleNAme)> 0  then 'Write'

			when patindex('%Admin',@RoleNAme)> 0  then 'Admin'
			when patindex('%Maint',@RoleNAme)> 0  then 'Maint'
			when patindex('%Ops',@RoleNAme)> 0  then 'Ops'
		end


	RETURN
END
GO

