SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 04 2010
-- Description:	Return the list of building seq numbers for room forms
-- =============================================
CREATE FUNCTION [pSchoolRead].[SurveyBuildingReviewSeq]
(
	-- Add the parameters for the function here
	@surveyID int

)
RETURNS
@Table_Var TABLE
(
	-- Add the column definitions for the TABLE variable here
	brevID  int,
	brevSeq int,
	brevTitle nvarchar(50)
)
WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- Fill the table variable with the rows for your result set

	INSERT INTO @Table_Var
	SELECT brevID
		, brevSeq
		, brevTitle
	FROM pSchoolRead.[SchoolSurveyBuildingReviews]
	WHERE ssID = @SurveyID
	AND brevSeq is not null
	RETURN
END
GO

