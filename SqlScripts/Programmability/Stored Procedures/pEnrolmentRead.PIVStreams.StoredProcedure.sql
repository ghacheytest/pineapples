SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 11 2007
-- Description:	Data for analysis of streams at each level
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[PIVStreams]
	@ServerSide int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- set up the estimate temp table
SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

 -- stream combines level enrolment with streams
-- we denormalise into a temporary table
-- 24 4 2010 it is quicker and less IO to use SELECT...INTO then to define a temp table and INSERT....?

SELECT SEL.ssID ,
SEL.LevelCode,
SEL.EnrolM,
SEL.enrolF,
sel.Enrol,
0 AS stmPupils,
0 as HasStreams,
0 as stmNum ,
0 as NumClasses,
cast(0 as numeric(18,2)) as ClassHours,
0 as ClassEnrolCheck
INTO #data
FROM pEnrolmentRead.ssIDEnrolmentLevel AS SEL

INSERT INTO #data
Select ssID,
stmLevel,
0,0,0,	-- enrolment placeholders
isnull(stmPupils,0),
1,
isnull(stmNum,0),
0,0,0
FROM Streams

	-- class totals
	INSERT INTO #data
	Select
	ssID
	, pclLevel
	, 0,0,0
	, 0,0,0
	, count(pclID)
	, isnull(sum(pcHrsWeek),0)
	, isnull(sum(pclNum),0)
	FROM ClassLevel CL
		INNER JOIN Classes C
		ON C.pcID = CL.pcID
	WHERE subjCode is null
	GROUP BY ssID, pclLEvel


-- group by ssID puts the enrol and streams data on one record

----if @ServerSide = 0 begin
----	Select
----		SchNo,
----		LifeYear,
----		bestssID,
----		bestYear as [Year of Data],
----		Estimate,
----		Offset as [Age Of Data],
----		bestssqLevel as [Data Quality Level],
----		actualssqLevel as [Survey Year Quality Level],
----		surveydimensionssID,
----		levelCode,
----		sum(EnrolM) as EnrolM,
----		sum(EnrolF) as EnrolF,
----		sum(Enrol) as Enrol,
----		sum(stmNum) as stmNum,
----		sum(stmPupils) as stmPupils,
----		max(HasStreams) as HasStreamData,
----		sum(NumClasses) as NumClasses,
----		sum(ClassHours) as ClassHours,
----		sum(ClassEnrolCheck) as ClassEnrolCheck
----	from #data D
----		INNER JOIN
----			#ebse EBSE
----			ON D.ssID = EBSE.bestssID
----	GROUP BY
----		SchNo,
----		LifeYear,
----		bestssID,
----		bestYear,
----		Estimate,
----		Offset,
----		bestssqLevel,
----		actualssqLevel,
----		surveydimensionssID,
----		levelCode
----end


if @serverSide = 0 begin
	Select
		SchNo,
		LifeYear,
		bestssID,
		bestYear as [Year of Data],
		Estimate,
		Offset as [Age Of Data],
		bestssqLevel as [Data Quality Level],
		actualssqLevel as [Survey Year Quality Level],
		surveyDimensionssID
		, sub.*
		, case when sub.ssId is null then 0 else 1 end HasStreamData
	FROM
			#ebse EBSE
	INNER JOIN
	(
	Select
		ssID
		,levelCode
		, sum(enrolM) EnrolM
		, sum(enrolF) EnrolF
		, sum(enrol) Enrol
		, sum(stmNum) NumStreams
		, sum(stmPupils) StreamEnrolCheck
		, sum(NumClasses) NumClasses
		, sum(ClassHours) as ClassHours
		, sum(ClassEnrolCheck) ClassEnrolcheck

	FROM #data P
	GROUP BY ssID, levelCode
	) sub
	ON EBSE.bestssID = sub.ssID
end

if @serverSide = 1 begin
	Select EBSE.LifeYear [Survey Year]
		, EBSE.Estimate
		, EBSE.Offset [Age of Data]
		, EBSE.bestYear [Year of Data]
		, sub.EnrolM
		, sub.enrolF
		, sub.Enrol
		, sub.NumStreams
		, sub.NumClasses
		, sub.StreamEnrolCheck
		, sub.ClassEnrolCheck
		, case when sub.ssId is null then 0 else 1 end HasStreamData

		, DSS.*
		, DL.*
		FROM
			-- dbo.tfnESTIMATE_BestSurveyEnrolments() EBSE
			#ebse EBSE

		INNER JOIN
			(
			Select
				ssID
				,levelCode
				, sum(enrolM) EnrolM
				, sum(enrolF) EnrolF
				, sum(enrol) Enrol
				, sum(stmNum) NumStreams
				, sum(stmPupils) StreamEnrolCheck
				, sum(NumClasses) NumClasses
				, sum(ClassHours) as ClassHours
				, sum(ClassEnrolCheck) ClassEnrolcheck

			FROM #data
			GROUP BY ssID, levelCode
			) sub
			ON EBSE.bestssID = sub.ssID
		LEFT JOIN DimensionSchoolSurveyNoYear DSS
			ON DSS.[Survey ID] = EBSE.surveyDimensionssID
		LEFT JOIN DimensionLEvel DL
			ON sub.LevelCode = DL.[LevelCode]

end


-- clean up
DROP TABLE #data
DROP TABLE #ebse
END
GO

