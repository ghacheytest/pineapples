SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 10 2014
-- Description:	summary of key survey statistics by year
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[SchoolAnnualSummary]
	@SchoolNo nvarchar(50) = null
	, @SurveyYear int = null
WITH EXECUTE AS 'Pineapples'
-- we'll let you read the teacher counts here, even if you do not have teacher read
-- hence the need for the execute as
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- strategy is temp table with each required field, records are added in sets
-- then summed to bring all the fields together
	DECLARE @tmpAS TABLE
	(
		schNo nvarchar(50)
		, svyYear int
		, minClass nvarchar(10)
		, maxClass nvarchar(10)
		, TeacherSurveyCount int
		, ClassroomCount int
	)

-- to begin make sure we have every life year for the school in the table
	INSERT INTO @TmpAS
	(schNo, svyYear)
	Select schNo, svyYear
	FROM SchoolLifeYears
	WHERE
		(
			(@SchoolNo is null or @SchoolNo = SchNo)
			AND (@SurveyYear is null or @SurveyYEar = svyYEar)
		)

-- add the teacher survey count records
	INSERT INTO @tmpAS
		(SchNo, svyYear, TeacherSurveyCount)

	SELECT
		schNo, svyYear
		, case when ssNumTeachers is not null then ssNumTeachers
			else nullif(count(tchsID), 0) end
	FROM
		SchoolSurvey SS
		LEFT JOIN TeacherSurvey TS
			ON Ss.ssID = TS.ssID
	WHERE
		(
			(@SchoolNo is null or @SchoolNo = SchNo)
			AND (@SurveyYear is null or @SurveyYEar = svyYEar)
		)
	group by schNo, svyYear, ssNumTeachers


-- add the set of classroom records
	INSERT INTO @tmpAS
		(SchNo, svyYear, ClassroomCount)
	SELECT
	-- fsm 3 11 2017 only use the count of classrooms if it is not on the survey record
		schNo, svyYear
		, case when ssNumClassrooms is not null then ssNumClassrooms
							else nullif(count(rmID), 0) end
	FROM
		SchoolSurvey SS
		LEFT JOIN Rooms
			ON Ss.ssID = Rooms.ssID
			and rmType = 'CLASS'
	where
		(
			(@SchoolNo is null or @SchoolNo = SchNo)
			AND (@SurveyYear is null or @SurveyYEar = svyYEar)
		)

	group by schNo, svyYear, ssNumClassrooms

-- calculate the minimum and maximum levels for enrol
------
------Select SchNo
------		, SvyYear
------		, MinYear
------		, MaxYear
------		, min(case when L.lvlYear = MinYear then L.codeCode else null end) MinLevelCode
------		, min(case when L.lvlYear = MinYear then L.codeDescription else null end) MinLevel
------		, min(case when L.lvlYear = MaxYear then L.codeCode else null end) MaxLevelCode
------		, min(case when L.lvlYear = MAxYear then L.codeDescription else null end) MaxLevel
------
------
------FROM
------	Enrollments E
------
------	INNER JOIN TRLevels L
------	ON E.enLevel = L.codeCode
------	INNER JOIN
------	(
------		Select S.SchNo
------			, S.svyYEar
------			, S.ssID
------			,min(lvlYear) MinYear
------			, max(lvlYear) MaxYear
------		from	SchoolSurvey S
------			INNER JOIN 	Enrollments E
------			ON S.ssID = E.ssID
------			INNER JOIN lkpLevels L
------			ON E.enLevel = L.codeCode
------		GROUP BY S.SchNo, S.svyYEar, S.ssID
------	) MiniMax
------	ON MiniMax.ssID = E.ssID
------GROUP BY SchNo, svyYEar, minYEar, maxYear

	Select
		TMP.schNo
		, TMP.svyYear
		, SS.ssSchType
		, SS.ssAuth
		, SS.ssEnrol
		, SS.ssEnrolM
		, SS.ssEnrolF
		, SS.ssID
		, SS.ssPrinFirstName
		, SS.ssPrinSurname
		, SS.ssNote
		, LMin.CodeDescription minLevel
		, LMax.CodeDescription maxLevel
		, ELS.MaxLevelEnrol
		, LMaxEnrol.codeDescription  maxEnrolLevel
		, TeacherSurveyCount
		, ClassroomCount
		, case when TeacherSurveyCount = 0  then null else cast(SS.ssEnrol as float)/TeacherSurveyCount end PTR
		, eAudit.Loaded
		, eAudit.Errors
		, eAudit.Warnings
		, case when eAudit.Loaded is null then 0 else 1 end eForm
	FROM
		(
		Select

			schNo
			, svyYear
			, min(minClass) minClass
			, max(maxClass) maxClass
			, max(TeacherSurveyCount) TeacherSurveyCount
			, max(ClassroomCount) ClassroomCount
		FROM
			@tmpAS
		GROUP BY
			schNo, svyYear
		) TMP
		LEFT JOIN SchoolSurvey SS
			ON ss.schNo = Tmp.SchNo
			AND SS.svyYear = TMP.svyYear
		LEFT JOIN pEnrolmentRead.ssIDEnrolmentLevelSummary ELS
			ON SS.ssID = ELS.ssID
		LEFT JOIN TRLevels LMin
			On ELS.LowestLevel = LMin.codeCode
		LEFT JOIN TRLevels LMax
			ON ELS.HighestLevel = LMAx.CodeCode
		LEFT JOIN TRLevels LMaxEnrol
			ON ELS.LEvelOfMaxEnrol = LMaxEnrol.CodeCode
		LEFT JOIN
			(
				Select ssID
					, max(sxaDate) Loaded
					, sum(case when sxaErrorflag = 2 then 1 else null end) Errors
					, sum(case when sxaErrorflag = 1 then 1 else null end) Warnings
				FROM audit.xfdfAudit A
					GROUP BY ssID
			) eAudit
			ON eAudit.ssID = SS.ssID

END
GO

