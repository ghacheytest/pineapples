SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 04 2010
-- Description:	Payslip pivot table
-- =============================================
CREATE PROCEDURE [pTeacherReadX].[PayslipPivot]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

 Select U.*
 , PPC.payptDesc
 , SP.salLevel SalaryLevel
 , DSS.*
 FROM
 (
 SELECT count([tpsID]) numPayslips
      ,[tpsPeriodStart] PeriodStart
      ,min([tpsPeriodEnd]) PeriodEnd
      ,sum([tpsGross]) GrossPay
      ,[tpsTitle] PositionTitle
      ,[tpsSalaryPoint] SalaryPoint
      ,[tpsPayPoint] PayPoint
  FROM TeacherPayslips
  LEFT JOIN Establishment E
	ON tpsPosition = E.estpNo
	GROUP BY tpsSalaryPoint, tpsPayPoint,  tpsPeriodStart, tpsTitle

  ) U
  LEFT JOIN lkpSalaryPoints SP
	ON U.SalaryPoint = SP.spCode

  LEFT JOIN PayPointCodes PPC
		ON PPC.PayptCode = U.PayPoint
  LEFT JOIN Schools
		ON Schools.schPayPtCode = PPC.PayPtCode
	LEFT JOIN DimensionSchoolSurvey DSS
		ON Schools.SchNo = DSS.schNo
		AND DSS.[Survey Year] = year(U.PeriodStart)


END
GO

