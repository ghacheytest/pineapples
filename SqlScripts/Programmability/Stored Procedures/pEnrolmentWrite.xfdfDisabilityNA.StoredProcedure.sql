SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	DiabiliotyVillage - pupils not attending due to disabiity
-- =============================================
CREATE PROCEDURE [pEnrolmentWrite].[xfdfDisabilityNA]
	-- Add the parameters for the stored procedure here
	@SurveyID int = 0,
	@toiXML xml
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @toiXML, @xmlns


	declare @data TABLE
	(
		r int,
		village nvarchar(50),
		age		int,
		code	nvarchar(10) collate database_default,
		gender	nvarchar(1)
	)

	INSERT INTO @data
	Select convert(int, r) seq
		, village
		, age
		, DIS.codeCode
		, gender
	 from OPENXML (@idoc, '/x:field/x:field',2)
		WITH (
				r			nvarchar(2)			'@name'
				, village	nvarchar(50)		'x:field[@name="Village"]/x:value'
				, age		int					'x:field[@name="Age"]/x:value'
				, code		nvarchar(10)		'x:field[@name="Type"]/x:value'
				, gender	nvarchar(1)			'x:field[@name="Gender"]/x:value'
			  ) X
	LEFT JOIN lkpDisabilities DIS
		ON X.code in (DIS.codeCode, DIS.codeDescription)
		-- this formulation defends against pdf software
		-- that incorrectly returns the text, not the export value, as the value of a dropdown
		-- Apple: you know hwo you are!

-- check if there are

begin transaction

begin try
	DELETE from DisabilityVillage
	WHERE ssid = @SurveyID
	print @@ROWCOUNT

	INSERT INTO DisabilityVillage
	(ssID, disVillage, disAge, disCode, disGender)
	SELECT @SurveyID,
		village, Age, code, gender
	From @data D
	WHERE D.village is not null
		OR D.age is not null
		OR D.gender is not null
		OR D.code is not null

	exec audit.xfdfInsert @SurveyID, 'Records inserted','DisabilityVillage',@@rowcount
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end
	exec audit.xfdfError @SurveyID, @ErrorMessage,'DisabilityVillage'
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

return


END
GO

