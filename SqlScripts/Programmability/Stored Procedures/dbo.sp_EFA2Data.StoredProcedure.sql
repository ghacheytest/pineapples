SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 11 2007
-- Description:	data for efa 2
-- =============================================
CREATE PROCEDURE [dbo].[sp_EFA2Data]
	@consolidation int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- begin by caching the ebse
create table #data
(
	ssID int,
)
Select *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

-- PSAs
IF (@Consolidation = -1) begin

	Select
	E.LifeYear [Survey Year],
	E.Estimate,
	E.Offset [Age of Data],
	E.bestYear [Year of Data],
	E.surveyDimensionssID,
	Age,
	sum(enM) EnrolM,
	sum(enF) EnrolF,
	sum(repM) repM,
	sum(repF) repF,
	sum(psaM) psaM,
	sum(psaF) psaF,
	presName,
	presSeq


	from
	(
		SELECT
		vtblPSA.ssID,
		vtblPSA.Age,
		0 AS enM, 0 AS enF,
		0 AS repM, 0 AS repF,
		vtblPSA.PSAM, vtblPSA.PSAF,
		vtblPSA.presName, vtblPSA.presSeq
		FROM vtblPSA

		UNION

		SELECT
		SS.ssID,
		SS.enAge,
		SS.enM, SS.enF,
		0 as repM, 0 as repF,
		0 AS PsaM, 0 AS PsaF,

		null,null
		FROM Enrollments SS
		INNER JOIN lkpLevels
		  ON ss.enLevel = lkpLevels.codeCode
		WHERE lkpLevels.lvlYEar = 1

		UNION SELECT
		SS.ssID,
		SS.ptAge,
		0, 0,
		ptM, ptF,
		0 AS PsaM, 0 AS PsaF,
		null,null
		FROM vtblRepeaters SS
		INNER JOIN lkpLevels
		  ON ss.ptLevel = lkpLevels.codeCode
		WHERE lkpLevels.lvlYEar = 1
	) U
	INNER JOIN #ebse E
	ON E.bestssID = U.ssID
	GROUP BY E.LifeYear, E.Estimate, E.bestYear, E.Offset,
	E.surveyDimensionssID,
	U.Age,  U.presName, U.presSeq
end

If (@Consolidation = 0)	begin
	Select
	E.LifeYear [Survey Year],
	E.Estimate,
	E.Offset [Age of Data],
	E.bestYear [Year of Data],
	E.surveyDimensionssID,
	Age,
	sum(enM) EnrolM,
	sum(enF) EnrolF,
	sum(repM) repM,
	sum(repF) repF,
	sum(psaM) psaM,
	sum(psaF) psaF,
	presName,
	presSeq


	from
	(
		SELECT
		vtblPSA.ssID,
		vtblPSA.Age,
		0 AS enM, 0 AS enF,
		0 AS repM, 0 AS repF,
		vtblPSA.PSAM, vtblPSA.PSAF,
		vtblPSA.presName, vtblPSA.presSeq
		FROM vtblPSA

		UNION

		SELECT
		SS.ssID,
		SS.enAge,
		SS.enM, SS.enF,
		0 as repM, 0 as repF,
		0 AS PsaM, 0 AS PsaF,

		null,null
		FROM Enrollments SS
		INNER JOIN lkpLevels
		  ON ss.enLevel = lkpLevels.codeCode
		WHERE lkpLevels.lvlYEar = 1

		UNION SELECT
		SS.ssID,
		SS.ptAge,
		0, 0,
		ptM, ptF,
		0 AS PsaM, 0 AS PsaF,
		null,null
		FROM vtblRepeaters SS
		INNER JOIN lkpLevels
		  ON ss.ptLevel = lkpLevels.codeCode
		WHERE lkpLevels.lvlYEar = 1
	) U
	INNER JOIN #ebse E
	ON E.bestssID = U.ssID
	GROUP BY E.LifeYear, E.Estimate, E.bestYear, E.Offset,
	E.surveyDimensionssID,
	U.Age,  U.presName, U.presSeq
end

if (@Consolidation = 1) begin
Select sub.*
	, cast(isnull(psaM,0) as float) / IntakeM as psaPercM
	, cast(isnull(psaF,0) as float) / IntakeF as psaPercF
	, cast(isnull(psa,0) as float) / Intake as psaPerc

	, (cast(isnull(psaF,0) as float) / IntakeF) /
		(cast(isnull(psaM,0) as float) / IntakeM) as psaPercGPI

	, cast(isnull(IntakeM,0) as float) / PopM as girM
	, cast(isnull(IntakeF,0) as float) / PopF as girF
	, cast(isnull(Intake,0) as float) / Pop as gir


	, (cast(isnull(IntakeF,0) as float) / PopF) /
		(cast(isnull(IntakeM,0) as float) / PopM ) as girGPI

	, cast(isnull(nIntakeM,0) as float) / PopM as nirM
	, cast(isnull(nIntakeF,0) as float) / PopF as nirF
	, cast(isnull(nIntake,0) as float) / Pop as nir

	, (cast(isnull(nIntakeF,0) as float) / PopF) /
		(cast(isnull(nIntakeM,0) as float) / PopM) nirGPI

FROM
	(
	Select
		E.LifeYear [Survey Year],
		sum(enM) EnrolM
		, sum(enF) EnrolF
		, sum(enSum) Enrol

		, sum(repM) repM
		, sum(repF) repF
		, isnull(sum(repM),0)+isnull(sum(repF),0) Rep

		, sum(psaM) psaM
		, sum(psaF) psaF
		, isnull(sum(psaM),0)+isnull(sum(psaF),0) Psa

		, sum ( case when Age = svyPSAge then enM else null end) nEnrolM
		, sum ( case when Age = svyPSAge then enF else null end) nEnrolF
		, sum ( case when Age = svyPSAge then enSum else null end) nEnrol

		, sum ( case when Age = svyPSAge then repM else null end) nRepM
		, sum ( case when Age = svyPSAge then repF else null end) nRepF
		, sum ( case when Age = svyPSAge then isnull(repM,0) + isnull(repF,0) else null end) nRep

		,sum ( case when Age = svyPSAge then psaM else null end) nPsaM
		,sum ( case when Age = svyPSAge then psaF else null end) nPsaF
		,sum ( case when Age = svyPSAge then isnull(psaM,0) + isnull(psaF,0) else null end) nPsa

		,  PopM
		,  PopF
		, PopM+popF Pop

		, isnull(sum(enM),0) - isnull(sum(repM),0) IntakeM
		, isnull(sum(enF),0) - isnull(sum(repF),0) IntakeF
		, isnull(sum(enSum),0) - isnull(sum(repM),0) - isnull(sum(repF),0) Intake

		, sum ( case when Age = svyPSAge then enM else null end)
			- sum ( case when Age = svyPSAge then repM else null end) nIntakeM
		, sum ( case when Age = svyPSAge then enF else null end)
			- sum ( case when Age = svyPSAge then repF else null end) nIntakeF
		, sum ( case when Age = svyPSAge then enSum else null end)
			- sum ( case when Age = svyPSAge then isnull(repM,0) + isnull(repF,0) else null end) nIntake


		from
		(
			SELECT
			vtblPSA.ssID,
			vtblPSA.Age,
			0 AS enM, 0 AS enF, 0 as enSum
			, 0 AS repM, 0 AS repF
			, vtblPSA.PSAM, vtblPSA.PSAF,
			vtblPSA.presName, vtblPSA.presSeq
			FROM vtblPSA

			UNION

			SELECT
			SS.ssID,
			SS.enAge,
			SS.enM, SS.enF, SS.enSum
			,0 as repM, 0 as repF
			,0 AS PsaM, 0 AS PsaF

			,null,null
			FROM Enrollments SS
			INNER JOIN lkpLevels
			  ON ss.enLevel = lkpLevels.codeCode
			WHERE lkpLevels.lvlYEar = 1

			UNION SELECT
			SS.ssID,
			SS.ptAge,
			0, 0, 0
			, ptM, ptF,
			0 AS PsaM, 0 AS PsaF,
			null,null
			FROM vtblRepeaters SS
			INNER JOIN lkpLevels
			  ON ss.ptLevel = lkpLevels.codeCode
			WHERE lkpLevels.lvlYEar = 1
		) U
		INNER JOIN #ebse E
		ON E.bestssID = U.ssID
		INNER JOIN
			(Select svyYear, svyPSAge,  sum(popM) popM, sum(PopF) popF
			FROM
				Survey
				INNER JOIN [Population] P
				ON Survey.svyYear = P.popYear
					AND Survey.svyPSAge = P.popAge
				LEFT JOIN PopulationModel PM
					ON P.popmodCode = PM.popmodCode
				WHERE PM.popmodEFA = 1
				GROUP BY svyYear, svyPSAge
			) Pop
			ON Pop.svyYear = E.LifeYear
		GROUP BY E.LifeYear, svyPSAge, pop.PopM, pop.PopF
	) sub
	ORDER BY [Survey Year]
end

END
GO
GRANT EXECUTE ON [dbo].[sp_EFA2Data] TO [pSchoolRead] AS [dbo]
GO

