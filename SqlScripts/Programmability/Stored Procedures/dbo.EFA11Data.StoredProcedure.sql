SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 11 2007
-- Description:	Core data for calculation of EFA 11. Used by EFA11 query.
-- =============================================
CREATE PROCEDURE [dbo].[EFA11Data]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()

Select *
into #ebst
from dbo.tfnESTIMATE_BestSurveyTeachers(DEFAULT,DEFAULT)

CREATE TABLE #efa11
(
	[Survey Year] int,
	schNo nvarchar(50),
	surveyDimensionssID int,
	SectorCode nvarchar(10),
	EnrolM int,
	enrolF int,
	Enrol int,
	Total int,
	NumQualified int,
	NumCertified int,
	QualifiedSalary int,
	CertifiedSalary  int,
	MaleTeachers int,
	FemaleTeachers int,
	TotalAgeMale int,
	TotalAgeFemale int,
	Fulltime int,
	FullTimeM int,
	FullTimeF int,
	FTE int,
	FTEM int,
	FTEF int,
	TAM int,
	TAMM int,
	TAMF int,
	TAMFTE int,
	TAMFTEM int,
	TAMFTEF int,

	GovTeacher int,
	GovTeacherM int,
	GovTeacherF int,
	[Enrolment Estimate] int,
	[Year of Enrolment Data] int,
	[Age of Enrolment Data] int,
	TsurveyDimensionssID int,
	[Teacher Estimate] int,
	[Year of Teacher Data] int,
	[Age of Teacher Data] int
)


SELECT
sub.[Survey Year],
sub.schNo,
isnull(surveyDimensionssID,TsurveyDimensionssID) surveyDimensionssID,
sub.SectorCode,
Sum(sub.EnrolM) AS EnrolM,
Sum(sub.enrolF) AS enrolF,
Sum(sub.Enrol) AS Enrol,
Sum(sub.Total) AS Total,
Sum(sub.NumQualified) AS NumQualified,
Sum(sub.NumCertified) AS NumCertified,
Sum(sub.QualifiedSalary) AS QualifiedSalary,
Sum(sub.CertifiedSalary) AS CertifiedSalary,
Sum(sub.MaleTeachers) AS MaleTeachers,
Sum(sub.FemaleTeachers) AS FemaleTeachers,
Sum(sub.TotalAgeMale) AS TotalAgeMale,
Sum(sub.TotalAgeFemale) AS TotalAgeFemale,
Sum(sub.Fulltime) AS Fulltime,
Sum(sub.FullTimeM) AS FullTimeM,
Sum(sub.FullTimeF) AS FullTimeF,
Sum(sub.FTE) AS FTE,
Sum(sub.FTEM) AS FTEM,
Sum(sub.FTEF) AS FTEF,
-- added TAM 23 11 2008
Sum(sub.TAM) AS TeachingDuties,
Sum(sub.TAMM) AS TeachingDutiesM,
Sum(sub.TAMF) AS TeachingDutiesF,
-- adn the product
-- added TAM 23 11 2008
Sum(sub.TAMFTE) AS DutiesFTE,
Sum(sub.TAMFTEM) AS DutiesFTEM,
Sum(sub.TAMFTEF) AS DutiesFTEF,

Sum(sub.GovTeacher) AS GovTeacher,
Sum(sub.GovTeacherM) AS GovTeacherM,
Sum(sub.GovTeacherF) AS GovTeacherF,

Max(case [sub].[Enrolment Estimate]
		when 9999 then Null
		else[sub].[Enrolment Estimate]
	end) AS [Enrolment Estimate],
Max(case [sub].[Year of Enrolment Data]
		when 9999 then Null
		else[sub].[Year of Enrolment Data]
	end) AS [Year of Enrolment Data],
Max(case [sub].[Age of Enrolment Data]
		when 9999 then Null
		else[sub].[Age of Enrolment Data]
	end) AS [Age of Enrolment Data],

Max(case [sub].[Teacher Estimate]
		when 9999 then Null
		else[sub].[Teacher Estimate]
	end) AS [Teacher Estimate],
Max(case [sub].[Year of Teacher Data]
		when 9999 then Null
		else[sub].[Year of Teacher Data]
	end) AS [Year of Teacher Data],
Max(case [sub].[Age of Teacher Data]
		when 9999 then Null
		else[sub].[Age of Teacher Data]
	end) AS [Age of Teacher Data]

into #tmpT

FROM
(
	SELECT
	E.LifeYear as [Survey Year],
	E.schNo,
	E.surveyDimensionssID,
	SS.SectorCode,
	SS.EnrolM, SS.enrolF, SS.Enrol,
	0 AS Total,
	0 AS NumQualified,
	0 AS NumCertified,
	0 AS QualifiedSalary,
	0 AS CertifiedSalary,
	0 AS MaleTeachers, 0 AS FemaleTeachers,
	0 AS TotalAgeMale, 0 AS TotalAgeFemale,
	0 AS Fulltime, 0 AS FullTimeM, 0 AS FullTimeF,
	0 AS FTE, 0 AS FTEM, 0 AS FTEF,
	0 AS TAM, 0 AS TAMM, 0 AS TAMF,
	0 AS TAMFTE, 0 AS TAMFTEM, 0 AS TAMFTEF,

	0 AS GovTeacher, 0 AS GovTeacherM, 0 AS GovTeacherF,
	E.Estimate as [Enrolment Estimate],
	E.bestYear as [Year of Enrolment Data],
	E.offset as [Age of Enrolment Data],
	0 as TsurveyDimensionssID,
	9999 as [Teacher Estimate],
	9999 as [Year of Teacher Data],
	9999 as [Age of Teacher Data]

	FROM #ebse AS E
	INNER JOIN pEnrolmentRead.ssidEnrolmentSector AS SS
	  ON E.bestssID = SS.ssID


	UNION SELECT
	E.LifeYear as [Survey Year],
	E.schNo,
	null,
	TSEC.SectorCode,
	0 AS EnrolM, 0 AS EnrolF, 0 AS Enrol,
	TSEC.Total,
	TSEC.NumQualified,
	TSEC.NumCertified,
	TSEC.QualifiedSalary,
	TSEC.CertifiedSalary,
	TSEC.MaleTeachers, TSEC.FemaleTeachers,
	TSEC.TotalAgeMale, TSEC.TotalAgeFemale,
	TSEC.FullTime, TSEC.FullTimeM, TSEC.FullTimeF,
	TSEC.FTE, TSEC.FTEM, TSEC.FTEF,
	TSEC.TAM, TSEC.TAMM, TSEC.TAMF,
	TSEC.TAMFTE, TSEC.TAMFTEM, TSEC.TAMFTEF,

	TSEC.GovTeacher, TSEC.GovTeacherM, TSEC.GovTeacherF,
	9999 as [Enrolment Estimate],
	9999 as [Year of Enrolment Data],
	9999 as [Age of Enrolment Data],
	E.surveydimensionssID,
	E.Estimate as [Teacher Estimate],
	E.bestYear as [Year of Teacher Data],
	E.offset as [Age of Teacher Data]
	FROM #ebst AS E
	INNER JOIN ssIDTeacherSummarySector AS TSEC
	  ON E.bestssID = TSEC.ssID
) sub

GROUP BY [Survey Year],schNo, isnull(surveyDimensionssID,TsurveyDimensionssID), SectorCode

Select T.*
	, R.[District Rank]
	, R.[District Decile]
	, R.[District Quartile]
	, R.[Rank]
	, R.[Decile]
	, R.[Quartile]
from #tmpT T
	left join DimensionRank R
	on T.[Survey Year] = R.svyYear
		and T.SchNo = R.SchNo

END
GO
GRANT EXECUTE ON [dbo].[EFA11Data] TO [pSchoolRead] AS [dbo]
GO

