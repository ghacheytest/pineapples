SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16/11/2007
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVExamSubjectsData]

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- create the temp table for estimates
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

-- join the exams data

Select
	X.exYear [Survey Year],
	ET.exCode [Exam Code],
	ET.exName [Exam Name],
	ET.exLevel [Exam Level],
	X.exYear [Exam Year],
	XS.exrGrade [Grade],
	XS.exrSubject [Subject],
	XS.exrGender,
	XS.exrCandidates Candidates,
	XS.exrRaw [Raw Score],
	XS.exrStandard [Standard Score],
	XS.exrFinal [Final Score],
	XS.schNo [Exam SchNo],
	Schools.schName  [Exam SchName],
	EBSE.surveydimensionssID,
	EBSE.ActualssID


FROM
	lkpExamTypes ET
	INNER JOIN Exams X
		ON ET.exCode = X.exCode
	INNER JOIN ExamSubjectGrades XS
		ON X.exID = XS.exID
	INNER JOIN Schools
		ON XS.schNo = Schools.schNo
	LEFT JOIN #ebse EBSE
		ON X.exYear = EBSE.LifeYear AND XS.schNo = EBSE.schNo

drop TABLE #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVExamSubjectsData] TO [pExamRead] AS [dbo]
GO

