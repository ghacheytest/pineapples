SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 12 2017
-- Description:	Refresh the warehouse population table
-- =============================================
CREATE PROCEDURE [warehouse].[buildPopulation]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
---- school level flow model

			-- population, based on the default model
			-- Output: warehouse.measurepopG
		begin transaction
			IF OBJECT_ID('warehouse.measurePopG', 'U') IS NULL BEGIN
				SELECT *
				INTO warehouse.measurePopG
				FROm dbo.measurePopG
				print 'warehouse.measurePopG created - rows:' + convert(nvarchar(10), @@rowcount)
			end else begin
				DELETE
				FROM warehouse.measurePopG
				WHERE (popYear >= @StartFromYear or @StartFromYEar is null)
				print 'warehouse.measurePopG deletes - rows:' + convert(nvarchar(10), @@rowcount)
				INSERT INTO warehouse.measurePopG
				SELECT *
				FROm dbo.measurePopG
				WHERE (popYear >= @StartFromYear or @StartFromYEar is null)
				print 'warehouse.measurePopG inserts - rows:' + convert(nvarchar(10), @@rowcount)

			end
		commit transaction
END
GO

