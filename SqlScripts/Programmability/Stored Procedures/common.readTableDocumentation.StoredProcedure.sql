SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brain Lewis
-- Create date: 11 11 2009
-- Description:	Read cusomtised table documentation
-- =============================================
CREATE PROCEDURE [common].[readTableDocumentation]
	-- Add the parameters for the stored procedure here
	@TableName sysname = 0,
	@SchemaName sysname
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

declare @xprops table
(
propName sysname,
propValue nvarchar(4000)
)


	-- make a little temp table

	INSERT INTO @xprops
	Select
		P.name
		, convert(nvarchar(4000),P.value)
	from
		sys.extended_properties P
		INNER JOIN sys.tables T
			on P.major_Id = T.object_id
		INNER JOIN sys.schemas S
		ON S.schema_ID = T.schema_ID
	WHERE T.name = @TableName
		AND S.name = @schemaName
		AND P.Minor_ID = 0			-- table props, not column props

	declare @description nvarchar(4000)
	declare @systemTopic nvarchar(100)
	declare @dataAccessUI nvarchar(4000)
	declare @dataAccessDB nvarchar(4000)
	declare @diagramName nvarchar(100)

	declare @propName sysname

	select @propname = 'MS_Description'
	select @description = propValue
	from @xprops
	where
			propName = @propName

	select @propname = 'pSystemTopic'
	select @systemTopic = propValue
	from @xprops
	where
			propName = @propName

	select @propname = 'pDataAccessUI'
	select @DataAccessUI = propValue
	from @xprops
	where
			propName = @propName

	select @propname = 'pDataAccessDB'
	select @DataACcessDB = propValue
	from @xprops
	where
			propName = @propName

	select @propname = 'pDiagramName'
	select @DiagramName = propValue
	from @xprops
	where
			propName = @propName

	Select @TableName TableName
		, @SchemaName SchemaName
		, @description [Description]
		, @SystemTopic SystemTopic
		, @DataAccessUI DataAccessUI
		, @DataAccessDB DataAccessDB
		, @DiagramName DiagramName


END
GO

