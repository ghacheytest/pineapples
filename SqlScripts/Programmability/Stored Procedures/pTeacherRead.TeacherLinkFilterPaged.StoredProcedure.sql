SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 8 2017
-- Description:	Filter of teacher links
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherLinkFilterPaged]
	-- Add the parameters for the stored procedure here

	@Columnset int = 1,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@LinkID				int = null,		-- the unique idenitifer of the
	@TeacherID			int = null,
	@TeacherName		nvarchar(100) = null,		-- matched with like if
	@PayrollNo			nvarchar(10) = null,
	@DocumentID			uniqueidentifier = null,
	@Keyword			nvarchar(50) = null,
	@Function			nvarchar(50) = null,
	@DateStart			datetime = null,
	@DateEnd			datetime = null,
	@DocumentSource		nvarchar(100) = null,
	@DocType			nvarchar(10) = null,
	@IsImage			int = null
AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @keys TABLE
	(
		Id int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pTeacherRead.TeacherLinkFilterIDS
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

		, @LinkID
		, @TeacherID
		, @TeacherName
		, @PayrollNo
		, @DocumentID
		, @Keyword
		, @Function
		, @DateStart
		, @DateEnd
		, @DocumentSource
		, @DocType
		, @IsImage


--- column sets ----
	Select lnkID
	, TL.tID
	, TL.docID
	, lnkFunction
	, lnkHidden
	, docTitle
	, docDescription
	, docSource
	, docDate
	, docRotate
	, docTags
	, docType
	, TL.pCreateUser
	, TL.pCreateDateTime
	, TL.pEditUser
	, TL.pEditDateTime
	, TL.pRowversion
	, TI.tFullName
	, TI.tGiven
	, TI.tSurname
	, TI.tPayroll
	, tDOB
	, tSex

	FROM pTeacherRead.TeacherLinks TL
		INNER JOIN @keys K
			ON TL.lnkID = K.ID
		LEFT JOIN TEacherIdentity TI
			ON TL.tID = TI.tID
	ORDER BY recNo


--- summary --------
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
END
GO

