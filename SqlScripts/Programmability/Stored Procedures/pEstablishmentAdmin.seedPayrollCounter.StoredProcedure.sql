SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		BrianLewis
-- Create date: 28 10 2009
-- Description:	Seed value for PAYROLL counter
-- =============================================
CREATE PROCEDURE [pEstablishmentAdmin].[seedPayrollCounter]
	-- Add the parameters for the stored procedure here
	@seedValue int
	, @CounterRetrieved int = 0

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	exec seedCounter 'PAYROLL', @seedValue, @CounterRetrieved

    -- Insert statements for procedure here

END
GO

