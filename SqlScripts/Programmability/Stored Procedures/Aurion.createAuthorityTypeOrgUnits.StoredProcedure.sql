SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Aurion].[createAuthorityTypeOrgUnits]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
	, @Nested int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

	If @Nested = 0
		create table #OrgUnits
		(
				OrgUnitNumber int
				, OrgUnitLevel int
				, OrgUnitName nvarchar(25)
				, OrgUnitShortName nvarchar(8)
				, OrgUnitLongDescription nvarchar(50)
				, OrgUnitParent int
				, OrgUnitReference nvarchar(10)
		)

	begin transaction
	-- list the schools that need an org unit number
		Select DISTINCT authType
		INTO #tmpAuthType
		FROM Establishment E
			INNER JOIN Schools S
				ON E.schNo = S.SchNo
			INNER JOIN Authorities AUTH
				ON S.schAuth = Auth.AuthCode
			INNER JOIN lkpAuthorityType AType
				ON Auth.AuthType = AType.CodeCode
		WHERE
			atOrgUnitNumber is null


		declare @ToAllocate int

		-- this is the number of org unit numbers we need
		Select @ToAllocate = count(*)
		from #tmpAuthType

		-- this is where we'll put them
		declare @counters table
		(
		counter int
		, maxCounters int
		, seq int
		, char nvarchar(6)
		)

		IF (@ToAllocate > 0 ) begin


		-- get the numbers

			insert into @counters
			exec getCounter 'Aurion_OrgUnit', @ToAllocate


			UPDATE lkpAuthorityType
				SET atOrgUnitNumber = counter

			FROM
				lkpAuthorityType AType
					INNER JOIN
						(Select row_number() over (ORDER BY authType) Pos
						, authType
						from #tmpAuthType
						) ORDERED
						ON AType.codeCode = ORDERED.authType
					INNER JOIN @counters C
						ON ORDERED.Pos = C.seq

			-- are the parent levels OK?


		end


	commit transaction

	declare @TopOrgUnit int
	declare @TopOrgLevel int

	Select @TopOrgUnit = common.SysParamInt('Aurion_TopLevelOrgNumber',0)
	Select @TopOrgLevel = common.SysParamInt('Aurion_TopLevelOrgLevel',4)

	INSERT INTO #OrgUnits
	(
		OrgUnitNumber
		, OrgUnitLevel
		, OrgUnitName
		, OrgUnitShortName
		, ORgUnitLongDescription
		, OrgUnitParent
		, OrgUnitReference
	)
	SELECT
		atOrgUnitNumber
		, @TopOrgLevel+1
		, left(codeDescription,25)
		, codeCode
		, left(codeDescription,50)
		, @TopOrgUnit
		, codeCode

	FROM lkpAuthorityType AType
		LEFT JOIN #tmpAuthType
			ON Atype.CodeCode = #tmpAuthType.authType
	WHERE
		atOrgUnitNumber is not null
		AND
		(#tmpAuthType.authType is not null
			OR @SendAll = 1)


	-- return the ones we added
	-- return the ones we added
	If (@Nested = 0) begin
		Select *
		FROM
			#OrgUnits

		drop table #OrgUnits
	end

	drop table #tmpAuthType

end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


END
GO

