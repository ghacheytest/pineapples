SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Procedure
-- =============================================
-- Author:                Brian Lewis
-- Create date: 10 6 2016
-- Description:        Fro retrieveal data of resources for web site
-- =============================================
CREATE PROCEDURE [pSurveyRead].[ResourceList]
        -- Add the parameters for the stored procedure here
        @schoolNo nvarchar(50)
        , @surveyYear int
        , @category nvarchar(100)
        , @split nvarchar(50) = null
        , @schoolType nvarchar(10) = null
AS
BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
-- identifying information
        declare @ssID int
        declare @paramSchoolType nvarchar(20)
        declare @schName nvarchar(100)

        Select @paramSchooltype = isnull(ssSchType, schType)
        , @ssID = ssID
        , @schName = schName
        from Schools S
                LEFT JOIN SchoolSurvey SS
                        ON SS.schNo = S.schNo
                        AND SS.svyYear = @SurveyYear
        WHERE S.schNo = @SchoolNo

        Select @schoolNo schoolNo, @SurveyYear year, @paramSchoolType schoolType, @schName schoolName
                , @category category,
                @ssID surveyID
-- the definition of the category
-- use this peculiar format simply to handle translation via TRmetaResourceDefs
        Select mresCat categoryCode
        , mresCatName categoryName
        , min(case when mresPromptAdq = 0 then 0 else -1 end) showAdequate
        , min(case when mresPromptAvail = 0 then 0 else -1 end) showAvailable
        , min(case when mresPromptNum = 0 then 0 else -1 end) showNum
        , min(case when mresPromptQty = 0 then 0 else -1 end) showQty
        , min(case when mresPromptCondition = 0 then 0 else -1 end) showCondition
        , min(0) showFunctioning
        FROM TRmetaResourceDEFS
        WHERE mresCat = @category
        GROUP BY mresCat, mrescatName

-- the resource definitions in the category
        Select mresCode code
        , mresName name
        , mresSeq seq
        , mresPromptAdq promptAdequate
        , mresPromptAvail promptAvailable
        , mresPromptNum promptNum
        , mresPromptCondition promptCondition
        , mresPromptQty promptQty
        , mresPromptNote promptNote
        FROM TRmetaResourceDefs
        WHERE mresCat = @category
        ORDER BY mresSeq, mresCode


-- this returns the resource data
        Select R.resSplit        resourceCode
        , R.resAvail                available
        , R.resAdequate                adequate
        , R.resNumber                num
        , R.resQty                        qty
        , R.resCondition    condition
        , R.resFunctioning  functioning
        , R.resLevel                classLevel
        , R.resLocation                location
        From Resources R
                INNER JOIN SchoolSurvey SS
                        ON R.ssID = SS.ssID
                INNER JOIN metaResourceDefs DEF
                        ON resSplit = DEF.mresName
                LEFT JOIN metaSchoolTypeLevelMap MAP
                        ON R.resLevel = map.tlmLevel
        WHERE SS.schNo = @schoolNo
                AND Ss.svyYear = @surveyYear
                AND DEF.mresCat = @category
                AND (R.resSplit = @split OR @split is null)
                AND (MAP.stCode = @schoolType OR @schoolType is null)


END
GO

