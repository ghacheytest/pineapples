SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 12 2017
-- Description:	Read all DisabilityVillage records for a survey
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[DisabilityVillageRead]
	-- Add the parameters for the stored procedure here
	@schoolNo nvarchar(50)
	, @surveyyear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select disID ID
      ,ssID
      ,disVillage
      ,disAge
      ,disCode
      ,disGender
	From DisabilityVillage
	WHERE ssID in (Select ssID from SchoolSurvey
		WHERE schNo = @schoolNo AND svyYEar = @surveyyear)


END
GO

