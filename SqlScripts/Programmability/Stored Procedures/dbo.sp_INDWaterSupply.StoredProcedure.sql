SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 11 2007
-- Description:	Water supply indicator
-- =============================================
CREATE PROCEDURE [dbo].[sp_INDWaterSupply]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- get the best water supply data

SELECT *
INTO #ebrc
FROM dbo.tfnESTIMATE_BestSurveyResourceCategory('Water Supply')
    -- Insert statements for procedure here


-- aggregate the water supply by ssID

Select
E.LifeYEar [Survey Year],
E.Estimate,
E.bestYear [Year of Data],
E.offset [Age of Data],
DD.*,
TotalTankCapacity,
Condition,
CleanSafe,
SupplyTypes,
HasPipedWater,
HasTank,
TankCapacitySupplied
from

(
SELECT vw.ssID,
Sum(vw.TankCapacity) AS TotalTankCapacity,
Min(vw.resCondition) AS Condition,
Max(vw.CleanSafe) AS CleanSafe,
Count(vw.WaterSupplyType) AS SupplyTypes,
Max(case when WaterSupplyType = 'Piped' then 1 else 0 end) AS HasPipedWater,
Max(case when WaterSupplyType = 'Roof Tank' then 1 else 0 end) AS HasTank,
Max(case when TankCapacity > 0 then 1 else 0 end) AS TankCapacitySupplied
FROM vtblWaterSupply AS vw
GROUP BY vw.ssID
) Q				-- ssIDWaterSupply in jet
INNER JOIN #ebrc E
	on E.bestssID = Q.ssID
INNER JOIN DimensionSchoolSurveyNoYear DD
	ON E.surveyDimensionssID = DD.[survey id]
END
GO
GRANT EXECUTE ON [dbo].[sp_INDWaterSupply] TO [pSchoolRead] AS [dbo]
GO

