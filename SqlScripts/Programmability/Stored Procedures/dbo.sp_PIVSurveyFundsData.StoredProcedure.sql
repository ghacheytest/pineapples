SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	Survey funds pivot data
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVSurveyFundsData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- no estimation used for survey funds, becuase they tend not to be repeated year on year

SELECT
SurveyFunds.ssID,
SurveyFunds.svfAmount AS Amount,
case when [fundDesc]='(specify...)' then 'Other' else [fundDesc] end AS Fund,
SurveyFunds.svfOther AS Other,
SourceOfFunds.sofDesc AS Source
FROM FundTypes
INNER JOIN SourceOfFunds
	ON FundTypes.sofCode = SourceOfFunds.sofCode
INNER JOIN SurveyFunds
	ON FundTypes.fundType = SurveyFunds.svfType

END
GO
GRANT EXECUTE ON [dbo].[sp_PIVSurveyFundsData] TO [pSchoolRead] AS [dbo]
GO

