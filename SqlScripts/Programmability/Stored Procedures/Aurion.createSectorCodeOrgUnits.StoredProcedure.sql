SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Aurion].[createSectorCodeOrgUnits]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
	, @Nested int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

	If @Nested = 0
		create table #OrgUnits
		(
				OrgUnitNumber int
				, OrgUnitLevel int
				, OrgUnitName nvarchar(50)
				, OrgUnitShortName nvarchar(50)
				, OrgUnitLongDescription nvarchar(100)
				, OrgUnitParent int
				, OrgUnitReference nvarchar(12)
		)

	begin transaction
	-- list the schools that need an org unit number
		Select DISTINCT S.SchNo
			, R.secCode
		INTO #tmpSec
		FROM Establishment E
			INNER JOIN Schools S
				ON E.schNo = S.SchNo
			LEFT JOIN RoleGrades
				ON E.estpRoleGRade = RoleGRades.RGCode
			LEFT JOIN lkpTeacherRole R
				On RoleGRades.roleCode = R.codeCode

			LEFT JOIN Aurion.SectorCodeOrgUnits ORGU
				ON R.secCode = ORGU.secCode
				AND S.schNo = ORGU.schNo
		WHERE
			R.secCode is not null
			AND OrgU.OrgUnitNumber is null


		declare @ToAllocate int

		-- this is the number of org unit numbers we need
		Select @ToAllocate = count(*)
		from #tmpSec

		-- this is where we'll put them
		declare @counters table
		(
		counter int
		, maxCounters int
		, seq int
		, char nvarchar(6)
		)

		IF (@ToAllocate > 0 ) begin


		-- get the numbers

			insert into @counters
			exec getCounter 'Aurion_OrgUnit', @ToAllocate


			INSERT INTO Aurion.SectorCodeOrgUnits
			SELECT
				schNo
				, secCode
				, C.counter
			FROM
						(Select row_number() over (ORDER BY schNo, secCode) Pos
						, schNo, secCode
						from #tmpSec
						) ORDERED
					INNER JOIN @counters C
						ON ORDERED.Pos = C.seq


		end


	commit transaction

	declare @TopOrgUnit int
	declare @TopOrgLevel int

	Select @TopOrgUnit = common.SysParamInt('Aurion_TopLevelOrgNumber',0)
	Select @TopOrgLevel = common.SysParamInt('Aurion_TopLevelOrgLevel',4)


	INSERT INTO #OrgUnits
	(
		OrgUnitNumber
		, OrgUnitLevel
		, OrgUnitName
		, OrgUnitShortName
		, ORgUnitLongDescription
		, OrgUnitParent
		, OrgUnitReference
	)
	SELECT
		ORGU.OrgUnitNumber
		, @TopOrgLevel + 5
		, secDesc
		, ORGU.secCode
		, secDesc
		, S.schOrgUnitNumber
		, ORGU.secCode

	FROM Aurion.SectorCodeOrgUnits ORGU
		LEFT JOIN Schools S
			on ORGU.schNo = S.schNo
		LEFT JOIN EducationSectors SEC
			ON ORGU.secCode = SEC.secCode
		LEFT JOIN #tmpSec
			ON ORGU.schNo = #tmpSec.schNo
			AND ORGU.secCode = #tmpSEc.secCode
	WHERE
		(#tmpSec.schNo is not null
			OR @SendAll = 1)


	-- return the ones we added
	-- return the ones we added
	If (@Nested = 0) begin
		Select *
		FROM
			#OrgUnits

		drop table #OrgUnits
	end

	drop table #tmpSec

end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


END
GO

