SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-18
-- Description:	Insert an appointment record for a teacher
-- =============================================
-- to optimise
-- cases
--  report only
--  post if no conflicts - report conflicts if found
--  force post (if possible)

-- 1) the proposed appointment has no conflicts - post

-- the proc returns 3 recordset

-- recordset 1 = return value
--    0 written - no conflicts found
--	  1 written - conflict modified
--	  2 not written , no conflicts
--    3 not written, conflicts, can force
--	  4 not written, conflicts, can't force
--	  5 not written, conflict is dup on TId and estpNo ie the teacher is already in the position

-- Field 2 -  not written
--		1 = not written
--      0 = written

-- Field 3
--	0 = no conflict
--  1 = conflict can force
--  2 = conflict cannot force

-- Field 4
--  1 = conflict is self ie matching teacher/position
--  2 = conflict is self same date

-- Field 5 - salary  AND of these values
--  0 is no salaryoint conflict
--  1 is salary point conflict with current payslip
--  2 there is a salary conflict with an active POR
--  4 there is a salary conflict with the selected position


-- recordset 2	- records written

-- recordset 3  - conflicts	- may be modified if return = 1

CREATE PROCEDURE [pEstablishmentWrite].[CreateTeacherAppointment]
	-- Add the parameters for the stored procedure here
	@TeacherID int			-- the teacher identity tID
	, @PositionNo nvarchar(20)		-- the position number
	, @SchoolNo nvarchar(50) = null
	, @RoleGrade nvarchar(10) = null
	, @AppointmentDate datetime
	, @SalaryPoint	nvarchar(10) 		-- the salary point code for the appointment
	, @AppointmentEnd datetime = null		-- openended
	, @AppointmentType nvarchar(20) = 'POSTING'
	, @Flag nvarchar(10) = null
	, @Note nvarchar(400) = null
	, @Verify int = 0			-- 0 = write without forcing
								-- 1 = write and force
								-- 2 = verify only
AS
BEGIN
-- Validate the arguments
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;


	DECLARE @Conflicts TABLE
		(
			ConflictOn int
			, taID int
		)

	DECLARE @Retval int

	declare @resultWritten int
	declare @resultConflictSeverity int
	declare @resultConflictSelf int
	Declare @resultSalaryConflict int
	declare @resultPORConflict int


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- validations

	-- is there an active appointment in this position?
	-- we'll assume you can only make one appointment to a position

	declare @currentAppointmentID int
	declare @teacherAppointmentID int
	declare @PayrollNo nvarchar(20)
	declare @porID int
	declare @porDate datetime
	declare @porRole nvarchar(10)
	declare @PORSalaryPoint nvarchar(10)
	declare @PORStatus nvarchar(10)
	declare @PorSchNo nvarchar(50)
	declare @POREstpNo nvarchar(20)


	begin try


		--

		-- you cannot make a position assignment without a payroll number
		Select @PayrollNo = tPayroll
			from TeacherIdentity
			WHERE tID = @TeacherID

		if (@PayrollNo is null)
			begin
				set @err = 999
				set @ErrorMessage = 'Cannot make an appointment for a teacher that does not have a payroll number.'
				set @ErrorSeverity = 16
				set @ErrorState = 0
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);

			end


		if (@SalaryPoint is null)
			begin
				set @err = 999
				set @ErrorMessage = 'You must enter the salary point.'
				set @ErrorSeverity = 16
				set @ErrorState = 0
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);

			end

		-- you cannot appoint to an Authority Scope appointment without a school and rolegrade

		declare @Scope nvarchar(1)
		declare @PosStart datetime
		declare @PosEnd datetime
		declare @PosSchoolNo nvarchar(50)			-- school of the position
		declare @POSRoleGrade nvarchar(10)
		declare @POSRole nvarchar(10)


		Select @Scope = estpScope
		, @PosStart = estpActiveDate
		, @PosEnd = estpClosedDate
		, @POSSchoolNo = schNo
		, @POSRoleGrade = estpRoleGrade
		, @POSRole = roleCode
		FROM Establishment
			INNER JOIN RoleGRades RG
				ON Establishment.estpRoleGRade = RG.rgCode
		WHERE estpNo = @PositionNo

		-- check the position dates against the appointment dates
		if @AppointmentDate < @PosStart
			OR @AppointmentDate > @PosEnd begin

				set @err = 999
				set @ErrorMessage = 'The dates for this appointment are outside the Open and Close dates of the position.'
				set @ErrorSeverity = 16
				set @ErrorState = 0
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		end


		if (@Scope = 'A') begin
			----if (@SchoolNo is null) begin
			----	set @err = 999
			----	set @ErrorMessage = 'Cannot appoint to a position of Authority Scope without supplying a school number.'
			----	set @ErrorSeverity = 16
			----	set @ErrorState = 0
			----	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);

			----end
			if (@RoleGrade is null) begin
				set @err = 999
				set @ErrorMessage = 'Cannot appoint to a position of Authority Scope without supplying a role/grade.'
				set @ErrorSeverity = 16
				set @ErrorState = 0
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);

			end
		end
		-- not required for school scope positions
		if (@Scope is null) begin
			Select @SchoolNo = null
			Select @RoleGRade = null
		end


		-- find any related POR

		SELECT TOP 1 @PORID = porID
		, @PORSTatus = porStatus
		, @porRole = porRoleNew
		, @porSalaryPoint = porAdjLvl
		, @PORDate  = porDate
		, @PORSchNo = schNo
		, @POREstpNo = POREstpNo
		FROM POR
		WHERE POR.tID = @TeacherID
		ORDER BY porDate DESC

		if (@PORID is not null) begin
			-- test for a POR conflict
			-- e.g. attempted to appoint to this position and rejected
			if (@POREStpNo = @PositionNo) begin
					-- if this is any status other than tchadd, its a problem
					-- either its not approved yet, its rejected, or its already done
					if (@PORStatus <> 'TCHADD') begin
						Select @ResultPORConflict = 1
					end
				end
			else begin

				-- is there a por in progress for this school same role?
				if (@POREStpNo is null and
						@PORSchNo = @POSSchoolNo and
						@PORRole = @POSRole
					) begin
					-- if we haven't selected a position from the por we shouldnt be allowed to proceed here
						Select @ResultPORConflict = 1
					end
				----else begin
				----	-- open POR to appoint to a DIFFERENT school or position
				----	if (@PORStatus not in ('FIN'
			end


		end


-- business rules about salary
-- salary can only be incremented if there is a POR to control that
-- default salary is the current salary, or if no salary, the minimum rate for the position (his is what aurion will do anyway)

-- if a supernumerary default is the current salary.

/*
	if the salary is null
		- use the last salary from the payslip if there is one
		- else, use the minimum allowed for the grade

	if the salary is not null
		-- if it is same as last pay slip, ok
		-- if not the same
				-- should be a POR to confirm change
				-- user is logged


*/

		declare @CurrentSalary nvarchar(10)

		SELECT TOP 1 @CurrentSalary = tpsSalaryPoint
		FROM TEacherPayslips
		WHERE tpsPayroll = @PayrollNo
		ORDER BY tpsPeriodEnd DESC


		Select 	@ResultSalaryConflict = 0
-- on the other hand, if we are supposed to give them a raise becuase of a POR , and we don't
-- that ought to be an exception too
		if (@SalaryPoint <> @PORSalaryPoint and @resultPORConflict = 0 and  @PORID is not null) begin
				Select @resultSalaryConflict = 2
			end

		if (@CurrentSalary <> @SalaryPoint) begin


			Select @ResultSalaryConflict = @ResultSalaryConflict + 1

		end


		-- does the salary lie in the range for the position

		declare @SPSortMin int
		declare @SPSortMax int

		SELECT
			@SPSortMin = min(spSort)
			, @SPSortMax = max(spSort)
		FROM
			EStablishment E
			INNER JOIN RoleGrades RG

				ON  isnull(E.estpRoleGRade, @RoleGrade) = RG.rgCode

			INNER JOIN lkpSalaryPoints SP
				ON SP.salLevel in (RG.rgSalaryLevelMin, RGSalaryLevelMax)
			WHERE estpNo = @PositionNo


		Select @resultSalaryConflict =
			case when count(spCode) = 1 then @resultSalaryConflict
				else @resultSalaryConflict + 4 end
		FROM lkpSalaryPoints
		WHERE spCode = @SalaryPoint
		AND spSort between @SPSortMin and @SPSortMax


		begin transaction

		-- these are date conflicts on position
		INSERT INTO @Conflicts
		Select 0		-- manes a position conflict
		, taID
		from TeacherAppointment TA
		WHERE
			 TA.taDate < isnull(@appointmentEnd,'2999-12-31')
				AND @AppointmentDate < isnull(taEndDate,'2999-12-31')
				AND estpNo = @PositionNo


		-- these are date conflicts on teacher
		INSERT INTO @Conflicts
		Select 1		-- manes a teacher conflict
		, taID
		from TeacherAppointment TA
		WHERE
			 TA.taDate < isnull(@appointmentEnd,'2999-12-31')
				AND @AppointmentDate < isnull(taEndDate,'2999-12-31')
				AND tID = @TeacherID


-- what is the outcome?
-- if there are no conflicts, we can write if we want

		declare @ConflictCount int
		declare @PosConflictCount int
		declare @TeacherConflictCount int
		declare @UniqueConflictCount int

		Select @ConflictCount = count(*)
		, @PosConflictCount = sum (case when ConflictOn = 0 then 1 else null end)
		, @TeacherConflictCount = sum (case when ConflictOn = 1 then 1 else null end)
		, @UniqueConflictCount = count(DISTINCT taID)
		FROM @conflicts


-- initialise som eretuin values

		if (@verify = 2)
			select @resultWritten = 0
		else
			select @resultWritten = 1		-- turn it off later if we have to

		select @resultConflictSelf = 0
		select @resultConflictSeverity = 0


		If (@ConflictCount > 1)
			begin
				If (@UniqueConflictCount = 1)
					-- ther a remultiple conflicts but they point to the same taID
					-- ie this record is the conflict on both position and tID
					-- which means that the employee is already in that position
					-- this can be fixed
					begin
						select @resultConflictSelf = 1
						select @resultConflictSeverity = 1 -- can force this
					end
				else
					select @resultConflictSeverity = 2 -- can't force
			end


		If (@ConflictCount = 1)
			begin
				if (@PosConflictCount > 0 )

					select @resultConflictSeverity = 2 -- can't force

				-- we are left with a single teacher conflict
				-- we can autofix this in these cases
				--  1) the dateof the conflict is earlier than the current date
				--		and it is open-ended =>truncate the exisitng appointment

				-- 2) the date of the conflict is later than the propsoed date
				--		and the current is openened => delete the future appointment

				if (isnull(@PosConflictCount,0) = 0)		-- the one conflict is a teacher conflict

					begin


						Select @resultConflictSeverity =
							(case when (taDate < @AppointmentDate and taEndDate is null )
								or (taDate >= @AppointmentDate and @AppointmentEnd is null)
							then 1 else 2
							end)
						from @Conflicts Conflicts
						INNER JOIN TeacherAppointment TA
								ON TA.taID = Conflicts.taID


					end

			end				-- @ConflictCount = 1

------Select @resultConflictSeverity =
------	case when @resultConflictSeverity = 0 and @resultSalaryConflict > 0 then 1 else @resultConflictSeverity end
------Select @resultConflictSeverity =
------	case when @resultConflictSeverity = 0 then
------		@resultPORConflict else @resultConflictSeverity end


If (@resultConflictSeverity = 2 )
	SELECT @resultWritten = 0

if (@Verify = 0)
	begin
		If (@resultConflictSeverity = 1
				OR @resultSalaryConflict > 0
				OR @resultPORConflict > 0
			)
			SELECT @resultWritten = 0


	end

/*---------------------------------------------------------------
	return
----------------------------------------------------------------*/
-- the first recordset we select is @RetVal

SELECT @RetVal = case  @resultWritten
					when 1 then
						case @resultConflictSeverity when 0 then 0
							when 1 then 1
						end

					when 0 then
						case @resultConflictSeverity when 0 then 2
							when 1 then 3
							when 2 then 4
						end
				end

		SELECT @Retval	ResultCode
			, @resultWritten Written
			, @resultConflictSeverity ConflictSeverity
			, @resultConflictSelf ConflictSelf
			, @resultSalaryConflict SalaryConflict
			, @ResultPORConflict PORConflict
			, @PORID porID


		If (@resultWritten = 0)
			-- for recordset 2 , return the values passed in

			SELECT
				null taID
				, @TeacherID tID		-- the teacher identity tID
				, @PositionNo estpNo
				, @AppointmentDate taDate
				, @AppointmentEnd taEndDate
				, @SalaryPoint	taSalaryPt


--------------------------------------------------------------
-- Force apointments
--------------------------------------------------------------
		If (@resultWritten = 1 )
			begin
				if (@resultConflictSeverity = 1)
					begin
						-- now we have to try to fix somehing
						-- truncate the exisitng ealier appointment if need be

						UPDATE TeacherAppointment
						SET taEndDate = @AppointmentDate - 1
						FROM TeacherAppointment
							INNER JOIN @Conflicts  Conflicts
							ON TeacherAppointment.taID = Conflicts.taID
						WHERE taDate < @AppointmentDate
							AND taEndDate is null

						DELETE FROM TEacherAppointment
						FROM TEacherAppointment
							INNER JOIN @Conflicts Conflicts
							ON TeacherAppointment.taID = Conflicts.taID
						WHERE taDate >= @AppointmentDate
							AND @AppointmentEnd is null

					end

------------------------------------------------------------------
-- Insert
------------------------------------------------------------------


				INSERT INTO TeacherAppointment
						(tID, estpNo
						, schNo
						, taRoleGrade
						, taDate, taType, spCode, taEndDate
						, taFlag, taNote)
					VALUES (@TeacherID
							, @PositionNo
							, @SchoolNo
							, @RoleGrade
							, @AppointmentDate
							, @AppointmentType
							, @SalaryPoint
							, @AppointmentEnd
							, @Flag
							, @Note)


				Select * from TeacherAppointment WHERE taID = scope_identity()		-- becuase the trigger writes the log
			end


------------------------------------------------------------------
		-- for the last recordset, return the conflicts
------------------------------------------------------------------
		SELECT ConflictOn
			, E.*
			, Schools.schAuth
			, Schools.SchName
			, Schools.schType
			, Appts.taID
			, Appts.taDate
			, Appts.taEndDate
			, Appts.taType
			, TI.tID
			, TI.tNAmePrefix
			, TI.tGiven
			, TI.tMiddleNames
			, TI.tSurname
			, TI.tNameSuffix
			, TI.tShortName
			, TI.tFullName
			, TI.tLongName
			, TI.tPayroll
			, TI.tDOB
			, TI.tSex
			, Appts.spCode
			, Appts.taDate
			, RG.rgCode
			, RG.rgDescription
			, TR.codeDescription RoleDesc
		FRom @Conflicts Conflicts
			INNER JOIN TeacherAppointment Appts
				ON Conflicts.taID = Appts.taID
			LEFT JOIN Establishment E
				on E.estpNo = Appts.estpNo
			LEFT JOIN TeacherIdentity TI
				on Appts.tID = TI.tID
			LEFT JOIN Schools
				ON E.schNo = Schools.schNo
			LEFT JOIN TRRoleGrades RG
				ON isnull(Appts.taRoleGrade,E.estpRoleGrade) = RG.rgCode
			LEFT JOIN TRTeacherRole TR
				ON RG.roleCode = TR.codeCode


	end try

--- catch block
	begin catch

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch
----

	-- and , go!
	if @@trancount > 0
		begin
			commit transaction
		end

-- our long journey from the left margin is at an END
END
GO

