SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 03 2010
-- Description:	Pivo ttable on Work Orders
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[PIVWorkOrders]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select * from PIVWorkOrdersVIEW

END
GO

