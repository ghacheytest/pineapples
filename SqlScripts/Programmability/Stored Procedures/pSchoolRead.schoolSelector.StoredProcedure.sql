SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pSchoolRead].[schoolSelector]
	@s nvarchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
if (isnumeric(@s) = 1) begin
	select top 30 schNo + ': ' + schName N, schNo C
	from Schools
	WHERE schNo like @s +'%'

end
else begin
	select top 30 schName N, schNo C
	FROM
	(
	Select TOP 30 schName, schNo
	from Schools
	WHERE schName like @s +'%'
	UNION
	Select TOP 30 schName, schNo
	from Schools
	WHERE schName like '_%' + @s +'%'
	) SUB
end
select @@rowcount num
END
GO

