SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 6 2009
-- Description:	Update resource provision from XML
-- =============================================
CREATE PROCEDURE [pSchoolWrite].[updateResourceProvision]
	-- Add the parameters for the stored procedure here
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml


begin try
	declare @category nvarchar(50)
	declare @schoolLevel nvarchar(10)

	Select @category = category
		, @schoolLevel = schoolLevel

	from OPENXML (@idoc, '/ResourceProvision/Resource/data',2)
		WITH (
				category nvarchar(50) '../../@category'
				, schoolLevel nvarchar(10) '../../@schoolLevel'

				)

	if (@schoolLevel = '')
		select @schoolLevel = null

	Select @surveyID SurveyID
			, @category Category
			, item
			, yearSupplied
			, number
			, @schoolLevel schoolLevel
			, classLevel
	INTO #rp
		from OPENXML (@idoc, '/ResourceProvision/Resource/data',2)
		WITH (
				item nvarchar(50) '../@item'
				, number int '@number'
				, yearSupplied int '@yearSupplied'
				, classLevel nvarchar(10)  '@classLevel'
				)

	begin transaction

	Delete from ResourceProvision
		WHERE ssID = @SurveyID
			and rpType = @category
			and isnull(rpSchLevel,'') = isnull(@schoolLevel,'')
			and rpSubject in (Select item from #rp)
			and (rpLevel in (Select classLevel from #rp) or rpLevel is null)		--15 11 2009 suport SI version of this

	INSERT INTO ResourcePRovision
		(ssID, rpType, rpSubject, rpYear, rpNum
			, rpSchLevel, rpLevel
		)

	Select surveyID
			, category
			, item
			, yearSupplied
			, number
			, schoolLevel
			, classLevel
		from #rp
		WHERE #rp.YearSupplied  is not null
		or #rp.number is not null
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

return
    -- Insert statements for procedure here
END
GO

