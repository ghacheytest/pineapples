SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 2 2010
-- Description:	Lite version of EFA5 query
-- =============================================
/*
<Indicators>

<EnrolmentRatios levelCode = "PRI" levelSet ="Alt2" levelName="Basic" minYearOfEd maxYearOfEd
			minAge maxAge
	<Year year=2005><Data gender="M"><GER><NER><Enrol><Pop><nEnrol></Data>

<TeacherQual sector="SEC">
	<Year year=2005><Data gender="M"><Num><NumCert><NumQual>NumCertQual><CertR><QualRatio><CertQualRAtio>
*/
CREATE PROCEDURE [dbo].[EFAScoreCard]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #erData
(
[Survey Year] int
, Estimate int
--, [District Code] nvarchar(10)
--, District nvarchar(50)
--, [Island Code] nvarchar(10)
--, Island nvarchar(50)
--, ElectorateN nvarchar(10)
--, ElectorateL nvarchar(10)
, Age int
, LevelCode nvarchar(10)
, enrolM int
, enrolF int
, repM int
, repF int
, popM int
, popF int
, intakeM int
, intakeF int
, popModCode nvarchar(10)
, popModName nvarchar(50)
, popModDefault int
, atAgeLevel int
, atEdLevelAge int
, atEdLevelAltAge int
, atEdLevelAlt2Age int
, edLevelCode nvarchar(10)
, edLevelAltCode nvarchar(10)
, edLevelAlt2Code nvarchar(10)
)

-- 7 10 2010
-- figure out the starting year of each ed level/alt/alt2

CREATE TABLE #edlRanges
(
num int			-- 0 1 or 2 for later cross join
, edlCode nvarchar(10)
, edlMinYear int
, edlMaxYear int
)

INSERT INTO #edlRanges
SELECT 0
, codeCode
, edlMinYear
, edlMaxYear
FROM lkpEducationLevels

INSERT INTO #edlRanges
SELECT 1
, codeCode
, edlMinYear
, edlMaxYear
FROM lkpEducationLevelsAlt

INSERT INTO #edlRanges
SELECT 2
, codeCode
, edlMinYear
, edlMaxYear
FROM lkpEducationLevelsAlt2

declare @FirstYear int

select @FirstYear = year(getdate()) - 5

--- from this make all enrolment ratio data
---

INSERT INTO #erData
EXEC dbo.sp_EFA5Data 1, @FirstYear


declare @XMLER xml

Select @XMLER =
(
Select *
, cast(enrolM as float)/popM	gerM
, cast(enrolF as float)/popF	gerF
, cast(enrol as float)/pop		ger
, cast(nEnrolM as float)/popM 	nerM
, cast(nEnrolf as float)/popF  nerF
, cast(nEnrol as float)/pop	ner
-- 7 10 2010 add intake rates
, cast(intakeM as float) / intakePopM girM
, cast(intakeF as float) / intakePopF girF
, cast(intake as float) / intakePop gir

, cast(nintakeM as float) / intakePopM nirM
, cast(nintakeF as float) / intakePopF nirF
, cast(nintake as float) / intakePop nir

, (cast(nEnrolf as float)/popF)/(cast(nEnrolM as float)/popM) nerGPI
, (cast(Enrolf as float)/popF)/(cast(EnrolM as float)/popM) gerGPI
, (cast(nIntakef as float)/intakepopF)/(cast(nIntakeM as float)/intakepopM) nirGPI
, (cast(Intakef as float)/IntakepopF)/(cast(IntakeM as float)/IntakepopM) girGPI

from
(
Select [Survey Year]
, case num when 0 then 'EdLevel'
			when 1 then 'Alt'
			when 2 then 'Alt2'
	end levelSet

, case num when 0 then edLevelCode
			when 1 then edLevelAltCode
			when 2 then edLevelAlt2Code
	end edLevelCode
, sum(enrolM) enrolM
, sum(enrolF) enrolF
, sum(isnull(enrolM,0)+isnull(enrolF,0)) Enrol
, sum(repM) repM
, sum(repF) repF
, sum(popM) popM
, sum(popF) popF
, sum(isnull(popM,0) + isnull(popF,0)) pop
, sum(case when lvlYear = ER.edlMinYear then intakeM else null end) intakeM
, sum(case when lvlYear = ER.edlMinYear then intakeF else null end) intakeF
, sum(case when lvlYear = ER.edlMinYear then isnull(intakeM,0) + isnull(intakeF,0) else null end) intake

, sum(case when (
					(num = 0 and atEdLevelAge = 1)
					or (num = 1 and atEdLevelAltAge = 1)
					or (num = 2 and atEdLevelAlt2Age = 1)
				)

					then enrolM else null end) nEnrolM
, sum(case when (
					(num = 0 and atEdLevelAge = 1)
					or (num = 1 and atEdLevelAltAge = 1)
					or (num = 2 and atEdLevelAlt2Age = 1)
				)

					then enrolF else null end) nEnrolF
, sum(case when (
					(num = 0 and atEdLevelAge = 1)
					or (num = 1 and atEdLevelAltAge = 1)
					or (num = 2 and atEdLevelAlt2Age = 1)
				)

					then isnull(enrolM,0) + isnull(enrolF,0) else null end) nEnrol
-- net intakes
, sum(case when lvlYear = ER.edlMinYear and atAgeLevel = 1


					then intakeM else null end) nIntakeM

, sum(case when lvlYear = ER.edlMinYear and atAgeLevel = 1

					then intakeF else null end) nIntakeF

, sum(case when lvlYear = ER.edlMinYear and atAgeLevel = 1

					then isnull(intakeM,0) + isnull(intakeF,0) else null end) nIntake
-- population of official start age for the level

-- net intakes
, sum(case when lvlYear = ER.edlMinYear and
				(
					(num = 0 and atEdLevelAge = 1)
					or (num = 1 and atEdLevelAltAge = 1)
					or (num = 2 and atEdLevelAlt2Age = 1)
				)

					then popM else null end) intakePopM

, sum(case when lvlYear = ER.edlMinYear and
				(
					(num = 0 and atEdLevelAge = 1)
					or (num = 1 and atEdLevelAltAge = 1)
					or (num = 2 and atEdLevelAlt2Age = 1)
				)

					then popF else null end) intakePopF

, sum(case when lvlYear = ER.edlMinYear and
				(
					(num = 0 and atEdLevelAge = 1)
					or (num = 1 and atEdLevelAltAge = 1)
					or (num = 2 and atEdLevelAlt2Age = 1)
				)

					then isnull(popM,0) + isnull(popF,0) else null end) intakePop
from #erData
INNER JOIN lkpLevels
ON #erData.levelCode = lkpLevels.codeCode
CROSS JOIN #edlRanges ER
WHERE
	case num when 0 then edLevelCode
			when 1 then edLevelAltCode
			when 2 then edLevelAlt2Code
	end = ER.edlCode


GROUP BY [Survey YEar], num
, case num when 0 then edLevelCode
			when 1 then edLevelAltCode
			when 2 then edLevelAlt2Code
	end
) sub
WHERE edLevelCode is not null
ORDER BY [Survey Year],edLevelCode
FOR XML AUTO
)
--SELECT * from #erData


 Select @XMLER


-- the second recordset is ger ner gir nir for individual class levels
-- note that we have the issue of matching up a population against the class level
-- this comes from yearofeducation, but at the moment, becuase of the pivot table origins of sp_EFA5Data
-- it only includes population for class levels that are part of th default education path.
-- This ensures that pivot tables don;t double up population when 2 class levels have the same year of ed
-- Means that e.g. SI RTC does not get a population value
declare @XMLER2 xml

Select @XMLER2 =
(
Select *
, case when isnull(popM,0) = 0 then null else cast(enrolM as float)/popM end	gerM
, case when isnull(popF,0) = 0 then null else cast(enrolF as float)/popF end	gerF
, case when isnull(pop,0) = 0 then null else cast(enrol as float)/pop end	ger

, case when isnull(popM,0) = 0 then null else cast(nEnrolM as float)/popM end	nerM
, case when isnull(popF,0) = 0 then null else cast(nEnrolF as float)/popF end	nerF
, case when isnull(pop,0) = 0 then null else cast(nEnrol as float)/pop end	ner


-- 7 10 2010 add intake rates
, case when isnull(intakePopM,0) = 0 then null else cast(intakeM as float) / intakePopM end girM
, case when isnull(intakePopF,0) = 0 then null else cast(intakeF as float) / intakePopF end girF
, case when isnull(intakePop,0) = 0 then null else cast(intake as float) / intakePop end gir

, case when intakePopM = 0 then null else cast(nIntakeM as float) / intakePopM end nirM
, case when intakePopF = 0 then null else cast(nIntakeF as float) / intakePopF end nirF
, case when intakePop = 0 then null else cast(nIntake as float) / intakePop end nir

, (cast(nEnrolf as float)/popF)/(cast(nEnrolM as float)/popM) nerGPI
, (cast(Enrolf as float)/popF)/(cast(EnrolM as float)/popM) gerGPI
, (cast(nIntakef as float)/intakepopF)/(cast(nIntakeM as float)/intakepopM) nirGPI
, (cast(Intakef as float)/IntakepopF)/(cast(IntakeM as float)/IntakepopM) girGPI

from
(
Select [Survey Year]
, levelCode
, sum(enrolM) enrolM
, sum(enrolF) enrolF
, sum(isnull(enrolM,0)+isnull(enrolF,0)) Enrol
, sum(repM) repM
, sum(repF) repF
, sum(popM) popM
, sum(popF) popF
, sum(isnull(popM,0) + isnull(popF,0)) pop
, sum(intakeM) intakeM
, sum(intakeF) intakeF
, sum(isnull(intakeM,0) + isnull(intakeF,0)) intake

, sum(case when atAgeLevel = 1 then enrolM else null end) nEnrolM
, sum(case when atAgeLevel = 1 then enrolF else null end) nEnrolF
, sum(case when atAgeLevel = 1 then isnull(enrolM,0) + isnull(enrolF,0) else null end) nEnrol

-- net intakes
, sum(case when atAgeLevel = 1 then intakeM else null end) nIntakeM
, sum(case when atAgeLevel = 1 then intakeF else null end) nIntakeF
, sum(case when atAgeLevel = 1 then isnull(intakeM,0) + isnull(intakeF,0) else null end) nIntake

-- population of official start age for the level
, sum(case when atAgeLevel = 1 then popM else null end) intakePopM
, sum(case when atAgeLevel = 1 then popF else null end) intakePopF
, sum(case when atAgeLevel = 1 then isnull(popM,0) + isnull(popF,0) else null end) intakePop

from #erData
INNER JOIN lkpLevels
ON #erData.levelCode = lkpLevels.codeCode
INNER JOIN Survey
ON Survey.svyYEar = #erData.[Survey Year]


GROUP BY [Survey YEar]
, levelCode
) sub
ORDER BY [Survey Year],levelCode
FOR XML AUTO
)
--SELECT * from #erData


 Select @XMLER2

END
GO

