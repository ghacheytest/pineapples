SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 11 2007
-- Description:	add a note tothe school notes table
-- =============================================
CREATE PROCEDURE [dbo].[addSchoolNote]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50)  ,
	@Subject nvarchar(255),
	@Note ntext,
	@Contact nvarchar(50) = null,
	@SurveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
INSERT INTO SchoolNotes
(
	schNo,
	ntDate,
	ntAuthor,
	ntContact,
	ntsubject,
	ntNote,
	svyYear
)
VALUES
(
	@schoolNo,
	getdate(),
	system_user,
	@Contact,
	@Subject,
	@Note,
	@SurveyYear
)
END
GO
GRANT EXECUTE ON [dbo].[addSchoolNote] TO [pSchoolWriteX] AS [dbo]
GO

