SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: <Create Date,,>
-- Description:	Calculates key statistics of the Flow model, at the national level
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[ReconstructedCohort2]
	-- Add the parameters for the stored procedure here
	@SendASXML int = 0
	, @xmlOut xml  = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- this table is the raw data for each school, year and level
    CREATE TABLE #d
(
	schNo nvarchar(50)
	, svyYear int
	, levelCode nvarchar(10)
	, yearOfEd int
	, enrolM int
	, enrolF int
	, enrol int
	, repM int
	, repF int
	, rep int
	, IntakeM int
	, IntakeF int
	, Intake int
	, estimate int
	, Age int
)

CREATE NONCLUSTERED INDEX ddT on #D
(
	schNo ASC
	, svyYear ASC
	, yearOfEd ASC
)


-- this table expands the #d table to include the Next Year columns, at the same level, and the next level up
CREATE TABLE #dd
(
	schNo nvarchar(50)
	, svyYear int
	, levelCode nvarchar(10)
	, yearOfEd int
	, enrolM int
	, enrolF int
	, enrol int
	, repM int
	, repF int
	, rep int
	, IntakeM int
	, IntakeF int
	, Intake int
	, estimate int
	, Age int
	, enrolNYM int
	, enrolNYF int
	, enrolNY int
	, repNYM int
	, repNYF int
	, repNY int
	, IntakeNYM int
	, IntakeNYF int
	, IntakeNY int
	, estimateNY int
	, AgeNY int
	, enrolNYNLM int
	, enrolNYNLF int
	, enrolNYNL int
	, repNYNLM int
	, repNYNLF int
	, repNYNL int
	, IntakeNYNLM int
	, IntakeNYNLF int
	, IntakeNYNL int
	, SlopedEnrolM float
	, SlopedEnrolF float
	, SlopedEnrol float

)

CREATE NONCLUSTERED INDEX ddT2 on #dd
(
	schNo ASC
	, svyYear ASC
	, yearOfEd ASC
)

-- get all the estimated enrolment data
Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()

-- this table is all the possible combinations we may want to look at
-- every combination of school and year adn level code that will have data in the svyYear,
-- or data in the same level the following year
-- or data in the next level the following following year


declare @t table
(
	schNo nvarchar(50)
	, svyYear int
	, levelCode nvarchar(10)
	, yearOfEd int
)


INSERT INTO #d
(
	schNo
	, svyYear
	, levelCode
	, yearOfEd
	, enrolM
	, enrolF
	, enrol
	, repM
	, repF
	, rep
	, IntakeM
	, IntakeF
	, Intake
	, estimate
	, age

)
	SELECT
			EBSE.schNo,
			EBSE.LifeYear svyYear,
			EL.LevelCode,
			L.lvlYear,
			isnull(EL.enrolM,0) ,
			isnull(EL.EnrolF,0) ,
			isnull(enrolM,0) + isnull(enrolF,0),
			isnull(RL.repM,0),
			isnull(RL.repF,0),
			isnull(repM,0) + isnull(repF,0),
			isnull(EL.enrolM,0) - isnull(RL.repM,0) ,
			isnull(EL.EnrolF,0) - isnull(RL.repF,0),
			isnull(enrolM,0) + isnull(enrolF,0)- isnull(RL.repM,0) - isnull(RL.repF,0),
			EBSE.Estimate,
			EBSE.offset
		FROM #ebse EBSE
		inner join pEnrolmentRead.ssidEnrolmentLevel EL
		on EBSE.bestssID = El.ssID
		INNER JOIN lkpLevels L
			on EL.levelCode = L.codeCode
		-- note the left join becuase we should never have repeaters without enrolments
		left join pEnrolmentRead.ssIDRepeatersLevel RL
		on EBSE.bestssID = Rl.ssID
		AND EL.levelCode = RL.ptLevel


INSERT INTO @t
Select
	schNo
	, svyYear
	, levelCode
	, yearOfEd
FROM #d D
UNION
Select
	schNo
	, svyYear -1
	,levelCode
	, yearOfEd
FROM #d D
UNION
-- 3rd part picks up wher ewe may have next year next level, but nothing for this year
Select
	SchNo
	, svyYear - 1
	, DPL.levelCode
	, D.yearOfEd - 1
FRom #d D
INNER JOIN common.LISTDEFAULTPAthLevels DPL
	ON D.yearOfEd -1 = DPL.YearOfEd

-- finsihed with this
drop table #ebse


INSERT INTO #DD
SELECT T.schNo
	, T.svyYear
	, T.levelCode
	, T.YearOfEd
	, Y.enrolM
	, Y.enrolF
	, Y.enrol
	, Y.repM
	, Y.repF
	, Y.rep
	, Y.IntakeM
	, Y.IntakeF
	, Y.Intake
	, Y.estimate
	, Y.age
	, NY.enrolM		enrolNYM
	, NY.enrolF		enrolNYF
	, NY.enrol		enrolNY
	, NY.repM		repNYM
	, NY.repF		repNYF
	, NY.rep		repNY
	, NY.IntakeM	intakeNYM
	, NY.IntakeF	intakeNYF
	, NY.Intake		intakeNY

	, NY.estimate	EstimateNY
	, NY.age		ageNY
	, NYNL.enrolM		enrolNYNLM
	, NYNL.enrolF		enrolNYNLF
	, NYNL.enrol		enrolNYNL
	, NYNL.repM		repNYNLM
	, NYNL.repF		repNYNLF
	, NYNL.rep		repNYNL
	, NYNL.IntakeM	intakeNYNLM
	, NYNL.IntakeF	intakeNYNLF
	, NYNL.Intake	intakeNYNL

--
-- now the calculated
	, (case when Y.[Age] > 0 and NY.Estimate = 0
					then (Y.age * NY.enrolM + Y.EnrolM) / cast((Y.age   + 1) as float)
											else Y.enrolM end ) SlopedEnrolM
	, (case when Y.[Age] > 0 and NY.Estimate = 0
					then (Y.age * NY.enrolF + Y.EnrolF) / cast((Y.age   + 1) as float)
											else Y.enrolF end ) SlopedEnrolF

	, (case when Y.[Age] > 0 and NY.Estimate = 0
					then (Y.age * NY.enrol + Y.Enrol) / cast((Y.age   + 1) as float)
											else Y.enrol end ) SlopedEnrol

FROM @t T
-- @t is now guaranteed to have every row it needs, so we can outer join from here to the other 3 parts
LEFT JOIN #D Y
	ON T.schNo = Y.schNo
	AND T.svyYear = Y.svyYear
	AND T.yearOfEd = Y.yearOfEd
LEFT JOIN #D NY
	ON T.schNo = NY.schNo
	AND T.svyYear + 1 = NY.svyYear
	AND T.yearOfEd = NY.yearOfEd
LEFT JOIN #D NYNL
	ON T.schNo = NYNL.schNo
	AND T.svyYear + 1 = NYNL.svyYear
	AND T.yearOfEd + 1 = NYNL.yearOfEd

	DROP TABLE #d

-- this nestngs allow the calculation of progressively more dependant results
SELECT sub2.*
-- dropout rate, Transition rate
, (1 - RRM-PRM) DRM
, (1 - RRF-PRF) DRF
, (1 - RR-PR) DR
, case when RRM = 1 then 1 else (PRM)/(1-RRM) end TRM
, case when RRF = 1 then 1 else (PRF)/(1-RRF) end TRF
, case when RR = 1 then 1 else (PR)/(1-RR) end TR
, cast(1 as Float)  SRM
, cast(1 as Float)  SRF
, cast(1 as Float)  SR

INTO #results
FROM
(
SELECT sub.*
	, case when SlopedenrolM = 0 then 0 else RepNYM / SlopedenrolM end RRM
	, case when SlopedenrolF = 0 then 0 else RepNYF / SlopedenrolF end RRF
	, case when SlopedEnrol = 0 then 0 else RepNY /SlopedEnrol end RR

	, case when SlopedenrolM = 0 then 0 else (IntakeNYNLM)/ SlopedenrolM end PRM
	, case when SlopedenrolF = 0 then 0 else (IntakeNYNLF)/ SlopedenrolF end PRF
	, case when Slopedenrol = 0 then 0 else (IntakeNYNL)/ Slopedenrol end PR
FROM
(
SELECT svyYear
	, levelCode
	, yearOfEd
	, sum(enrolM)		enrolM
	, sum(enrolF)		enrolF
	, sum(enrol)		enrol
	, sum(repM)		repM
	, sum(repF)		repF
	, sum(rep)		rep
	, sum(IntakeM)		intakeM
	, sum(IntakeF)		intakeF
	, sum(Intake)		intake
	--, Y.estimate
	--, Y.age
	, sum(enrolNYM)		enrolNYM
	, sum(enrolNYF)		enrolNYF
	, sum(enrolNY)		enrolNY
	, sum(repNYM)		repNYM
	, sum(repNYF)		repNYF
	, sum(repNY)		repNY
	, sum(IntakeNYM)	intakeNYM
	, sum(IntakeNYF)	intakeNYF
	, sum(IntakeNY)		intakeNY

	--, NY.estimate	EstimateNY
	--, NY.age		ageNY
	, sum(enrolNYNLM)		enrolNYNLM
	, sum(enrolNYNLF)		enrolNYNLF
	, sum(enrolNYNL)		enrolNYNL
	, sum(repNYNLM)		repNYNLM
	, sum(repNYNLF)		repNYNLF
	, sum(repNYNL)			repNYNL
	, sum(IntakeNYNLM)		intakeNYNLM
	, sum(IntakeNYNLF)		intakeNYNLF
	, sum(IntakeNYNL)		intakeNYNL

--
	, round(sum(SlopedEnrolM),0)		SlopedEnrolM
	, round(sum(SlopedEnrol) ,0) - round(sum(SlopedEnrolM),0)		SlopedEnrolF
	, round(sum(SlopedEnrol),0)		SlopedEnrol


FROM #dd
GROUP BY svyYear, levelCode, yearOfEd
) sub
) sub2
ORDER By svyYEar, yearOfEd
drop table #dd

declare @i int
declare @max int
SELECT @max = max(lvlYear)
from lkpLevels

-- this part multiplies the transitions rates together
Select @i = 1
while (@i < @max) begin
	UPDATE #Results
	SET SRM = T.SRM * T.TRM
	, SRF = T.SRF * T.TRF
	, SR = T.SR * T.TR
	FROM #REsults
		INNER JOIN #results T
	ON #results.svyYear = T.svyYear
		AND #results.yearOfEd = T.yearOfEd + 1
		AND T.yearofEd = @i
		AND T.levelCode in (Select levelCode from common.LISTDEFAULTPAthLevels)

	SELECT @i = @i + 1

end

IF @SendASXML = 0 begin
	SELECT * from #results

end

IF @SendASXML <> 0 begin

	declare @XML xml
	SELECT @XML =
	(
		SElect svyYear [@year]
		, levelCode [@levelCode]
		,yearOfEd	[@yearOfEd]
		, EnrolM
		, EnrolF
		, Enrol
		, RepM
		, RepF
		, Rep
		, cast(SlopedEnrolM as int) SlopedEnrolM
		, cast(SlopedEnrolF as int)SlopedEnrolF
		, cast(SlopedEnrol as int)SlopedEnrol
		, cast(PRM as decimal(8,5)) PRM
		, cast(PRF as decimal(8,5)) PRF
		, cast(PR as decimal(8,5)) PR
		, cast(RRM as decimal(8,5)) RRM
		, cast(RRF as decimal(8,5)) RRF
		, cast(RR as decimal(8,5)) RR
		, cast(DRM as decimal(8,5)) DRM
		, cast(DRF as decimal(8,5)) DRF
		, cast(DR as decimal(8,5)) DR
		, cast(TRM as decimal(8,5)) TRM
		, cast(TRF as decimal(8,5)) TRF
		, cast(TR as decimal(8,5)) TR
		, cast(SRM as decimal(8,5)) SRM
		, cast(SRF as decimal(8,5)) SRF
		, cast(SR as decimal(8,5)) SR
	FROM #results
	FOR XML PATH('Survival')
	)

	SELECT @XMLOut =
	(
		SELECT @xml
		FOR XML PATH('Survivals')
	)

	IF @SendASXML = 1
		SELECT @XMLOut
end

drop table #results


END
GO

