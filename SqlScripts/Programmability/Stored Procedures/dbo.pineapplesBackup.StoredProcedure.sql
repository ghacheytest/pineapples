SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 09 2010
-- Description:	Standard Pineapples Backup procedure
-- =============================================
CREATE PROCEDURE [dbo].[pineapplesBackup]
	-- Add the parameters for the stored procedure here
	@ForceMonthlyBackup int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @dbName sysname
	declare @currentDate datetime

	select @dbName = db_name()

	select @currentDate = getDate()			-- don;t want bizarre behaviour at midnight....

	declare @DayOfMonth int
	declare @DayOfWeek int


	declare @BackupPath nvarchar(200)
	declare @BackupDevice nvarchar(200)


	declare @BackupFileName nvarchar(200)

	-- what do we need to do based on date
	select @DayOfWeek = datepart(dw,@CurrentDate)
	select @DayOfMonth = datepart(d,@CurrentDate)

	if (@DayOfMonth = 1 or @ForceMonthlyBackup = 1) begin

		-- make the monthly backup name

		select @BackupDevice = paramText
		from sysParams
		where paramName = 'BACKUP_DEVICE_MONTH'

		select @BackupPath = paramText
		from sysParams
		where paramName = 'BACKUP_PATH_MONTH'

		if not (@BackupDevice is null)
			BACKUP DATABASE @dbName
			TO @BackupDevice

		else
			if not (@BackupPath is null) begin
				-- e.g. KEMIS200911.bak
				select @BackupFileName = db_name() + convert(nchar(6),@currentDate,112) + '.bak'

				if (right(@BackupPath,1) <> '\')
					select @BackupPath = @BackupPath + '\'

				select @BackupFileName = @BackupPath + @BackupFileName
				print 'Backup to ' + @BackupFileName
				BACKUP DATABASE @dbName
					TO DISK = @BackupFileName
			end
			else
				print 'No device or path specified for monthly backup. ' +
					' Set the device or path in sysParams BACKUP_DEVICE_MONTH or BACKUP_PATH_MONTH ' +
					' using Pineapples System Configuration.'


	end


-- daily backup now

	select @BackupDevice = paramText
	from sysParams
	where paramName = 'BACKUP_DEVICE_DAILY'

	select @BackupPath = paramText
	from sysParams
	where paramName = 'BACKUP_PATH_DAILY'

	if not (@BackupDevice is null) begin
			if ( @DayOfMonth = 1 or @ForceMonthlyBackup = 1)
				BACKUP DATABASE @dbName
				TO @BackupDevice
			else
				BACKUP DATABASE @dbName
				TO @BackupDevice
				WITH DIFFERENTIAL

		end
	else begin
		if not (@BackupPath is null) begin
			-- find the file path for the most recent monday
			select @BackupFileName = db_name() + convert(nchar(8),@currentDate - @DayOfWeek + 1,112) + '.bak'

			if (right(@BackupPath,1) <> '\')
				select @BackupPath = @BackupPath + '\'

			select @BackupFileName = @BackupPath + @BackupFileName
			print 'Backup to ' + @BackupFileName
			if (@DayOfWeek = 1 or @DayOfMonth = 1 or @ForceMonthlyBackup = 1)
				-- do another full backup into the weekly backup file
				-- otherwise the differential will relate to themonthly backup, not the start of this file

				BACKUP DATABASE @dbName
					TO DISK = @BackupFileName
			else
				BACKUP DATABASE @dbName
					TO DISK = @BackupFileName
					WITH DIFFERENTIAL


		end
		else
				print 'No device or path specified for daily backup. ' +
					' Set the device or path in sysParams BACKUP_DEVICE_DAILY or BACKUP_PATH_DAILY ' +
					' using Pineapples System Configuration.'
	end


END
GO
GRANT EXECUTE ON [dbo].[pineapplesBackup] TO [pAdminOps] AS [dbo]
GO

