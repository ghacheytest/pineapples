SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 11 2007
-- Description:	Data for analysis of STA exams
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVSTAAnalysisData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- set up the estimate temp table
SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

    -- Insert statements for procedure here
-- the sta data

Select STA.*,
EBSE.surveydimensionssID

FROM ExamStandardTest STA
	INNER JOIN Exams EX
		ON STA.exID = Ex.exID
	LEFT JOIN #EBSE EBSE
		ON STA.schNo = EBSE.schNo and EX.exYear = EBSE.LifeYear
DROP TABLE #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVSTAAnalysisData] TO [pSchoolRead] AS [dbo]
GO

