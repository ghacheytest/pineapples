SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 3 2010
-- Description:	Totals by work item type, for work Order Analysi (Scope of works)
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[WorkTypeTotals]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT
	schNo
	, schName
	, iCode
	, schType
	, schAuth
	, witmType
	, sum(witmQty) as Qty
	, sum(witmEstCost) as EstCost
	, sum(witmContractValue) as ContractValue
	, sum(case witmProgress when 'ID' then 1 else 0 end) InProgress

FROM
	WotkItem WI
	INNER JOIN Schools
		ON WI.schNo = Schools.schNo
END
GO

