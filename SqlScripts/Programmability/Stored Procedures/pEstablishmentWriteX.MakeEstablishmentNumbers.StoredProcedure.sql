SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-11
-- Description:
-- =============================================
CREATE PROCEDURE [pEstablishmentWriteX].[MakeEstablishmentNumbers]
(
	-- Add the parameters for the function here
	@SchoolNo nvarchar(50)
	, @numPositions int = 1
)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @PositionNumbers TABLE
	( posNo nvarchar(50)
	, counter int)

	DECLARE @Result nvarchar(50)

	declare @Prefix nvarchar(20)
	declare @seq int
	declare @MaxNo nvarchar(20)
	declare @NumDigits int		-- number of digits to format the number

	declare @PositionNoFormat int

	Select @Prefix = isnull(common.sysParam('ESTAB_PREFIX'),'272-')

	select @PositionNoFormat = isnull(common.sysParam('ESTAB_FORMAT'),0)
	Select @NumDigits = isnull(common.sysParam('ESTAB_DIGITS'),
								case when @PositionNoFormat = 1 then 3
									else 5 end
								)


If (@PositionNoFormat = 1)  begin
	-- format uses school no
	-- if the school number is null we are making a supernumerary

	if @SchoolNo is null begin
			select @MaxNo = isnull(max(estpNo),'00000000000000000000') from Establishment WHERE SchNo is null
			Select @NumDigits = isnull(common.sysParam('ESTAB_SUPERDIGITS'),@NumDigits)
			Select @Prefix = isnull(common.sysParam('ESTAB_SUPERPREFIX'),@Prefix)

			Select @seq = cast(right(@MaxNo,@NumDigits) as int)

			-- Add the T-SQL statements to compute the return value here
			INSERT INTO @PositionNumbers
			SELECT
				@Prefix +  replace(str(@seq+num,@NumDigits),' ','0')
				, num
			FROM metaNumbers
			WHERE num between 1 and @NumPositions

		end
	else begin
		select @MaxNo = isnull(max(estpNo),'00000000000000000000') from Establishment WHERE SchNo = @SchoolNo


		Select @seq = cast(right(@MaxNo,@NumDigits) as int)


		-- Add the T-SQL statements to compute the return value here
		INSERT INTO @PositionNumbers
		SELECT pos
		, recNo
		FROM
		(
			SELECT pos
			, row_number() OVER(ORDER BY pos) recNo
			FROM
			(
				SELECT
					@Prefix + @SchoolNo + '' + replace(str(@seq+num,@NumDigits),' ','0') pos

				FROM metaNumbers
				WHERE num between 1 and (999 - @Seq)
			) LIST2
			WHERE pos not in (Select estpNo from Establishment)
		)  LIST
		WHERE recNo between 1 and @numPositions

	end
	-- Return the result of the function
end

If (@PositionNoFormat = 0)  begin
	-- format doesn't use school no
	-- if the school number is null we are making a supernu
	select @MaxNo = isnull(max(estpNo),'00000000000000000000') from Establishment


	exec @seq = dbo.getCounter 'Establishment',@NumPositions,1

	-- Add the T-SQL statements to compute the return value here
	INSERT INTO @PositionNumbers
	SELECT
		@Prefix +  replace(str(@seq+num,@NumDigits),' ','0')
		, num
	FROM metaNumbers
	WHERE num between 1 and @NumPositions
	-- Return the result of the function
end

	Select * from @PositionNumbers

END
GO

