SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 10 2009
-- Description:	save a workflow action
-- =============================================
CREATE PROCEDURE [workflow].[WriteAction]
	-- Add the parameters for the stored procedure here
	@porID  int
	, @actionID int
	, @WorkflowActionID int
	, @Outcome nvarchar(50)
	, @Note ntext = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	begin try

		begin transaction

		INSERT INTO PORActions
				( porID
					, pactDate
					, pactUser
					, actID
					, pactOutcome
					, pactNote
				)
		VALUES (@porID
					, getDate()
					, user_name()
					, @actionID
					, @Outcome
					, @Note
				)


		UPDATE POR
		Set porStatus = FWO.wfoStatus
		FROM workflow.FlowOutcomes FWO
		WHERE
			FWO.wfqID =@WorkflowActionID
			AND FWO.wfoOutcome = @Outcome
			AND POR.porID = @PORID


	end try

	begin catch

		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch
-- and commit
if @@trancount > 0
	commit transaction

return

END
GO

