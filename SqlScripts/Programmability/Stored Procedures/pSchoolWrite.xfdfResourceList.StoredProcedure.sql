SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pSchoolWrite].[xfdfResourceList]
	@surveyID int,
	@grid xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid, @xmlns

-- the grid expected sits under the base resource node - ie the name is a tag for the resource category
-- the full name of the resource category
-- get the reource category
-- this sits in the 'Cat' child under the base node
-- <field name="cat"><value>Communication</value></field>
-- in the field codes, this look like e.g. <field name="Resource"><field name="Comm"><field name="Cat"><value>Communications</value>....
	declare @resourceCategory nvarchar(100)
	declare @ResourceTag nvarchar(100)
	Select @resourceCategory = resourceCategory
			from OPENXML (@idoc, '/x:field',2)
			WITH (
				resourceCategory nvarchar(100) 'x:field[@name="Cat"]/x:value'
				)
print @resourceCAtegory

Select @ResourceTag = 'Resource:' + @ResourceCategory			-- for audit logging
-- get all the possible rows
	-- this is the set of row tags for the grid
	declare @rowSet table(

	r nvarchar(2)
	, rk nvarchar(100)
	, rv nvarchar(100)
	)

	INSERT INTO @rowSet
		select r, isnull(rk, rv), rv
		FROM OPENXML(@idoc ,'/x:field/x:field[@name="R"]/x:field',2)
		WITH
		(
			r			nvarchar(2)			'@name'
			, rk			nvarchar(100)	'x:field[@name="K"]/x:value'		-- there are no Ks actually...
			, rv			nvarchar(100)	'x:field[@name="V"]/x:value'
		)
		WHERE r <> 'T'			-- ignore any totals


-- populate the table of grid values
-- this table has no column split - instead it has an array of values under the row field node
declare @data TABLE
(
	r			nvarchar(2),
	a			int,					-- -1 or 0
	adq			int,					-- -1 or 0
	n			int,					--	'field[@name="Num"]/value',		-- number
	q			int,					--	'field[@name="Qty"]/value',
	l			nvarchar(10),			--	'field[@name="Level"]/value',	-- resource level for books?
	roomID		int,					--	'../@roomID',				--?? to do....
	gfp			nvarchar(1),				--  'field[@name="C"]/value'
	c			int,
	ok			int				--  'field[@name="OK"]/value'	-- functioning Y/N in Somalia
)

	INSERT INTO @data
	Select
	r,
	case aYN when 'Y' then -1 when 'N' then 0 else null end,
	case adqYN when 'Y' then -1 when 'N' then 0 else null end,
	n,
	q,
	l,
	roomID,
	c,
	case c when 'G' then 1 when 'F' then 2 when 'P' then 3 else null end,
	case ok when 'Y' then -1 when 'N' then 0 else null end
	from OPENXML (@idoc, '/x:field/x:field[@name="D"]/x:field',2)
		WITH (
				r			nvarchar(2)				'@name',
				aYN			nvarchar(1)				'x:field[@name="A"]/x:value',		-- Y or N
				adqYN		nvarchar(1)				'x:field[@name="AQ"]/x:value',	-- Adequate Y or N
				n			int						'x:field[@name="Num"]/x:value',		-- number
				q			int						'x:field[@name="Qty"]/x:value',
				l			nvarchar(10)			'x:field[@name="Level"]/x:value',	-- resource level for books?
				roomID		int						'../@roomID',				--?? to do....
				c			nvarchar(1)				'x:field[@name="C"]/x:value',
				ok			nvarchar(1)				'x:field[@name="OK"]/x:value'	-- functioning Y/N in Somalia
				--Materials nvarchar(1) '@material',
				--Location nvarchar(50) '../../@location',
				--Note nvarchar(100) '@note'
				)
Select * from @data

begin transaction

begin try

	DELETE from Resources
	WHERE ssID = @SurveyID
		AND resName = @ResourceCategory
		and resSplit in (Select rk from @rowset)
		and (resLevel is null or  resLevel in (Select l from @data))
		--and (resLocation is null or resLocation in (select Location from @listtbl))

	exec audit.xfdfInsert @SurveyID, 'Records deleted',@ResourceTag,@@rowcount


	Insert into Resources
			(ssID,
			resName,
			resLevel,
			rmID,
			resSplit,
			resAvail,
			resAdequate,
			resNumber,
			resQty,
			resCondition,
			resFunctioning,
			resMaterial,
			resLocation,
			resNote)
	Select @SurveyID, @ResourceCategory,
		l, roomID, R.rk,
			-- not always prompted -e.g. Water supply so have to provide
		isnull(a
			, case when n > 0 then -1 end)
		-- hsitorically adequate is always 0 if it is not applicable
		, isnull(adq,0)
		, n, q,
		c, ok,
		null, null, null		-- Materials, Location, Note
	from @data D
		INNER JOIN @rowSet R
			ON D.r = R.r
	WHERE a <> 0 or
			adq <> 0 or
			n <> 0 or
			(c is not null)
	exec audit.xfdfInsert @SurveyID, 'Records inserted',@ResourceTag,@@rowcount
	-- 14 03 2010
	----declare @BuildingType nvarchar(10)

	----Select @BuildingType = mresCatBuildingType
	----FROM metaResourceCategories
	----WHERE mresCAtCode = @ResourceCategory

	----If (@BuildingType is not null) begin
	----	-- We won;t do this if there is a later Survey mapped building inspection
	----	declare @SurveyYear int
	----	select @SurveyYear = svyYEar
	----	from schoolSurvey
	----	WHERE ssID = @SurveyID

	----	declare @LastInspectionYear int

	----	Select @LastInspectionYEar = max(SIS.svyYear)
	----	from SchoolInspection SI
	----		INNER JOIN SurveyInspectionSet SIS
	----			ON SI.inspSetID = SIS.inspsetID
	----		INNER JOIN SchoolSurvey SS
	----			ON SIS.svyYear = SS.svyYear
	----			AND SI.schNo = SS.schNo
	----		WHERE SS.ssID = @SurveyID
	----			AND SIS.intyCode = 'BLDG'
	----	print @LastInspectionYear

	----	If (@LastInspectionYear is null or @LastInspectionYear <= @SurveyYear )
	----		exec pSurveyWrite.applyTeacherHouseResourceToBuildings  @SurveyID, @ResourceCategory
	----end


end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end
	exec audit.xfdfError @SurveyID, @ErrorMessage,@ResourceTag
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

return


END
GO

