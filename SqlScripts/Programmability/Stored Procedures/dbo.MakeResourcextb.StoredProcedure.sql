SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[MakeResourcextb]
	-- Add the parameters for the stored procedure here
(
@ResourceCategory nvarchar(50)
)
AS
BEGIN

	DECLARE @lists varchar(2000)
	DECLARE @listfields varchar(2000)

	declare @defs table
	(
		defcode nvarchar(50),
		defName nvarchar(50)
	)

	INSERT INTO @defs
	SELECT mresCode, mresName
	FROM TRMetaResourceDefs
	WHERE mresCat = @resourceCategory


	SET @lists = ''
	set @listfields = ''
	--// Get a list of the pivot columns that are important to you.

	SELECT @lists = @lists + '[' + convert(varchar(50),defName,101) + '],'
	FROM (SELECT Distinct defName FROM @defs) l
	SELECT @listfields = @listfields + '[' + convert(varchar(50),defName,101) + '] nvarchar(50),'
	FROM (SELECT Distinct defName FROM @defs) l

	--// Remove the trailing comma

	SET @lists = LEFT(@lists, LEN(@lists) - 1)
	set @listfields = LEFT(@listfields, LEN(@listfields) - 1)
	--// Now execute the Select with the PIVOT and dynamically add the list
	--// of dates for the columns
	declare @cmd nvarchar(2000)

	set @cmd = 'SELECT schNo, ' + @lists +
		' FROM Resources PIVOT (MAX(resAvail) FOR resSplit IN (' + @lists + ')) p
		WHERE resName = ''' + @ResourceCategory + ''''


	set @cmd=  'create VIEW #resourcesX AS ' + @cmd

	print @cmd

	exec (@cmd)
END
GO

