SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brian Lewis
-- Create date: 01 02 2015
-- Description:    utility function to fake an identity insert - to force the value returned by @@identity
-- =============================================
CREATE PROCEDURE [common].[forgeIdentity]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @sql nvarchar(500)
	if @id is null
		return

	set @sql='select identity (int, ' + cast(@id as varchar(10)) + ',1) as id into #tmp;drop TABLE #tmp'
	execute (@sql)

END
GO

