SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 2 2017
-- Description:	build warehouse classrom
-- =============================================
CREATE PROCEDURE [warehouse].[buildRoomCounts]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET NOCOUNT ON;
	IF OBJECT_ID('warehouse.roomCounts', 'U') IS NOT NULL
		DROP TABLE warehouse.roomCounts
    -- Insert statements for procedure here

Select E.schNo
, E.LifeYear SurveyYear
, E.rmType
, numRooms
, avgSize
, NewestRoom
, rmMaterials
, E.Estimate
INTO warehouse.roomCounts
 from ESTIMATE_BestSurveyAnyRooms E
 INNER JOIN
 (
	Select rmType, rmMaterials, ssID, count(*) NumRooms
	, avg(rmSize) AvgSize
	, max(rmYear) NewestRoom
	from Rooms
	GROUP BY rmType, rmMaterials, ssID
) T
	ON E.bestssID = T.ssID
	AND E.rmType = t.rmType

END
GO

