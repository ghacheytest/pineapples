SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 10 2009
-- Description:	Check payslips to confirm PORS
-- =============================================
CREATE PROCEDURE [workflow].[checkPayslipsforConfirmation]
	-- Add the parameters for the stored procedure here
	@PORID as int = null
	, @PayslipDate as datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
if (@PayslipDate is null)
	Select @PayslipDate = max(tpsPeriodEnd)
	from
		TeacherPayslips
-- what status do they go to?


select POR.porID
	, FlowName
	, TI.tPayroll
	, TA.estpNo
	, TA.taDate
	, TA.spCode
	, TPS.tpsID
	, case when (POR.porestpNo = TPS.tpsPosition)
				and TI.tPayroll = TPS.tpsPayroll
				and TA.spCode = TPS.TPSSalaryPoint then 1
			else 0
	 end Confirmed
INTO #Confirmed

FROM
-- these are the PORIDs
		-- for whcih we can get an appointment
		-- and check the details against a payslip for matching tpayroll, PositionNo and SalaryPoint
	POR
	INNER JOIN TeacherAppointment TA
		on POR.porAdjStart = TA.taDate
			and POR.tID = TA.tID
			and POR.porestpNo = TA.estpNo
	INNER JOIN workflow.Flows F
		ON F.flowID = POR.flowID
	INNER JOIN TeacherIdentity TI
		ON POR.tID = TI.tID


	LEFT JOIN (Select *
				from TeacherPayslips
				WHERE tpsPeriodEnd = @PayslipDate
				) TPS

	ON TI.tPayroll = TPS.tpsPayroll

	WHERE POR.porStatus = 'PSLIPCONF'

	begin try
		begin transaction

			INSERT INTO PORActions
				( porID
				  , actID
				  , pactOutcome
				)
			SELECT
				POR.porID
				, FA.actID
				, FO.wfoOutCome
			FROM
				POR
				INNER JOIN 	#confirmed CONFIRMED
					on CONFIRMED.porID = POR.porID
				INNER JOIN workflow.FlowActions FA
					on FA.flowID = POR.flowID
					and FA.wfqStatus = POR.porStatus
				INNER JOIN workflow.FlowOutcomes FO
				on FO.wfqID = FA.wfqID

				WHERE CONFIRMED.Confirmed = 1

			UPDATE POR
				set POR.porStatus = FO.wfoStatus

			FROM
				POR
				INNER JOIN 	#confirmed CONFIRMED
					on CONFIRMED.porID = POR.porID
				INNER JOIN workflow.FlowActions FA
					on FA.flowID = POR.flowID
					and FA.wfqStatus = POR.porStatus
				INNER JOIN workflow.FlowOutcomes FO
				on FO.wfqID = FA.wfqID

				WHERE CONFIRMED.Confirmed = 1
		commit transaction

		select
			*
			, TPS.tpsPosition
			, TPS.tpsSalaryPoint
			, case when TPS.tpsID is null then 0 else 1 end HasPayslip
			, case when estpNo = TPS.tpsPosition then 1 else 0 end PositionMatch
			, case when spCode = TPS.tpsSalaryPoint then 1 else 0 end SalaryPointMatch
		from #Confirmed
			LEFT JOIN TeacherPayslips TPS
			ON #Confirmed.tpsID = TPS.tpsID


		Select
			count(PORID) as TotalToConfirm
			, sum(Confirmed) as TotalConfirmed
			, sum(case when Confirmed = 0 then 1 else 0 end) TotalUnconfirmed
			, sum(case when tpsID is null then 0 else 1 end) TotalPayslips
		from #Confirmed

	end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch
----

END
GO

