SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 4 2010
-- Description:	the output recordset is a script that can be sent from the reference system to the client. It updates all table permissions, and checks the column count of each table as well.
-- =============================================
CREATE PROCEDURE [dbo].[makePineapplesPermissions]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Select 'IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''' + TABLE_SCHEMA + '.'+ TABLE_NAME + ' '') AND type in (N''U'', N''V'')) '
	+ N'GRANT ' + privilege_type + N' ON ' + TABLE_SCHEMA + N'.['+ TABLE_NAME + N'] TO ' + GRANTEE
	+ ' ELSE print ''' + TABLE_SCHEMA + '.['+ TABLE_NAME + '] missing''' + char(13) + char(10) +'GO'
	from INFORMATION_SCHEMA.TABLE_PRIVILEGES

UNION

Select 'IF not EXISTS (Select count(COLUMN_NAME) from INFORMATION_SCHEMA.COLUMNS ' +
	' WHERE TABLE_SCHEMA=''' + TABLE_SCHEMA + '''' +
	' and TABLE_NAME=''' + TABLE_NAME + '''' +
	' HAVING count(COLUMN_NAME) =' + CAST(count(COLUMN_NAME) as nvarchar(3)) + ')' +
	' PRINT ''' + TABLE_SCHEMA + '.' + TABLE_NAME + ''' ' + char(13) + char(10) + 'GO'
	FROM INFORMATION_SCHEMA.COLUMNS
	GROUP BY TABLE_SCHEMA, TABLE_NAME

END
GO

