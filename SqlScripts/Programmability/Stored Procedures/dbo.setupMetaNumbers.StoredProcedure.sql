SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 12 2007
-- Description:	Build metanumbers
-- =============================================
CREATE PROCEDURE [dbo].[setupMetaNumbers]
AS
BEGIN
SET NOCOUNT ON;

declare @i int
select @i = -100

DROP TABLE  metaNumbers

SELECT TOP 110001
	IDENTITY(INT,-10000,1) AS Num
	INTO MetaNumbers

  FROM master.sys.all_columns ac1
  CROSS JOIN master.sys.all_columns ac2

   ALTER TABLE metaNumbers
   ADD CONSTRAINT PK_metaNumbers_N
        PRIMARY KEY CLUSTERED (num) WITH FILLFACTOR = 100

  GRANT SELECT ON metaNumbers TO PUBLIC


END
GO

