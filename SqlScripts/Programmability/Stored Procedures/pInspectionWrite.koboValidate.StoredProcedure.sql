SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 12 2017
-- Description:	first attempt at working with kobo downloaded data
-- =============================================
CREATE PROCEDURE [pInspectionWrite].[koboValidate]
	-- Add the parameters for the stored procedure here

	@xml xml
	, @filereference uniqueidentifier
	, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- this is the type of Kobo form implemented by this stored proc
-- it must match a description in the table lkpInspectionTypes


declare @rows TABLE
(
 	rowIndex int
	, [start] datetime
	, [end] datetime
	, inspectionType nvarchar(20)
	, schNo nvarchar(50)
	, rowXml xml
	, filereference uniqueidentifier null
)

-- represent the xml file - column names are 'sanitized'
INSERT INTO @rows
SELECT
v.value('@Index', 'int') [ RowIndex]
, nullif(ltrim(v.value('@start', 'nvarchar(19)')),'')
, nullif(ltrim(v.value('@end', 'nvarchar(19)')),'')
, nullif(ltrim(v.value('@inspectionType', 'nvarchar(50)')),'')
, nullif(ltrim(v.value('@schoolNo', 'nvarchar(50)')),'')
, v.query('.')

-- file reference is the id of the input file
, @fileReference


from @xml.nodes('ListObject/row') as V(v)

-- report any rows ehre the inspectionType is missing or invalid, school is missing or invalid
Select rowIndex
, inspectionType
, R.schNo
, case when inspectionType is null then 'Missing inspection type code'
	when intyCode is null then 'Invalid inspection type code'
end inspStatus
, case when R.schNo is null then 'Missing school no'
	when S.schNo is null then 'Invalid school no'
end schStatus

from @rows R
LEFT JOIN lkpInspectionTypes I
ON R.inspectionType = I.intyCode
LEFT JOIN Schools S
	ON R.schNo = S.schNo
WHERE intyCode is null or S.schNo is null

END
GO

