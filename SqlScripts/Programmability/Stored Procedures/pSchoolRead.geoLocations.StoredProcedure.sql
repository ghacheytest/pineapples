SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pSchoolRead].[geoLocations]
	-- Add the parameters for the stored procedure here
	@mode nvarchar(10) = 'POINT'
	, @xmlFilter xml = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @rows TABLE
(
	schNo nvarchar(50) PRIMARY KEY NOT NULL
	, recNo int
)

declare @IDs TABLE
(
	schNo nvarchar(50)
	, Lat float
	, Lng float
	, SchoolType nvarchar(10)
	, Name nvarchar(200)
	UNIQUE NONCLUSTERED (schNo)
)

declare @numMatches int

INSERT INTO @rows
EXEC pSchoolRead.SchoolFilterIDs
	@NumMatches = @numMatches
	, @xmlFilter = @xmlFilter
	, @PageNo = 1
	, @PageSize = 0

	INSERT INTO @IDS
	Select Schools.schNo
	, schLat
	, schLong
	, schType
	, schName

	FROM Schools
		INNER JOIN @rows R
			ON schools.schNo = R.schNo


    -- Now we have the sIDs
    -- find how many points are colocated - these require special handling

    declare @Colocs TABLE
    (
		Lat float
		, Lng float
		, colocCount int
		PRIMARY KEY (Lat, Lng)
	)

	INSERT INTO @colocs
	SELECT Lat, Lng, count(*)
	FROM @IDS
	WHERE Lat is not null and Lng is not null
	GROUP BY Lat, Lng


    if @mode = 'POINT' begin
		Select IDS.*
		, C.colocCount coloc
		FROM @IDS IDs
			LEFT JOIN @Colocs C
				ON IDS.Lat = C.Lat
					AND IDS.Lng = C.lng

		WHERE IDs.Lat is not null and IDs.Lng is not null
    end


    -- now get the bounding box of all the points
    Select isnull(max(Lat),40) n
    , isnull(min(Lat),-40) s
    , isnull(max(Lng),30) e
    , isnull(min(Lng),-30) w
    From @IDS

    -- this is the set of zooms
    -- include islands, districts and selected schools
    Select null code
    , '(all markers)' name
    , isnull(max(Lat),40) n
    , isnull(min(Lat),-40) s
    , isnull(max(Lng),30) e
    , isnull(min(Lng),-30) w
    , null lat
    , null lng
    , null zoom
    From @IDS
    UNION
    Select schNo code, Name name
    , null n
    , null s
    , null e
    , null w
	,  lat
	,  lng
	, 13	zoom
	From @IDs
	UNION
	Select
	I.iCode code
	, I.iName Name
	, max(lat) n
	, min(lat) s
	, max(lng) e
	, min(lng) w
	, case when max(lat) = min(lat) then max(lat) else null end
	, case when max(lng) = min(lng) then max(lng) else null end
	, 13		zoom
	from Schools S
		INNER JOIN Islands I
		ON S.iCode = I.iCode
		INNER JOIN @IDs IDS
			ON S.schNo = IDS.schNo
	GROUP BY I.iCode, I.iName

	UNION
	Select
	D.dID code
	, D.dName Name
	, max(lat) n
	, min(lat) s
	, max(lng) e
	, min(lng) w
	, case when max(lat) = min(lat) then max(lat) else null end
	, case when max(lng) = min(lng) then max(lng) else null end
	, 13		zoom
	from Schools S
		INNER JOIN Islands I
		ON S.iCode = I.iCode
		INNER JOIN Districts D
			ON I.iGroup = D.dID
		INNER JOIN @IDs IDS
			ON S.schNo = IDS.schNo
	GROUP BY D.dID, D.dName


 --   -- now get teh list of different countries...
 --   SELECT CC.couCode
 --   , CC.couName
 --   , n, s, e, w
 --   FROM
 --   ( SELECT DISTINCT couCode
	--	, couName
	--	FROM lkpCountries C
	--		INNER JOIN Applicants_ A
	--			ON C.couCode = A.sAddressCountry
	--	WHERE A.sID in (Select sID from @IDs WHERE sID is not null)
	--) CC
	--INNER JOIN mapping.CountryGeometry G
	--	ON CC.couCode = G.iso_a2
	--CROSS APPLY mapping.polygonNSEW(G.Geom, null	)


	--ORDER BY couName

END
GO

