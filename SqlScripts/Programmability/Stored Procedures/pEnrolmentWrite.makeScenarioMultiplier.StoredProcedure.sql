SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 1 2010
-- Description:	Generate an enrolment projection, using the multiplier method
-- =============================================
CREATE PROCEDURE [pEnrolmentWrite].[makeScenarioMultiplier]
	-- Add the parameters for the stored procedure here
	@ScenarioID	int	= null	-- if replacing an exitisng scenario
	,@ScenarioName nvarchar(50)	= null	-- if updating, or creating new
	,@StartYear int
	,@EndYear int = null
	,@BaseYear int
	,@SizeFactor float =1
	,@SchoolNo nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try
	-- transaction control

	begin transaction
-- first , do we need to create a Scenario?

	if @EndYEar is null
		Select @EndYear = @StartYear

	declare @Params as XML

	Select @Params =
	(
	Select
		'Multiplier' [@tool]
		,@StartYear [@startYear]
		, @EndYear [@endYear]
		, @BaseYear [Params/@baseYear]
		, @SizeFactor [Params/@sizeFactor]
	FOR XML PATH('Projection')
	)

		-- are we operating on an existing scenario?

	If @ScenarioID is null begin
		INSERT INTO EnrolmentScenario
		(escnName
		, escnSource
		, escnSplit
		, escnLevelSplit
		, escnYearStart
		, escnYearEnd
		, escnParams
		)
		Select @ScenarioName
		, 'Multiplier'
		, 'S'
		, 'L'		-- has a level split
		, @StartYear
		, @EndYear
		, @Params

		Select @ScenarioID = @@IDENTITY
		end
	else begin
		-- just update the parameters and sour
		UPDATE EnrolmentScenario
		SET escnSource = 'Multiplier'
			, escnSplit = 'S'
			, escnLevelSplit = 'L'
			, escnYearStart = @StartYear
			, escnYearEnd = @EndYear
			, escnParams = @Params
		WHERE escnID = @ScenarioID
	end

	CREATE TABLE #schools
	(
		schNo nvarchar(50)
		, schClosed int
	)

-- get a master list of all the schools we are going to operate on
-- this may be expanded if more selection criteria are added later

	INSERT INTO #schools
	Select schNo, schClosed
	From Schools
	WHERE (schNo = @SchoolNo or @SchoolNo is null)
		AND (schClosed = 0 or @StartYear < schClosed)
	-- AND .....


	Select #schools.schNo
	, LifeYear
	, enLevel
	, T.bestEnrol
	, schClosed
	, sum(enSum) LevelEnrol
	INTO #base
	from dbo.tfnEstimate_BestSurveyEnrolments() T
	INNER JOIN Enrollments E
		ON T.bestssId = E.ssID
	INNER JOIN #schools
		ON #schools.schNo = T.schNo
	WHERE LifeYear = @BaseYear
	Group By #schools.schNo, LifeYear, T.bestEnrol, enLevel, schCLosed


	-- next stpe is to create the Enrolment Projection records.
	-- there is one of these for each school

	-- remove any that are not required, unless they are frozen

-- to get the right set of EnrolmentProjections, we delete any not required,
--  and any any that are, that are not present


	DELETE 	from EnrolmentProjection
	from EnrolmentProjection
		INNER JOIN #Schools
			ON EnrolmentProjection.schNo = #schools.schNo	-- in the set of schools we are working with
		LEFT JOIN #base
			ON EnrolmentProjection.schNo = #base.schNo
	WHERE
		--#base.schNo is null	-- not required becuase no enrolment in base year
		 epYear between @StartYEar and @EndYEar		-- limit to the range of years
		AND
			escnID = @ScenarioID
		AND epLocked = 0

-- remove all the data records that are not locked
	DELETE FROM EnrolmentProjectionData
	FROM EnrolmentProjectionData EPD
	INNER JOIN EnrolmentProjection EP
		ON EP.epID = EPD.epID
	WHERE EP.epLocked = 0
	AND EP.escnID = @ScenarioID


	INSERT INTO EnrolmentProjection
	(escnID, epYear, schNo, epAggregate)
	Select DISTINCT
		@scenarioID
		, num + @StartYear
		, #base.schNo
		, round(#base.bestEnrol * power(@sizeFactor, (num + 1)),0)-- fix on roungin 6 3 2010
	from #base
	INNER JOIN metaNumbers
		ON (#base.schClosed = 0  or metaNumbers.num < schClosed - @StartYear)
	LEFT JOIN	-- these records are only there if they are locked
	 (Select schNo, epYear from EnrolmentProjection
		WHERE epYear between @StartYEar and @EndYear
			 AND escnID = @ScenarioID ) Locks

		ON #base.schNo = Locks.schNo
			AND Locks.epYear = (metaNumbers.num + @StartYear)
	WHERE
		metaNumbers.num between 0 and @EndYear - @StartYear
		AND locks.schNo is null

print @@ROWCOUNT
	-- now we get to write the projectionData

CREATE TABLE #tmpEPD
(
	epdID	int IDENTITY not null
	, epID int
	, epdLevel	 nvarchar(10)
	, epdM		int			-- not used, but we are mirroring the real table
	, epdF		int
	, epdU		int
	, RawValue	 float
	, RoundError float
)


	-- add them in, ignore locked
	INSERT INTO #tmpEPD
	(epID, epdLevel, epdU, RawValue, RoundError)
	Select epID
		, enLevel
		, round(levelEnrol * power(@sizeFactor, (epYear - @StartYear + 1)),0) -- power causes the growth to compund for each year
		, levelEnrol * power(@sizeFactor, (epYear - @StartYear + 1))
		, round(levelEnrol * power(@sizeFactor, (epYear - @StartYear + 1)),0) -
			levelEnrol * power(@sizeFactor, (epYear - @StartYear + 1))
	from EnrolmentProjection EP
	INNER JOIN #base
		ON #base.schNo = EP.schNo			--#base already selects for our affected schools
	WHERE EP.escnID = @ScenarioID
		AND epYear between @StartYear and @EndYEar
		AND epLocked = 0
		AND #base.LifeYear = @BaseYear		-- whoch it always is, for now


-- a btter strategy is:
------ if the adjustment needs to come down, we have to take one or more values
------ down. if adjustment = -n , we find the n values that got rounded up the most
------ and knock them down 1 each

------ if adjustment = n (has to come up n) we find the n values that got rounded down the most
------ and bump them up 1 each


		-- this logic is to ensure that rounding errors do not change the best approximation to the total
		-- ie if after rounding the total is not the same as the rounding of the aggreagte, we adjust
		Select EP.schNo, EP.epYear
			, EP.epID
			, EP.epAggregate
			, sum(epdU) LevelTotals,EP.epAggregate - sum(epdU) Adjustment
		INTO #toAdjust
		FROM EnrolmentProjection EP
		INNER JOIN  #tmpEPD EPD
			ON EPD.epID = EP.epID
		INNER JOIN #schools
			ON EP.schNo = #schools.schNo
		WHERE  EP.escnID = @ScenarioID
			AND EP.epLocked = 0
		GRoup BY EP.schNo, EP.epYear, EP.epID, EP.epAggregate
		HAVING sum(epdU) <> EP.epAggregate


		UPDATE #tmpEPD
		SET epdU = epdU + case when Adjustment > 0 then 1 else - 1  end
		FROM
		#tmpEPD
			INNER JOIN #toAdjust
				ON #tmpEPD.epID = #toAdjust.epID
		INNER JOIN
		(Select  epdID
			, Row_number() OVER(PARTITION BY epID ORDER BY RoundError DESC) PosRoundError
			, Row_number() OVER(PARTITION BY epID ORDER BY RoundError ) NegRoundError

		from #tmpEPD
		) roundErrorRank
		ON #tmpEPD.epdID = roundErrorRank.epdID
		WHERE
			(
				-- so if we are making a positive adjustment, we adjust the top (Adjustment) rows ranked by NegRoundError
				-- ie those with the biggset amount of rounding down
				(Adjustment > 0 and NegRoundError <= Adjustment)
			OR
				-- if we are makeing a negative adjustment, we adjust those with the biggest
				(Adjustment < 0 and PosRoundError <= (Adjustment * -1))
			)


	-- loving myself sick


	-------------- pick the biggest value and adjust it
	------------	UPDATE #tmpEPD
	------------	SET epdU = epdU + Adjustment
	------------	FROM
	------------	#tmpEPD
	------------		INNER JOIN #toAdjust
	------------			ON #tmpEPD.epID = #toAdjust.epID
	------------	INNER JOIN
	------------	(Select  epdID, Row_number() OVER(PARTITION BY epID ORDER BY epdU DESC) Big
	------------	from #tmpEPD
	------------	) Biggest
	------------	ON #tmpEPD.epdID = Biggest.epdID
	------------	AND Biggest.Big = 1


-- add them in, ignore locked
	INSERT INTO EnrolmentProjectionData
	(epID, epdLevel, epdU)
	Select epID
		, epdLevel
		, epdU
	FROM #tmpEPD


	commit transaction

--- OUTPUTS -------------------------------------------------
	-- recordset returns some stats about the scenario

	Select ES.escnID, escnName
		, escnSource
		, escnSplit
		, escnLevelSplit
		, escnYearStart
		, escnYearEnd
		, count(DISTINCT EP.schNo) NumSchools
		, sum(case when epYEar = escnYearEnd then epdSum else null end) enrolTot		-- last year
	FROM EnrolmentScenario ES
		INNER JOIN EnrolmentProjection EP
			ON ES.escnID = EP.escnID
		INNER JOIN EnrolmentProjectionData EPD
			ON EP.epID = EPD.epID
	WHERE ES.escnID = @ScenarioID
	GROUP BY ES.escnID, escnName
		, escnSource
		, escnSplit
		, escnLevelSplit
		, escnYearStart
		, escnYearEnd


end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch


END
GO

