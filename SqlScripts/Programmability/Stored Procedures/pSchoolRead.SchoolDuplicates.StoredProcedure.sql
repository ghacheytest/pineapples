SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SchoolDuplicates]
	-- Add the parameters for the stored procedure here
	@Summary int = 1,
	@ExactNameMatch int = 0,
	@SchoolTypeMatch int = 0,
	@DistrictMatch int = 0,

	@Name nvarchar(100) = null,
	@SchoolType nvarchar(10) = null,
	@District nvarchar(10) = null,

	@School1 nvarchar(50) = null,
	@School2 nvarchar(50) = null

AS
BEGIN
	/*
		return a recordset of duplicates, based on school name, school type or district
	 */
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @Summary = 1 begin

	Select  SchoolType
	, Name
	, District
	, count(Name) Matches
	FROM
	(
		SELECT
			case
				when @SchoolTypeMatch = 0 then null
				else schType
			end SchoolType,
			case
				when (@ExactNameMatch = 1) then schName
				when (patindex('% %',schName) > 3) then
						substring(schName,1,patindex('% %',schName)-1)
				else
						schName
			end  Name,
			case
				when @DistrictMatch = 0 then null
				else dID
			end  District
		FROM
			Schools
			LEFT JOIN Islands I
				on I.iCode = Schools.iCode
			LEFT JOIN Districts D
				on I.iGroup = D.dID
	) SUB


		GROUP BY Name, SchoolType, District
		HAVING Count(Name)>1


end

if @Summary = 0 begin

Select sub.*
, LSs.ssPrinsurName
, LSS.ssPrinFirstName
from
(
SELECT
	Schools.schNo
	, Schools.schType
	, Schools.schName
	, Schools.schVillage
	, I.iName
	, D.dName
	, Schools.schLang
	, Schools.schAuth
	, Min(SS.svyYear) AS MinYear
	, Max(SS.svyYear) AS MaxYear
	, Count(SS.ssID) AS NumSurveys
FROM Schools
	LEFT JOIN SchoolSurvey SS
		on SS.schNo = Schools.schNo
	LEFT JOIN Islands I
		on I.iCode = Schools.iCode
	LEFT JOIN Districts D
		on I.iGroup = D.dID
WHERE
	Schools.schNo in (@school1, @school2)
	or (@School1 is null
		and
			( case
				when (@ExactNameMatch = 1) then schName
				when (patindex('% %',schName) > 3) then
						substring(schName,1,patindex('% %',schName)-1)
				else
						schName
			end  = @Name
			)
			AND
			(SchType = @schoolType or @SchoolTypeMatch = 0)
			AND
			( dID = @District or @DistrictMatch = 0)
		)
GROUP BY
	Schools.schNo, Schools.schType, Schools.schName, Schools.schVillage
	, I.iName, D.dName, Schools.schLang, Schools.schAuth
) sub
LEFT JOIN SchoolSurvey LSS
	ON sub.schNo = LSS.schNo
	AND sub.MaxYear = LSS.svyYear
end

if @Summary = 0 begin

SELECT
	Schools.schNo
	, Schools.schType
	, Schools.schName
	, Schools.schVillage
	, I.iName
	, D.dName
	, Schools.schLang
	, Schools.schAuth
	--, Last(SchoolSurvey.ssPrinSurname) AS ssPrinSurname
	, Min(SS.svyYear) AS MinYear
	, Max(SS.svyYear) AS MaxYear
	, Count(SS.ssID) AS NumSurveys
FROM Schools
	LEFT JOIN SchoolSurvey SS
		on SS.schNo = Schools.schNo
	LEFT JOIN Islands I
		on I.iCode = Schools.iCode
	LEFT JOIN Districts D
		on I.iGroup = D.dID


WHERE
	( case
		when (@ExactNameMatch = 1) then schName
		when (patindex('% %',schName) > 3) then
				substring(schName,1,patindex('% %',schName)-1)
		else
				schName
	end  = @Name
	)
	AND
	(SchType = @schoolType or @SchoolTypeMatch = 0)
	AND
	( dID = @District or @DistrictMatch = 0)
GROUP BY
	Schools.schNo, Schools.schType, Schools.schName, Schools.schVillage
	, I.iName, D.dName, Schools.schLang, Schools.schAuth

end

END
GO

