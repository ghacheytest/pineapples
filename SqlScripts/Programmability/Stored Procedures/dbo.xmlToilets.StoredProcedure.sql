SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 02 2009
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[xmlToilets]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50) = '',
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select T.*
into #t
from Toilets T
		inner join SchoolSurvey S
			on T.ssId = S.ssID
	WHERE S.schNo = @schNo and S.svyYEar = @SurveyYEar
    -- Insert statements for procedure here

declare @xmlResult xml

set @xmlResult =
(
Select Tag
, Parent
, Category as [Toilets!1!ID!Hide]
, toiletType as [Toilet!2!type]
, toiletUse as [data!3!use]
, toiletNum as [data!3!number]
, toiletCondition as [data!3!condition]
, toiletYN as [data!3!yn]

from

(Select DISTINCT
	1 as Tag
	, null as Parent
	, 'Toilets' as Category
	, null as toiletType
	, null as toiletUse
	, null as toiletNum
	, null as toiletCondition
	, null as toiletYN
	from #t

UNION ALL
Select DISTINCT
	2 as Tag
	, 1 as Parent
	, 'Toilets' as Category
	, toiType as toiletType
	, null as toiletUse
	, null as toiletNum
	, null as toiletCondition
	, null as toiletYN
	from #t

UNION ALL
Select
	3 as Tag
	, 2 as Parent
	, 'Toilets' as Category
	, toiType as toiletType
	, toiUse as toiletUse
	, toiNum as toiletNum
	, toiCondition as toiletCondition
	, toiYN as toiletYN
	from #t

) u
ORDER BY

	[Toilets!1!ID!Hide]
,   [Toilet!2!type]
,	[data!3!use]


for xml explicit
)


select @xmlResult
drop table #t

END
GO
GRANT EXECUTE ON [dbo].[xmlToilets] TO [pSchoolRead] AS [dbo]
GO

