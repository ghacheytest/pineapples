SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1/12/2015
-- Description:
-- =============================================
CREATE PROCEDURE [pPARead].[PerfAssessTable]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 1,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

	@PerfAssessID int = null,
	@TeacherID int = null,
	@ConductedBy nvarchar(50) = null,
	@SchoolNo nvarchar(50) = null,
	@District nvarchar(10) = null,
	@Island nvarchar(10) = null,

	@StartDate datetime = null,
	@EndDate datetime = null,


	@subScoreCode nvarchar(10) = null,
	@subScoreMin float = null,
	@subScoreMax float = null,

	@xmlFilter xml = null,

	@row nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @keys TABLE
	(
		ID int
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pPARead.PerfAssessFilterIDs
		@NumMatches OUT,
		0,
		0,
		null,
		null

		, @PerfAssessID
		, @TeacherID
		, @ConductedBy

		, @SchoolNo
		, @District
		, @Island

		, @StartDate
		, @EndDate


		, @subScoreCode
		, @subScoreMin
		, @subScoreMax

		, @xmlFilter

if @row = 'school'
	Select paschNo R
	, '<>' C
	, avg(paScore) Num
	From paAssessmentRpt A
		INNER JOIN @keys K
			ON A.paID = K.id
	Group by paSchNo

if @row = 'island'
	Select iName R
	, '<>' C
	, avg(paScore) Num
	From paAssessmentRpt A
		INNER JOIN @keys K
			ON A.paID = K.id
	Group by iName

if @row = 'district'
	Select dName R
	, '<>' C
	, avg(paScore) Num
	From paAssessmentRpt A
		INNER JOIN @keys K
			ON A.paID = K.id
	Group by dName
	-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

