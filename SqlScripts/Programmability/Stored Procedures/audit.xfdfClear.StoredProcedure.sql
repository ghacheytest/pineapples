SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 5 2015
-- Description:	clear the xfdfAudit records for a survey - securely
-- =============================================
CREATE PROCEDURE [audit].[xfdfClear]
	-- Add the parameters for the stored procedure here
	@SurveyID int
	with execute as 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE from xfdfAudit
	WHERE ssID = @SurveyID
END
GO
GRANT EXECUTE ON [audit].[xfdfClear] TO [pSurveyWrite] AS [pineapples]
GO

