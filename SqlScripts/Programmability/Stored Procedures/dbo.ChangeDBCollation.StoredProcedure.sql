SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChangeDBCollation]
as

begin

-- first step is to remove all check constraints from the database

declare @t sysname		-- table
declare @ts sysname     -- table schema
declare @c sysname		-- column
declare @cn sysname		-- constraint name
declare @cc nvarchar(4000) -- constraint clause

declare @sql nvarchar(2048)

-- prototypes of the various sql statements

declare @addcheckconstraint nvarchar(4000)
declare @dropconstraint nvarchar(4000)

declare  @drops table(
	stmnt nvarchar(4000)
)
declare  @adds table(
	stmnt nvarchar(4000)
)
select @addcheckconstraint = 'ALTER TABLE [%ts%].[%tn%]  WITH CHECK ADD  CONSTRAINT [%cn%] CHECK  %cc%
ALTER TABLE [%ts%].[%tn%] CHECK CONSTRAINT [%cn%]'

select @dropconstraint = 'ALTER TABLE [%ts%].[%tn%] DROP CONSTRAINT [%cn%]'
select *
into #CN
from information_schema.check_constraints -- save them
select * from #CN
while EXISTS (Select constraint_name from information_schema.check_constraints)
	begin
		select @t = table_name,
				@ts = table_schema,
				@cn = cc.constraint_name,
				@cc = check_clause
				from information_schema.check_constraints CC
				inner join information_schema.table_constraints TC
					on Tc.constraint_name = CC.constraint_name
		select @sql = @addcheckconstraint
		-- make the add constraint
		select @sql = replace(@sql,'%ts%',@ts)
		select @sql = replace(@sql,'%tn%',@t)
		select @sql = replace(@sql,'%cn%',@cn)
		select @sql = replace(@sql,'%cc%',@cc)

		-- save it
		INSERT INTO @Adds(stmnt)
		SELECT @sql

		select @sql = @dropconstraint
		-- make the drop constraint
		select @sql = replace(@sql,'%ts%',@ts)
		select @sql = replace(@sql,'%tn%',@t)
		select @sql = replace(@sql,'%cn%',@cn)
		select @sql = replace(@sql,'%cc%',@cc)

		INSERT INTO @Drops(stmnt)
		SELECT @sql
		print (@sql)
		exec (@sql)


	end

drop table #cn

while exists (Select stmnt from @adds A)
begin
	select TOP 1 @sql = stmnt from @adds A
	print (@sql)
	exec (@sql)
	delete from @adds WHERE stmnt = @sql
end

end
GO

