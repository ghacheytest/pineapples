SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	create or update a school survey from xfdf
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfSurvey]
	@xml xml

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
/*
-- the incoming xml is a tree split from the xfdf file extracted from the pdf
-- it looks like this:
<field name="Survey">
			<field name="SurveyYear" />
			<field name="SchoolName" />
			<field name="SchoolNo">
				<value>KPS001</value>
			</field>
			<field name="HtFamily" />
			<field name="HtGiven" />
			<field name="Pupils" />
			<field name="Classes" />
			<field name="Teachers" />
			<field name="Comment" />
			<field name="Language" />
			<field name="LanguageLower" />
		</field>
*/
   DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;

	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns

declare @schNo nvarchar(50)
declare @schName nvarchar(100)
declare @year int
declare @surveyID int

-- hanbdle inserting the School Survey recdord if it is not present
SELECT @schNo = schNo
	, @schName = schName
	, @year = svyYear
FROM OPENXML(@idoc, '/x:field',2)
WITH
(
	schNo		nvarchar(50)			'x:field[@name="SchoolNo"]/x:value'
	, schName	nvarchar(100)			'x:field[@name="SchoolName"]/x:value'
	, svyYear	int						'x:field[@name="SurveyYear"]/x:value'
)


Select @surveyID = ssID
FROM SchoolSurvey SS
		WHERE SS.schNo = @schNo
		AND SS.svyYear = @Year

-- clear the log if there is a surveyID
exec audit.xfdfClear @surveyID

begin try
	IF @surveyID is null begin
		-- use the exisitng proc for this to get all the right defaults
		-- 31 1 2016 swallow the return value, which create a resultset otherwise
		DECLARE @createResult TABLE
		(
			ssID int
		)
		-- 27 6 2016 we should test that there is a Survey record for @year
		Select @year = svyYear from Survey
		WHERE svyYear = @year
		if (@@ROWCOUNT = 0) begin
			Select @errorMessage = 'Cannot add survey - no survey has been commenced for year ' + convert(nvarchar(4), @year ) + '. Add the record to the Survey table.'
			RAISERROR(@ErrorMessage,16,1);
		end
		INSERT INTO @createResult
		exec pSurveyWrite.CreateSchoolSurvey @schNo, @year

		Select @surveyID = ssID
		FROM SchoolSurvey SS
				WHERE SS.schNo = @schNo
				AND SS.svyYear = @Year

		-- write the upload audit record
		exec audit.xfdfInsert @SurveyID, 'Survey created','Survey'


	end


	-- update the survey from the value s suppplied in the xml
	UPDATE SchoolSurvey
		SET ssNote = X.comment
		, ssPrinFirstName = X.htGiven
		, ssPrinSurname = X.htFamily
		, ssTotalClasses = X.classes
		, ssTotalPupils = X.pupils
		, ssTotalTeachers = X.teachers
		, ssLang = X.lang
		, ssLangLower = X.langLower
		-- additional fields could be added to school survey table, and loaded from the xml here
		--, ssaaaa = X.aaaa

	FROM OPENXML(@idoc, '/x:field',2)
	WITH
	(
	-- the xpath leads to the value for each field in the incoming xml
		comment		nvarchar(max)			'x:field[@name="Comment"]/x:value'
		, htGiven	nvarchar(50)			'x:field[@name="HtGiven"]/x:value'
		, htFamily	nvarchar(50)			'x:field[@name="HtFamily"]/x:value'
		--- these are aggregate totals, as collected on the original KEMIs layouts
		, classes	int						'x:field[@name="Classes"]/x:value'
		, pupils	int						'x:field[@name="Pupils"]/x:value'
		, teachers	int						'x:field[@name="Teachers"]/x:value'
		, lang		nvarchar(5)				'x:field[@name="Language"]/x:value'
		, langlower	nvarchar(5)				'x:field[@name="LanguageLower"]/x:value'
		-- any field in the pdf named Survey.aaaa can be added as a field here named aaaa
		-- this can then be added to the school survey table
	) X
	WHERE SchoolSurvey.ssID = @surveyID

	-- ensure the schoolyearhistory values are updated
	UPDATE SchoolYEarHistory
	SET saReceived = isnull(saReceived, getDate())
	, saEnteredBy = 'eSurveyLoader Loader'
	, saCompletedBy = ssPrinFirstName + ' ' + ssPrinSurname
	, syDormant = 0
	FROM SchoolYEarHistory
	INNER JOIN SchoolSurvey
		ON SchoolYearHistory.syYear = SchoolSurvey.svyYear
		AND SchoolYearHistory.schNo = SchoolSurvey.schNo
	WHERE SchoolSurvey.ssID = @surveyID

	Select @surveyID SurveyID, @schNo schNo, @schName schName
	exec audit.xfdfInsert @SurveyID, 'Survey updated','Survey'
end try
begin catch


	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

end
GO

