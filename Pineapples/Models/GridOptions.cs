﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using Softwords.Web.Models;
using System.ComponentModel.DataAnnotations;

namespace Pineapples.Models
{
    /// <summary>
    /// repreent a columnDef in the ui-grid grid options model
    /// </summary>
    public class ColumnDef
    {
        public ColumnDef() { }

        /// <summary>
        /// create a new ColumnDef from a ProprertyInfo
        /// This will be passed from an EF POCO object
        /// </summary>
        /// <param name="pi"></param>
        public ColumnDef(PropertyInfo pi)
        {
            name = pi.Name;
            field = pi.Name;

            DisplayAttribute da = pi
                                 .GetCustomAttributes(typeof(DisplayAttribute), true)
                                 .Cast<DisplayAttribute>()
                                 .SingleOrDefault();
            if (da != null)
            {
                if (da.Name != null)
                {
                    displayName = da.Name;
                }
                if (da.Description != null)
                {
                    description = da.Description;
                }
            }

            StringLengthAttribute sla = pi
                                 .GetCustomAttributes(typeof(StringLengthAttribute), true)
                                 .Cast<StringLengthAttribute>()
                                 .SingleOrDefault();
            if (sla != null)
            {
                maxlength = sla.MaximumLength;
                // if its a string, make the width proportional to the number of chars
                width = maxlength * 10;
                if (maxlength > 20)
                {
                    width = 200;
                }
                if (maxlength < 5)
                {
                    width = 50;
                }
            } else
            {
                width = 100;
            }
            // ClientLookup is an attribute to define the lookup list to use in the editable grid
            ClientLookupAttribute cla =  pi
                                            .GetCustomAttributes(typeof(ClientLookupAttribute), true)
                                            .Cast<ClientLookupAttribute>()
                                            .SingleOrDefault();
            if (cla != null)
            {
                lookup = cla.Lookup;
                // width of description will not correspond to width of the foreign key field
                width = 150;
            }

        }
        string cellClass;
        string cellFilter;
        string cellTemplate;
        bool cellTooltip;
        public string displayName;
        bool enableColumnMenu;
        bool enableColumnMenus;
        bool enableFiltering;
        bool enableHiding;
        bool enableSorting;
        public string field;
        object filter;
        bool filterCellFiltered;
        bool filterHeaderTemplate;
        object filters;
        string footerCellClass;
        string footerCellFilter;
        string footerCellTemplate;
        string headerCellClass;
        string headerCellFilter;
        string headerCellTemplate;
        string headerTooltip;
        int maxWidth;
        int minWidth;
        public string name;
        bool sortCellFiltered;
        bool suppressRemoveSort;
        public string type;
        public bool visible = true;
        public object width;

        // from Edit extension to ColumnDef
        bool cellEditableCondition;
        string editDropdownFilter;
        string editDropdownIdLabel;
        string editDropdownValueLabel;
        string editableCellTemplate;
        string enableCellEdit;
        bool enableCellEditOnFocus;

    // these are added for our own purposes - to be available to the template
        public int? maxlength;
        public string lookup;
        public string description;
    }
    public class GridOptions
    {
        public GridOptions()
        {
            columnDefs = new List<ColumnDef>();
        }
        public List<ColumnDef> columnDefs;
        /// <summary>
        /// Get any ChangeTracking attribute for the property
        /// </summary>
        /// <param name="piDest"></param>
        /// <returns>the change tracking attribute enum if any associated to this property</returns>
        private static ChangeTrackingAttribute getChangeTrackAttribute(System.Reflection.PropertyInfo piDest)
        {
            return (ChangeTrackingAttribute)piDest.GetCustomAttributes(true).FirstOrDefault(a => a is ChangeTrackingAttribute);
        }

        public static GridOptions fromType(Type typ)
        {
            GridOptions gopts = new GridOptions();

            return gopts;
        }
    }

    public class ViewMode
    {
        public GridOptions gridOptions;
        public bool editable = true;
        public bool deletable = true;
        public bool insertable = true;

        public ViewMode(Type typ)
        {
            gridOptions = new GridOptions();
            foreach (System.Reflection.PropertyInfo piDest in typ.GetProperties())
            {
                if (getChangeTrackAttribute(piDest) != null)
                {
                    // don;t add for the change tracked fields - client side we add these
                    continue;
                }
                if (isRowversion(piDest))
                {
                    // don't add the timestamp
                    continue;
                }
                {
                    gridOptions.columnDefs.Add(new ColumnDef(piDest));
                }
            }
        }
        private ChangeTrackingAttribute getChangeTrackAttribute(System.Reflection.PropertyInfo piDest)
        {
            return (ChangeTrackingAttribute)piDest.GetCustomAttributes(true).FirstOrDefault(a => a is ChangeTrackingAttribute);
        }

        private bool isRowversion(System.Reflection.PropertyInfo piDest)
        {
            return piDest.GetCustomAttributes(true).Any(a => a is TimestampAttribute);
        }

    }

}
