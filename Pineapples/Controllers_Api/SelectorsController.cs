﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;

namespace Pineapples.Controllers
{
    public class SelectorsController : PineapplesApiController
    {
      public SelectorsController(DataLayer.IDSFactory factory) : base(factory) { }

      [HttpPost]
      [ActionName("school")]
      public object School([FromBody] string search)
      {
        return Factory.Selectors().Schools(search);
      }

      [HttpPost]
      [ActionName("teacher")]
      public object Teacher([FromBody] string search)
      {
      return Factory.Selectors().Teachers(search);
      }
  }
}
