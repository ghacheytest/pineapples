﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;

namespace Pineapples.Controllers
{
    public class WorkforceController : PineapplesApiController
    {
      public WorkforceController(DataLayer.IDSFactory factory) : base(factory) { }
        [HttpPost]
        [ActionName("filter")]
        public object Filter(Data.WorkforceFilter fltr)
        {
            return Factory.Workforce().Filter(fltr); //.ResultSet;

        }
    }
}
