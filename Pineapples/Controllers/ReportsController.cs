﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.Controllers
{
    [RoutePrefix("reports")]
    public class ReportsController : Softwords.Web.mvcControllers.SSRSController
    {

        [HttpGet]
        async public Task<ActionResult> SchoolListByDistrict()
        {
            System.Collections.Generic.Dictionary<string, string> postData = new System.Collections.Generic.Dictionary<string, string>();
            //postData.Add("rs:command", "Render");
            postData.Add("rs:Format", "EXCEL");
            //postData.Add("rc:Parameters", "collapsed");

            //Scholar.Data.ApplicantFilter fltr = new Scholar.Data.ApplicantFilter();

            //fltr.Country.Set(country);
            //fltr.ScholarshipCountry.Set("AU");
            //fltr.ScholarshipStatus.Set("(all successful)");
            //postData.Add("XmlFilter", fltr.xmlFilter());

            postData.Add("District", "05");

            return await doRunReport(@"SchoolsReport", "EXCEL", postData, "Schools.xls");

        }

        [Route(@"reportDefComponent/{layout}")]
        public ActionResult ReportDefcomponent(string layout)
        {
            ViewBag.layout = layout;
            return View();
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult jasperInputDialog()
        {
            return View();
        }

        public ActionResult JasperList()
        {
            return View();
        }

        public ActionResult ReportsPage()
        {
            return View();
        }

    }
}
