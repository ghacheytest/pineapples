﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;


namespace Pineapples.mvcControllers
{
    public class SurveyController : Softwords.Web.mvcControllers.mvcControllerBase
    {

        // TODO: may be obsolete by following routes
        public ActionResult Grid()
        {
            return View();
        }

        public ActionResult PupilTableGrid()
        {
            return View();
        }

        [LayoutInjector("EditPanelLayout")]
        public ActionResult PupilTablePanel()
        {
            // pass through the data needed for the model
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Survey, PermissionAccess.Write);
            return View();
        }

        public ActionResult ResourceListGrid()
        {
            return View();
        }

        [LayoutInjector("EditPanelLayout")]
        public ActionResult ResourceListPanel()
        {
            // pass through the data needed for the model
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Survey, PermissionAccess.Write);
            return View();
        }

        public ActionResult Survey()
        {
            return View();
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult AuditLogDialog()
        {
            return View();
        }
        public ActionResult AuditLog()
        {
            return View();
        }
    }
}