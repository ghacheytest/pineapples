﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Softwords.Web.mvcControllers;

namespace Pineapples.Controllers
{
    // Provides virtualisation of commonly used assets
    public class AssetController : mvcControllerBase
    {
        [Route("asset/logo")]
        public ActionResult Logo()
        {
            // first test if there is a file with the context name
            string context = System.Web.Configuration.WebConfigurationManager.AppSettings["context"] ?? String.Empty;
            ActionResult file = null;
            if (context != string.Empty)
            {
                file = vImage(context);
                if (!(file is HttpNotFoundResult))
                    return file;
            }
            return vImage("logo");
        }

        // return an image
        [Route("asset/image/{filename}")]
        [Route("asset/image")]
        public ActionResult Image(string filename)
        {

            return vImage(filename);
        }
    }
}