﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pineapples.mvcControllers
{

    public class PerfAssessController : Softwords.Web.mvcControllers.mvcControllerBase
    {

        // GET: PerfAssess
        public ActionResult Index()
        {
            return View();
        }
        // GET: PerfAssess
        public ActionResult Entry()
        {
            return View();
        }

        public ActionResult PagedList()
        {
            return View();
        }

        public ActionResult PagedListEditable()
        {
            return View();
        }

        [Authorize]
        public ActionResult Searcher(string version)
        {
            return View();
        }

        #region PerfAssess-centric components
        public ActionResult SearcherComponent()
        {
            return View();
        }
        #endregion

    }
}