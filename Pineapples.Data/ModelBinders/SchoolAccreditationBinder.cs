﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.SchoolAccreditation>))]
    public class SchoolAccreditationBinder : Softwords.Web.Models.Entity.ModelBinder<Models.SchoolAccreditation>
    {
        public int? saID
        {
            get
            {
                return (int?)getProp("saID");
            }
            set
            {
                definedProps.Add("saID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.SchoolAccreditation sa = null;

            IQueryable<Models.SchoolAccreditation> qry = from schoolaccreditation in cxt.SchoolAccreditations
                                        where schoolaccreditation.saID == saID
                                        select schoolaccreditation;

            if (qry.Count() == 0)
            {
                throw new RecordNotFoundException(saID);
            }

            sa = qry.First();
            if (Convert.ToBase64String(sa.pRowversion.ToArray()) != this.RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(sa);
                throw new ConcurrencyException(this.definedProps);
            }
            ToDb(cxt, identity, sa);


        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.SchoolAccreditation sa = null;

            IQueryable<Models.SchoolAccreditation> qry = from schoolaccreditation in cxt.SchoolAccreditations
                                      where schoolaccreditation.saID == saID
                                      select schoolaccreditation;

            if (qry.Count() != 0)
            {
                throw new DuplicateKeyException(saID);
            }

            sa = new Models.SchoolAccreditation();
            cxt.SchoolAccreditations.Add(sa);
            ToDb(cxt, identity, sa);
        }
    }
}
