﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    [JsonConverter(typeof(FilterJsonConverter))]
    public class SchoolScatterOptions:Filter
    {
          // for now....

        public IFilterParam BaseYear { get { return getParam("BaseYear", typeof(int)); } }
        public IFilterParam XSeries { get { return getParam("XSeries", typeof(string)); } }
        public IFilterParam XSeriesOffset { get { return getParam("XSeriesOffset", typeof(int)); } }
        public IFilterParam YSeries { get { return getParam("YSeries", typeof(string)); } }
        public IFilterParam YSeriesOffset { get { return getParam("YSeriesOffset", typeof(int)); } }
        public IFilterParam xArg1 { get { return getParam("xArg1", typeof(string)); } }
        public IFilterParam xArg2 { get { return getParam("xArg2", typeof(string)); } }
        public IFilterParam yArg1 { get { return getParam("yArg1", typeof(string)); } }
        public IFilterParam yArg2 { get { return getParam("yArg2", typeof(string)); } }
        public IFilterParam SchoolType { get { return getParam("SchoolType", typeof(string)); } }
        public IFilterParam Authority { get { return getParam("Authority", typeof(string)); } }
        public IFilterParam District { get { return getParam("District", typeof(string)); } }
        public IFilterParam SchoolNo { get { return getParam("SchoolNo", typeof(string)); } }
        public IFilterParam kml { get { return getParam("kml", typeof(string)); } }

    }
}
