using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpBookTG")]
    public partial class BookTG : SimpleCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeDescription", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "code Description")]
        public override string Description { get; set; }

        [Column("codeDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "code Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("codeDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "code Description L2")]
        public override string DescriptionL2 { get; set; }
    }
}
