using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpBuildingValuationClass")]
    [Description(@"Classification of buildings for the purpose of valuation. Foreign key on Buildings and BuildingReview")]
    public partial class BuildingValuationClass : SimpleCodeTable
    {
        [Key]

        [Column("bvcCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "bvc Code is required")]
        [Display(Name = "bvc Code")]
        public override string Code { get; set; }

        [Column("bvcDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bvc Description")]
        public override string Description { get; set; }

        [Column("bvcDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bvc Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("bvcDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bvc Description L2")]
        public override string DescriptionL2 { get; set; }
    }
}
