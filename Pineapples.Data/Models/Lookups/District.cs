﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{

    [Table("Districts")]
    [Description(@"District is the principal geographic classifier of the school. 
District is determined by Island, iCode is on the School record.
District may be renamed using vocabulary, most usually to Province.")]
    public partial class District : ChangeTracked
    {
        [Key]
        [Column("dID", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Required(ErrorMessage = "ID is required")]
        [Display(Name = "ID")]
        public string Code { get; set; }

        [Column("dName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "d Name")]
        public string Name { get; set; }

        [Column("censusCode", TypeName = "nvarchar")]
        [MaxLength(2)]
        [StringLength(2)]
        [Display(Name = "Census Code")]
        public string censusCode { get; set; }

        [Column("examCode", TypeName = "nvarchar")]
        [MaxLength(2)]
        [StringLength(2)]
        [Display(Name = "Exam Code")]
        public string examCode { get; set; }

        [Column("oldCode", TypeName = "nvarchar")]
        [MaxLength(2)]
        [StringLength(2)]
        [Display(Name = "Old Code")]
        public string oldCode { get; set; }

        [Column("dShort", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "Short")]
        public string dShort { get; set; }

        [Column("dgisID", TypeName = "int")]
        [Display(Name = "gis ID")]
        public int? dgisID { get; set; }

        [Column("dMap", TypeName = "image")]
        [Display(Name = "d Map")]
        public byte[] dMap { get; set; }

        [Column("dPayrollID", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Payroll ID")]
        [Description("Identifier for the District as used in Payroll system")]
        public string dPayrollID { get; set; }
    }

}
