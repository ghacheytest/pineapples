using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpQualityIssues")]
    [Description(@"Classification of quality issues. Foreign key on SchoolSurveyQuality")]
    public partial class QualityIssue : SequencedCodeTable
    {
        [Column("codeSort", TypeName = "int")]
        public override int? Seq { get; set; }
    }
}
