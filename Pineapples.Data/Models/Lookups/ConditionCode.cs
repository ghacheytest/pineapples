using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpConditionCodes")]
    [Description(@"Codes representing the conditions of buildings. System defined.")]
    public partial class ConditionCode : SimpleCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description")]
        public override string Description { get; set; }

        [Column("codeDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("codeDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description L2")]
        public override string DescriptionL2 { get; set; }

        [Column("codeCodeN", TypeName = "smallint")]
        [Display(Name = "code Code N")]
        public short? codeCodeN { get; set; }

        [Column("codeInitial", TypeName = "nvarchar")]
        [MaxLength(3)]
        [StringLength(3)]
        [Display(Name = "code Initial")]
        public string codeInitial { get; set; }

        [Column("codeInitialL1", TypeName = "nvarchar")]
        [MaxLength(3)]
        [StringLength(3)]
        [Display(Name = "code Initial L1")]
        public string codeInitialL1 { get; set; }

        [Column("codeInitialL2", TypeName = "nvarchar")]
        [MaxLength(3)]
        [StringLength(3)]
        [Display(Name = "code Initial L2")]
        public string codeInitialL2 { get; set; }
    }
}
