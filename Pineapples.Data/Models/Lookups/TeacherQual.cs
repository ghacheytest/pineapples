using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpTeacherQual")]
    [Description(@"Qualifications a teacher may have. Collected on the teacher survey. The qualifiactions are divided into Ed and nonEd. The teacher's qualifications, and the secotr in which they are teacerhing, determine if the teacher is qualified to teach and/or certified to teach.")]
    public partial class TeacherQual : SequencedCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeSeq", TypeName = "int")]
        [Display(Name = "code Seq")]
        public override int? Seq { get; set; }

        [Column("codeTeach", TypeName = "bit")]
        [Display(Name = "code Teach")]
        public bool? codeTeach { get; set; }

        [Column("codeQualified", TypeName = "bit")]
        [Display(Name = "code Qualified")]
        public bool? codeQualified { get; set; }

        [Column("codeCertified", TypeName = "bit")]
        [Display(Name = "code Certified")]
        public bool? codeCertified { get; set; }

        [Column("codeGroup", TypeName = "nvarchar")]
        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "code Group")]
        public string codeGroup { get; set; }

        [Column("codeOld", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Old")]
        public string codeOld { get; set; }
    }
}
