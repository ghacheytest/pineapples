﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class WorkforceFilter : Filter
    {
        public DateTime? asAtDate { get; set; }
        public string SchoolNo { get; set; }
        public string Authority { get; set; }
        public string SchoolType { get; set; }
        public string RoleGrade { get; set; }
        public string Role { get; set; }
        public string Sector { get; set; }
        public int? Unfilled { get; set; }
        public int? PayslipExceptions { get; set; }
        public int? SurveyExceptions { get; set; }
        public int? SalaryPointExceptions { get; set; }
        public string PositionFlag { get; set; }
        public int? PositionFlagOp { get; set; }
    }
}
