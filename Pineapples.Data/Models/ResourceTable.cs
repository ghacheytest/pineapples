﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pineapples.Data
{
    public class ResourceCategory
    {
        //public string C { get; set; }
       // public int? ID { get; set; }
        //public string N { get; set; }
        public string categoryCode { get; set; }
        public string categoryDescription { get; set; }

    }
    public class ResourceDef
    {
        public int ResourceID;
        public string categoryCode;
        public int? seq;
        public string name;
        public bool promptAdq;          // prompt for 'Adequate'
        public bool promptAvailable;
        public bool promptNum;
        public bool promptCondition;
        public bool promptQty;
        public bool promptNote;
    }
    public class ResourceData
    {
        public string ResourceCode;
        public bool Adequate;          // prompt for 'Adequate'
        public bool Available;
        public int? Num;
        public string Condition;
        public int? Qty;
        public bool Functioning;
        public string Note;
    }
    public class ResourceList
    {
        public string SchoolNo { get; set; }
        public int Year { get; set; }
        public string Category { get; set; }
        public string Split { get; set; }
        public string Schooltype { get; set; }

        public ResourceDef[] Rows;
        public ResourceData[] Data;

    }
}
