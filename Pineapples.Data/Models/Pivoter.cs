﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    [JsonConverter(typeof(FilterJsonConverter))]
    public class Pivoter : Filter
    {
        public Pivoter():base()
        {
        }

        public Pivoter(string server, string database)
            : base(server, database)
        {
        }

        public IFilterParam DimensionColumns { get { return getParam("DimensionColumns", typeof(string)); } }
        public IFilterParam DataColumns { get { return getParam("DataColumns", typeof(string)); } }
        public IFilterParam Group { get { return getParam("Group", typeof(string)); } }
        public IFilterParam SchoolNo { get { return getParam("SchoolNo", typeof(string)); } }
        public IFilterParam SurveyYear { get { return getParam("SurveyYear", typeof(string)); } }

    }
}
